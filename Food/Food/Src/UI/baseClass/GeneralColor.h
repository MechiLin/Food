//
//  GeneralColor.h
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/11/29.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

//默认主色调
#define LTXDefaultsColor [UIColor mainColor]
//默认主字体
#define LTXDefaultsFont [UIFont systemFontOfSize:14]

//默认分割线颜色
#define LTXDefaultsLineColor [UIColor colorWithHexString:@"#C7C7CC"]

//默认背景颜色
#define LTXBgColor [UIColor colorWithHexString:@"#f7f7f7"]

//默认文字颜色
#define LTXDefaultsTextColor [UIColor colorWithHexString:@"#393a58"]

//设置字体
#define FontSet(fontSize)  [UIFont systemFontOfSize:fontSize]

//16进制颜色
#define UICOLOR_RGB_Alpha(_color,_alpha) [UIColor colorWithRed:((_color>>16)&0xff)/255.0f green:((_color>>8)&0xff)/255.0f blue:(_color&0xff)/255.0f alpha:_alpha]

//颜色
#define ZQColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

#define ZQColorRGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:a]

//加载本地图片
#define LoadImage(imageName) [UIImage imageNamed:imageName]

/// View 圆角
#define ViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]

///  View加边框
#define ViewBorder(View, BorderColor, BorderWidth )\
\
View.layer.borderColor = BorderColor.CGColor;\
View.layer.borderWidth = BorderWidth;
