//
//  LTXNavgationViewController.m
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/11/29.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import "LTXNavgationViewController.h"

@interface LTXNavgationViewController ()

@end

@implementation LTXNavgationViewController

+ (void)initialize
{
    
    UINavigationBar *bar = [UINavigationBar appearance];
    NSMutableDictionary *itemAttrs = [NSMutableDictionary dictionary];
    itemAttrs[NSForegroundColorAttributeName] = [UIColor blackColor];
    itemAttrs[NSFontAttributeName] = [UIFont boldSystemFontOfSize:17];
    [bar setTitleTextAttributes:itemAttrs];

    //设置导航栏背景图片
    UIImage* image = [[UIImage imageNamed:@"navigationBg"]
                      resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    [bar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
}

- (UIViewController *)childViewControllerForStatusBarStyle{
    
    return self.topViewController;
    
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
    // 修改tabBra的frame
    CGRect frame = self.tabBarController.tabBar.frame;
    frame.origin.y = [UIScreen mainScreen].bounds.size.height + frame.size.height;
    self.tabBarController.tabBar.frame = frame;
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

@end
