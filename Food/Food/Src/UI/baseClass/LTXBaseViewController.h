//
//  LTXBaseViewController.h
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/11/29.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LTXBaseViewController : UIViewController<UIGestureRecognizerDelegate>

@property(nonatomic, assign) BOOL isShowingLoading;

@property (nonatomic, assign) BOOL isHideLeftBtn;

@property (nonatomic, strong) UILabel *tittleLabel;

@property (nonatomic, assign) NSInteger menuItemIndex;


// 请求加载框隐藏显示
-(void) showLoading;
-(void) hideLoading;

- (void) showTabBar;
- (void) hideTabBar;

//返回按钮点击，子类可以重载，处理返回前需要做的逻辑处理
-(void) leftButtonPressed;


////传一个BarButton数组进去 生成导航右边按钮组
-(void)setNavRigthBarButton:(NSMutableArray *)array;

- (void)messageShow:(NSString *)message;

/**
 * 设置左上角返回键图标，默认是 “icon_navback”, 子类需要重载
 *
 */
- (UIImage *)imageForNavgationBackItem;

/**
 * 设置左上角返回按钮的偏移，默认是 ｛ －10， 0 ｝
 *
 */
- (UIOffset)offsetForNavigationBackItem;


/**
 创建控制器的TitlteView
 */
- (void)createTitleViewIsShowTip:(BOOL)isShow;

/**
 控制器titile
 
 @return 控制器titile文本
 */
-(NSString*) getDefaultTittle;


/**
 点击控制器title的响应事件
 */
- (void)handleTittleTouch;


@end
