//
//  HDConfig.pch
//  HDSmart
//
//  Created by 李宗帅 on 2018/7/2.
//  Copyright © 2018年 hdzkong.com. All rights reserved.
//

#ifdef __OBJC__
#ifndef HDConfig_pch
#define HDConfig_pch

#import "GeneralHead.h"
#import "GeneralColor.h"


// 兼容ios10
#define IOS7_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IOS8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define JSRGBColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define JSRGBColorWithAlpha(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define APP_DEFAULT_COLOR [UIColor blackColor]

#define StatusBarHeight (ScreenH == 812.0 ? 44 : 20)

#define SafeAreaTopHeight (ScreenH == 812.0 ? 88 : 64)

#define SafeAreaBottomHeight (ScreenH == 812.0 ? 34 : 0)

#define isIphoneX ScreenH == 812.0

// 第一个参数是当下的控制器适配iOS11 一下的，第二个参数表示scrollview或子类
#ifdef __IPHONE_11_0
#define AdjustsScrollViewInsetNever(controller,view) if(@available(iOS 11.0, *)) {view.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;} else if([controller isKindOfClass:[UIViewController class]]) {controller.automaticallyAdjustsScrollViewInsets = false;}
#endif

#define ScreenW [UIScreen mainScreen].bounds.size.width
#define ScreenH [UIScreen mainScreen].bounds.size.height

#define SCREEN_MAX_LENGTH (MAX(ScreenW, ScreenH))
#define SCREEN_MIN_LENGTH (MIN(ScreenW, ScreenH))


//最大最小值
#define MaxX(frame) CGRectGetMaxX(frame)
#define MaxY(frame) CGRectGetMaxY(frame)
#define MinX(frame) CGRectGetMinX(frame)
#define MinY(frame) CGRectGetMinY(frame)
//宽度高度
#define Width(frame)    CGRectGetWidth(frame)
#define Height(frame)   CGRectGetHeight(frame)

//常用宏
#define YZMGetMethodReturnObjc(objc) if (objc) return objc

#define App [UIApplication sharedApplication]
#define YZMFileManager [NSFileManager defaultManager]

#define YZMNotiCenter [NSNotificationCenter defaultCenter]

//适配机型  最低到5
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)


#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_5_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH <= 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE_6_LATER (IS_IPHONE && SCREEN_MAX_LENGTH > 667.0)

#define NSUSER_DEFAULTS [NSUserDefaults standardUserDefaults]


#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
#define SS(strongSelf) __strong __typeof(&*self)strongSelf = weakSelf;
// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.
// 这个宏用来定义运行环境
#define RUNNING_IN_PE   2       // 生产环境
#define RUNNING_IN_TE   1       // 测试环境
#define RUNNING_IN_DE   0       // 预发布环境
#define RUNNING_IN_HE   3       // 外网UAT

#define RUNNING_IN_ENVIR     RUNNING_IN_DE     // 更改这个宏定义可以切换环境

#define IS_RUNNING_IN_PE    (RUNNING_IN_ENVIR == RUNNING_IN_PE)  // 判断当前环境是不是生产环境
#define IS_RUNNING_IN_TE    (RUNNING_IN_ENVIR == RUNNING_IN_TE)  // 判断当前环境是不是测试环境
#define IS_RUNNING_IN_DE    (RUNNING_IN_ENVIR == RUNNING_IN_DE)  // 判断当前环境是不是开发环境
#define IS_RUNNING_IN_HE    (RUNNING_IN_ENVIR == RUNNING_IN_HE)  // 判断当前环境是不是外网UAT环境

#if IS_RUNNING_IN_PE
//生产环境
#define LTX_URL @"http://api.yclcp.com/ycl"
#define HD_URL @"http://dev.alian.me:8090"

#elif IS_RUNNING_IN_TE
//预发布环境
#define LTX_URL @"http://www.tongxuezhan.com:8080"
#define HD_URL @"http://dev.alian.me:8090"

#elif IS_RUNNING_IN_HE
//预发布环境
#define LTX_URL @"http://192.168.12.130:8080"
#define HD_URL @"http://dev.alian.me:8090"

#else   // DE
//开发环境
#define LTX_URL @"http://dev.alian.me:8080"
#define HD_URL @"http://dev.alian.me:8090"
#endif


// 请求缓存时间
#define Y_CACHETIMEINSECONDS  -1

#define Y_CACHETIMEINSECOND_5S  5

#define Y_CACHETIMEINSECONDS_1  1*60
#define Y_CACHETIMEINSECONDS_2  2*60
#define Y_CACHETIMEINSECONDS_3  3*60
#define Y_CACHETIMEINSECONDS_4  4*60
#define Y_CACHETIMEINSECONDS_5  5*60

#define Y_CACHETIMEINSECONDS_DAY  Y_CACHETIMEINSECONDS_1*60*24
#define Y_CACHETIMEINSECONDS_DAY_2  Y_CACHETIMEINSECONDS_DAY*2
#define Y_CACHETIMEINSECONDS_DAY_5  Y_CACHETIMEINSECONDS_DAY*5

#define hashEqual(str1,str2)  str1.hash == str2.hash  //hash码
//后期如果用户量打  可以采取分区  分端口 动态获取链接的方法

//多语言转换的宏
#define Multilingual(str) NSLocalizedStringFromTable(str, @"STLocalizable", nil)

#define k750Scale (ScreenW/375.0)

/*
 设备
 */
static NSString *DeviceType                = @"iOS";    //设备类型

static NSString *WLWGoodsListMainScrollNotification = @"WLWGoodsListMainScrollNotification";
static NSString *WLWSubMainScrollNotification = @"WLWSubMainScrollNotification";

//static NSString *poJieNiMei                = @"sdlqoiuetvz";//开锁密码拼接的key
//
//static NSString *fingerprintOpenKey = @"fingerprintOpenKey";//是否启用指纹
//
//static NSString *kHDNoticeIdentiferUpdateToken = @"登录信息过期的通知";//登录信息过期的key
//
//static NSString *kHDNoticeIdentiferWXThirdLogin = @"937kskdjvb";//微信登录
//
//static NSString *updateLastTimeKey = @"最后一次更新的时间";//说实话
//
//static NSString *kWeChatBindKey = @"lalalayiduoxiaohua";//需要绑定微信的通知

#define networkStatus  [GLobalRealReachability currentReachabilityStatus]  //网络状态

//static NSString *kWeChatAppID = @"wxfc35d8c0e2f756e4";
//
//static NSString *kWeChatBindSuc = @"wxbangdingchenggong";//微信绑定成功的通知
//
//
//static NSString *JPUSHServiceAppkey = @"082e199968fe3ca8ae79bcea";

//第三方登录  微信回调状态标注 就是为了验证是不是第三方登录发出去的消息
//#define WX_APP_AUTH_STATE @"lishuaiwolianwangIos"//登录的
//
//#define WX_APP_BIND_STATE @"lishuaibindhuisuo"//绑定的
//
//#define WX_SHARE_SEND_SUC @"contentShareSucce"//分享成功


#endif /* HDConfig_pch */
#endif
