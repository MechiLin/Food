//
//  YCLBaseHtmlViewController.m
//  Lottery
//
//  Created by szdl on 2017/7/3.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLBaseHtmlViewController.h"

@interface YCLBaseHtmlViewController ()<UIWebViewDelegate>
@property(strong, nonatomic)UIWebView* webView;
@end

@implementation YCLBaseHtmlViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    self.webView = [[UIWebView alloc]init];
    self.webView.backgroundColor = [UIColor whiteColor];
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    
    __weak typeof(self)weakSelf = self;
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(weakSelf.view.mas_top).offset(0);
        make.left.equalTo(weakSelf.view.mas_left).offset(0);
        make.right.equalTo(weakSelf.view.mas_right).offset(0);
        make.bottom.equalTo(weakSelf.view.mas_bottom).offset(0);
        
    }];
    
    
    /*
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES) objectAtIndex:0];
    NSString * path = [cachesPath stringByAppendingString:[NSString stringWithFormat:@"/Caches/%lu.html",(unsigned long)[_urlStr hash]]];
    
    NSString *htmlString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    if (!(htmlString ==nil || [htmlString isEqualToString:@""])) {
        [_webView loadHTMLString:htmlString baseURL:[NSURL URLWithString:_urlStr]];
    }else{
        NSURL *url = [NSURL URLWithString:_urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [_webView loadRequest:request];
        [self writeToCache];
    }
     */
    
    if ([_urlStr rangeOfString:@"http"].length > 0) {
        
        NSURL *url = [NSURL URLWithString:_urlStr];
        //NSURL *url = [NSURL URLWithString:@"https://www.baidu.com"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [_webView loadRequest:request];
    }
    else {
        
        NSString *path = [[NSBundle mainBundle] pathForResource:_urlStr ofType:@"html"];
        NSString *htmlString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        NSString *basePath = [[NSBundle mainBundle] bundlePath];
        NSURL *baseURL = [NSURL fileURLWithPath:basePath];
        
        [self.webView loadHTMLString:htmlString baseURL:baseURL];
    }
    
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    //页面背景色
   // [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.background='#ffffff'"];
}


/**
 * 网页缓存写入文件
 */
- (void)writeToCache
{
    NSString * htmlResponseStr = [NSString stringWithContentsOfURL:[NSURL URLWithString:_urlStr] encoding:NSUTF8StringEncoding error:Nil];
    //创建文件管理器
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    //获取document路径
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,      NSUserDomainMask, YES) objectAtIndex:0];
    [fileManager createDirectoryAtPath:[cachesPath stringByAppendingString:@"/Caches"]withIntermediateDirectories:YES attributes:nil error:nil];
    //写入路径
    NSString * path = [cachesPath stringByAppendingString:[NSString stringWithFormat:@"/Caches/%lu.html",(unsigned long)[_urlStr hash]]];
    
    [htmlResponseStr writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
}



- (void)createTitleView
{
    UILabel* label = [[UILabel alloc]init];
    label.textColor = JSRGBColor(0xff, 0xff, 0xff);
    label.text = _htmlTittle;
    label.font = [UIFont boldSystemFontOfSize:18.0f];
    CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:label.font,NSFontAttributeName,nil]];
    label.frame = CGRectMake(0, 0, size.width, 18);
    
    self.navigationItem.titleView = label;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
