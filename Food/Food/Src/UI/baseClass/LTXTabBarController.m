//
//  LTXTabBarController.m
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/11/29.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import "LTXTabBarController.h"
#import "LTXNavgationViewController.h"
#import "MQLHomeViewController.h"
#import "YCLMeViewController.h"
#import "MQLOrderListViewController.h"
@interface LTXTabBarController ()

@end

@implementation LTXTabBarController

+ (void)initialize
{
    // 通过appearance统一设置所有UITabBarItem的文字属性
    // 后面带有UI_APPEARANCE_SELECTOR的方法, 都可以通过appearance对象来统一设置
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:11];
    attrs[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#c7c7c7"];
    
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName] = attrs[NSFontAttributeName];
    selectedAttrs[NSForegroundColorAttributeName] = APP_DEFAULT_COLOR;
    
    UITabBarItem *item = [UITabBarItem appearance];
    [item setTitleTextAttributes:attrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupSubItem];
    [self.tabBar setBackgroundImage:[UIImage imageNamed:@"tab_bg"]];
}

- (void)setupSubItem
{
    [self setupChildVc:[[MQLHomeViewController alloc] init] title:@"首页" image:@"Tab-icon-1gray" selectedImage:@"Tab-icon-1"];

    [self setupChildVc:[[MQLOrderListViewController alloc] init] title:@"订单" image:@"Tab-icon-2gray" selectedImage:@"Tab-icon-2"];

    [self setupChildVc:[[YCLMeViewController alloc] init] title:@"我的" image:@"Tab-icon-5gray" selectedImage:@"Tab-icon-5"];
//
//    [self setupChildVc:[[YCLMeViewController alloc] init] title:@"我的" image:@"navbar_icon_user_normal" selectedImage:@"navbar_icon_user_selected"];
    
}



- (void)setupChildVc:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage
{
    // 设置文字和图片
    vc.tabBarItem.title = title;
    NSDictionary *dictHome = [NSDictionary dictionaryWithObject:LTXDefaultsColor forKey:NSForegroundColorAttributeName];
    [vc.tabBarItem setTitleTextAttributes:dictHome forState:UIControlStateSelected];

    vc.tabBarItem.image = [[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    // 包装一个导航控制器, 添加导航控制器为tabbarController的子控制器
    LTXNavgationViewController *nav = [[LTXNavgationViewController alloc] initWithRootViewController:vc];
    [self addChildViewController:nav];
    
}


@end
