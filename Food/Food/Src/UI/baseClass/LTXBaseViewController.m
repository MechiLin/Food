//
//  LTXBaseViewController.m
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/11/29.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import "LTXBaseViewController.h"
#import "LTXTabBarController.h"
@interface LTXBaseViewController ()

@property (nonatomic, strong) UIImageView *arrowImageView ;

@end

@implementation LTXBaseViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    TTDPRINT(@"\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"
             "     %@   ------->   dealloc                  \n"
             "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n",[self class]);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = YES;
//    self.view.backgroundColor = YCLBgColor;
//    UIImageView* barView = [[UIImageView alloc]init];
//    barView.image = [UIImage imageNamed:@"headBgView"];
//    barView.frame = CGRectMake(0, 64, CGRectGetWidth(self.view.bounds), barView.image.size.height);
//    [self.view addSubview:barView];
//    self.navigationController.view.backgroundColor = YCLBgColor;
    [self showTabBar];
    _menuItemIndex = 0;
    //   [self SelectCityChange];
    if (!self.isHideLeftBtn) {
        [self customNavgationBar];
    }else
    {
        self.navigationItem.hidesBackButton = YES;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc]init]];
    
    self.navigationController.navigationBar.barTintColor = LTXDefaultsColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
//    self.navigationController.navigationBar.hidden=YES;
}



-(void)customNavgationBar{
    
    //    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];//nav_back02
    
    UIImage* navBackImage = [self imageForNavgationBackItem] ?: [UIImage imageNamed:@"navSsqBackButtonTip"];
    
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setImage:navBackImage forState:UIControlStateNormal];
    [rightBtn sizeToFit];
    rightBtn.frame=CGRectMake(0, 0, rightBtn.width, rightBtn.height);
    [rightBtn addTarget:self action:@selector(leftButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbutton=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
//    leftbutton.tintColor = JSRGBColor(255,255, 255);
    
    if(IOS7_OR_LATER) {
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                       target:nil action:nil];
        negativeSpacer.width = [self offsetForNavigationBackItem].horizontal; //这个数值可以根据情况自由变化, 默认：－10
        self.navigationItem.leftBarButtonItems = @[negativeSpacer, leftbutton];
    }
    else
        self.navigationItem.leftBarButtonItem = leftbutton;
    
    self.navigationItem.hidesBackButton = YES;
//    self.tabBarController.tabBar.hidden=YES;
}

-(void) leftButtonPressed
{
    [self hideLoading];
    [self popViewController];

}


- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}




- (void) showTabBar{
    
    if (self.tabBarController.tabBar.hidden == NO)
    {
        //return;
    }
    UIView *contentView;
    if ([[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]])
        
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    
    else
        
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    contentView.frame = CGRectMake(contentView.bounds.origin.x, contentView.bounds.origin.y,  contentView.bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    self.tabBarController.tabBar.hidden = NO;
}

-(void)hideTabBar
{

    if (self.tabBarController.tabBar.hidden == YES) {
        return;
    }
    UIView *contentView;
    if ( [[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] )
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    else
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    
    contentView.frame = CGRectMake(contentView.bounds.origin.x,  contentView.bounds.origin.y,  contentView.bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    self.tabBarController.tabBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    LTXTabBarController *tabBarController=(LTXTabBarController*)self.navigationController.tabBarController;
    tabBarController.tabBar.hidden=NO;
}



#pragma mark - MBProgressHUD
-(void) showLoading
{
    [self performSelectorOnMainThread:@selector(showMB) withObject:nil waitUntilDone:NO];
}

-(void) showMB
{
    if (self.isShowingLoading) {
        return;
    }
    
    self.isShowingLoading = YES;
    showAnimationAutoMiss(self.view);
}

-(void) hideLoading
{
    [self performSelectorOnMainThread:@selector(hideMB) withObject:nil waitUntilDone:NO];
}
-(void) hideMB
{
    self.isShowingLoading=NO;
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}



//Barbutton设置
//传一个BarButton数组进去
-(void)setNavRigthBarButton:(NSMutableArray *)array{
    // 调整 leftBarButtonItem 在 iOS7 下面的位置
    if(([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0?20:0))
    {
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                       target:nil action:nil];
        negativeSpacer.width = -10;//这个数值可以根据情况自由变化
        NSMutableArray *array1=[NSMutableArray arrayWithObject:negativeSpacer];
        [array1 addObjectsFromArray:array];
        
        self.navigationItem.rightBarButtonItems =array1;
        
    }
    else
    {
        self.navigationItem.rightBarButtonItems=array;
    }
}

- (void)messageShow:(NSString *)message
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alert show];
}

//返回按钮
- (UIImage *)imageForNavgationBackItem
{
    return [UIImage imageNamed:@"icon_back_1"];
}

- (UIOffset)offsetForNavigationBackItem {
    return UIOffsetMake(0, 0);
}

/**
 创建头View

 @param isShow 是否需要隐藏和显示
 */
- (void)createTitleViewIsShowTip:(BOOL)isShow
{
    UILabel* label = nil;
    if (_tittleLabel == nil) {
        label = [[UILabel alloc]init];
        label.text = [self getDefaultTittle];
        _tittleLabel = label;
    }
    else {
        label = _tittleLabel;
    }
    
    label.textColor = JSRGBColor(0xff, 0xff, 0xff);
    label.font = [UIFont boldSystemFontOfSize:18];
    
    CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:label.font,NSFontAttributeName,nil]];
    label.frame = CGRectMake(0, 3, ceil(size.width), 16);
    
    CGFloat titlConW = isShow?label.frame.size.width + 9 + 12:label.frame.size.width;
    
    UIControl *titleControl = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, titlConW, 24)];
    [titleControl addTarget:self action:@selector(handleTittleTouch) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleControl;
    [titleControl addSubview:label];
    
    if (isShow)
    {
        CGFloat imageX = label.frame.size.width + 9;
        
        UIImageView *arrowView = nil;
        if (_arrowImageView == nil) {
            arrowView = [[UIImageView alloc] init];
            arrowView.image = [UIImage imageNamed:@"navSsqBackButtonTip"];
            _arrowImageView = arrowView;
        }
        else {
            arrowView = _arrowImageView;
        }
        
        arrowView.frame = CGRectMake(imageX, 8.5, 12, 7);
        
        [titleControl addSubview:arrowView];
    }
    
}




-(NSString*) getDefaultTittle {
    //viewController的title
    return @"";
}


#pragma mark-- bet style menu dismiss
- (void)dismissView {
    //收起
    _arrowImageView.image = [UIImage imageNamed:@"navSsqDownArrowTip"];
    
}

- (void)handleMenuTouch:(NSInteger)index {
    _menuItemIndex = index;
    
}

- (void)handleTittleTouch {

}

@end
