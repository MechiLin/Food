//
//  LTXBaseRequest.h
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/12/5.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import <YTKNetwork/YTKNetwork.h>

@interface LTXBaseRequest : YTKBaseRequest

- (id)requestNewArgument;

@end
