//
//  LTXAccount.m
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/11/29.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import "HDAccount.h"
@implementation HDAccount

#define ZQAccountFilePath  [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"account.data"]

static HDAccount *account;



+(instancetype)accountWithDic:(NSDictionary *)dic
{
    HDAccount *account = [HDAccount account];
    if (!account) {
        account = [[self alloc]init];
    }
    account.userID =[NSString stringWithFormat:@"%@",dic[@"id"]];
    account.isAdmin = [[dic objectForKey:@"isAdmin"] boolValue];
//    account.phone = [NSString stringWithFormat:@"%@",[dic valueForKey:@"phone"]];
//    account.username = [NSString stringWithFormat:@"%@",[dic valueForKey:@"username"]];
//    account.photo = [NSString stringWithFormat:@"%@",[dic valueForKey:@"photo"]];
//    account.sex = [NSString stringWithFormat:@"%@",[dic valueForKey:@"sex"]];
//    account.email = [NSString stringWithFormat:@"%@",[dic valueForKey:@"email"]];
//    account.longitude = [NSString stringWithFormat:@"%@",[dic valueForKey:@"longitude"]];
//    account.latitude = [NSString stringWithFormat:@"%@",[dic valueForKey:@"latitude"]];
    
    return account;
}

/**
 *  当从文件中解析出一个对象的时候调用
 *  在这个方法中写清楚：怎么解析文件中的数据
 */
-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        
        self.userID =[aDecoder decodeObjectForKey:@"id"];
        self.isAdmin = [[aDecoder decodeObjectForKey:@"isAdmin"] boolValue];
//        self.username = [aDecoder decodeObjectForKey:@"username"];
//        self.photo = [aDecoder decodeObjectForKey:@"photo"];
//        self.phone = [aDecoder decodeObjectForKey:@"phone"];
//        self.sex = [aDecoder decodeObjectForKey:@"sex"];
//        self.email = [aDecoder decodeObjectForKey:@"email"];
//        self.imageKey = [aDecoder decodeObjectForKey:@"key"];
//        self.isLoginProtect = [aDecoder decodeBoolForKey:@"isLoginProtect"];
//        self.isLockPassword = [aDecoder decodeBoolForKey:@"isLockPassword"];
//        self.isEncrypted = [aDecoder decodeBoolForKey:@"isEncrypted"];
//        self.lockPassword = [aDecoder decodeObjectForKey:@"lockPassword"];
//        self.isBindWeChat = [aDecoder decodeBoolForKey:@"isBindWeChat"];
//        self.longitude = [aDecoder decodeObjectForKey:@"longitude"];
//        self.latitude = [aDecoder decodeObjectForKey:@"latitude"];
    }

    return self;
}

/**
 *  将对象写入文件的时候调用
 *  在这个方法中写清楚：要存储哪些对象的哪些属性，以及怎样存储属性
 */
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userID forKey:@"id"];
    [aCoder encodeObject:@(self.isAdmin) forKey:@"isAdmin"];
//    [aCoder encodeObject:self.username forKey:@"username"];
//    [aCoder encodeObject:self.photo forKey:@"photo"];
//    [aCoder encodeObject:self.phone forKey:@"phone"];
//    [aCoder encodeObject:self.sex forKey:@"sex"];
//    [aCoder encodeObject:self.email forKey:@"email"];
//    [aCoder encodeObject:self.imageKey forKey:@"key"];
//    [aCoder encodeBool:self.isLockPassword forKey:@"isLockPassword"];
//    [aCoder encodeBool:self.isEncrypted forKey:@"isEncrypted"];
//    [aCoder encodeBool:self.isLoginProtect forKey:@"isLoginProtect"];
//    [aCoder encodeObject:self.lockPassword forKey:@"lockPassword"];
//    [aCoder encodeBool:self.isBindWeChat forKey:@"isBindWeChat"];
//    [aCoder encodeObject:self.longitude forKey:@"longitude"];
//    [aCoder encodeObject:self.latitude forKey:@"latitude"];
}


/// 账号归档
+ (void)save:(HDAccount *)newAccount {
    
    account=newAccount;
    
    [NSKeyedArchiver archiveRootObject:newAccount toFile:ZQAccountFilePath];
}

/// 账号反归档
+ (HDAccount *)account {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //读取账号 解挡
        account = [NSKeyedUnarchiver unarchiveObjectWithFile:ZQAccountFilePath];
    });
    //    读取账号 解挡
    //        YCLAccount * account = [NSKeyedUnarchiver unarchiveObjectWithFile:ZQAccountFilePath];
    //    判断账号是否过期
    //        NSDate * now = [NSDate date];
    //        if ([now compare:account.remind_in] != NSOrderedAscending   ) {
    //            account = nil;
    //        }
    return account;
}


+ (void)delAccount {
    TTDPRINT(@"");
    account=nil;
    NSFileManager * defaultManager = [NSFileManager defaultManager];
    if ([defaultManager isDeletableFileAtPath:ZQAccountFilePath]) {
        [defaultManager removeItemAtPath:ZQAccountFilePath error:nil];
    }
}

@end
