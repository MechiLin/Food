//
//  YCLBaseModel.m
//  Lottery
//
//  Created by 李宗帅 on 2017/6/16.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "LTXBaseModel.h"

@implementation LTXBaseModel

- (void)setNilValueForKey:(NSString *)key
{
    [self setValue:@0 forKey:key];
}
@end
