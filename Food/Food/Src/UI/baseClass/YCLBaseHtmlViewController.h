//
//  YCLBaseHtmlViewController.h
//  Lottery
//
//  Created by szdl on 2017/7/3.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCLAbstractViewController.h"

@interface YCLBaseHtmlViewController : YCLAbstractViewController
@property(copy, nonatomic)NSString* htmlTittle;
@property(copy, nonatomic)NSString* urlStr;
@end
