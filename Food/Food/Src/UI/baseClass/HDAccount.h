//
//  LTXAccount.h
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/11/29.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDAccount : NSObject<NSCoding>

@property (nonatomic, copy) NSString *userID;//用户ID;

@property (nonatomic,assign) BOOL isAdmin; //是否是管理员

@property (nonatomic, copy) NSString *username;//用户名

@property (nonatomic, copy) NSString *photo;//头像

@property (nonatomic, copy) NSString *phone;//手机号  也就是账户

@property (nonatomic, copy) NSString *sex;//性别

@property (nonatomic, copy) NSString *email;//邮箱

@property (nonatomic, copy) NSString *imageKey;//头像的key

@property (nonatomic, copy) NSString *lockPassword;//开锁密码

@property (nonatomic, assign) BOOL isLockPassword;//是否设置过开锁密码

@property (nonatomic, assign) BOOL isEncrypted;//是否设置过密保

@property (nonatomic, assign) BOOL isLoginProtect;//是否设置过登录保护

@property (nonatomic, assign) BOOL isBindWeChat;//是否绑定过微信

//@property (nonatomic, copy) NSString *longitude;//经度
//
//@property (nonatomic, copy) NSString *latitude;//纬度

//NSLog(@"旧的经度：%f,旧的纬度：%f",oldCoordinate.longitude,oldCoordinate.latitude);


+(instancetype)accountWithDic:(NSDictionary *)dic;

/**
 *  存储帐号
 */
+ (void)save:(HDAccount *)account;

/**
 *  读取帐号
 */
+ (HDAccount *)account;
/**
 *  删除帐号
 */
+(void )delAccount;


@end
