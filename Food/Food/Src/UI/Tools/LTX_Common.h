//
//  YCL_Common.h
//  Lottery
//
//  Created by 李宗帅 on 2017/6/1.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>
#define  MAIN_BACK_COLOR @"#00ada7"
@interface LTX_Common : NSObject

/**
 *  提示框
 * @param _mes 提示内容
 */
+(void) showAlertWithMessage:(NSString*)_mes;



+ (void)showAlertWithMessage:(NSString *)_mes offset:(UIOffset)offset;

+ (void)showAlertWithTitle:(NSString *)title msg:(NSString *)msg actions:(NSArray *)actions viewController:(UIViewController *)vc callBack:(void(^)(NSString *title))clickAction;

/**
 *  提示框（提示框垂直居中显示）
 * @param _mes 提示内容
 */
+(void) showAlertWithMessageup:(NSString*)_mes;

//判断是否为整形：
+ (BOOL)isPureInt:(NSString*)string;

//判断是否为浮点形：
+ (BOOL)isPureFloat:(NSString*)string;

//6-20位  包含数字和字母
+(BOOL)judgePassWordLegal:(NSString *)pass;

// 设置圆形图片
+ (void) setImageViewBorder:(UIImageView*)faceImageView;

//UILabel自适应
+(CGSize)sizeFitForContent:(NSString *)content withSize:(CGSize)size withFont:(UIFont *)font;

//查看URL分割后的参数
+ (NSDictionary *)queryParams:(NSString *)url;


//压缩图片 小于200K的不压缩
+ (NSData *)getCompressedImage:(UIImage *)originImage;

/**
 获取带下滑线的副文本

 @param color 文本颜色
 @param lineColor 下滑线颜色  为nil  默认为文本颜色
 @param text 文本
 @return 副文本
 */
+ (NSMutableAttributedString*)getUnderlineTextWithTextColor:(UIColor *)color lineColor:(UIColor *)lineColor text:(NSString *)text;
/**
 发短信

 @param number 手机号
 */
void openSMS(NSString *number);

/**
 *  调用浏览器
 *
 *  @param str http://
 */
void openUrl(NSString *str);

/**
 *  打电话
 *
 *  @param phoneNumber 电话号码
 */
void openCall(NSString *phoneNumber);

/**
 *  打开email
 *
 *  @param email email 地址
 */
void openEmail(NSString *email);

/**
 *  跳转到AppStore
 *
 */
void gotoAppStore(NSString *appid);


/**
 跳转到touchID设置页面
 */
void gotoTouchID(void);

/**
 *  是否允许推送
 *
 */
BOOL isAllowedNotification(void);


/**
 是否拒绝访问照片库权限
 */

BOOL wlwUserPhotoDenied(void);

/**
 *  获取系统版本
 *
 */
NSString *getVersion(void);


/**
 *  获取设备名称
 *
 */
NSString *getDeviceName(void);


/**
 *  获取设备的机型
 *
 */
NSString * getCurrentDeviceModel(void);

//提示信息
void showMessageAutoMiss(NSString *message);

void showAnimationAutoMiss(UIView *view);

/**
 获取当前控制器

 @return 获取当前控制器
 */
+ (UIViewController *)getCurrentVC;


/**
 获取当前tableView的图片

 @param tableView 当前的tableView
 @return tableview的截图
 */
+ (UIImage*)getCaptureWithTableView:(UITableView *)tableView;


/**
 生成一个view的图片

 @param theView 要生成为图片的view
 @return 图片
 */
+(UIImage*) captureView:(UIView *)theView;



/**
 合并两个图片

 @param image1 图片1
 @param image2 图片2
 @return 合成完成的图片
 */
+ (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2;


/**
 左右拼接两个图片

 @param image1 图片左
 @param image2 图片右
 @return 合完成的图片
 */
+ (UIImage *)addLfetImage:(UIImage *)image1 rightImage:(UIImage *)image2;

//josn字符串转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

//字典转json字符串
+ (NSString *)convertToJsonData:(NSDictionary *)dict;

+ (NSDate*)toDateWith:(NSString*)str;

/**
 *  改变字间距
 */
+ (void)changeWordSpaceForLabel:(UILabel *)label WithSpace:(float)space;



/**
 开启一个定时器

 @param target 定时器持有者
 @param timeInterval 执行间隔时间
 @param handler 重复执行事件
 */
void dispatchTimer(id target, double timeInterval,void (^handler)(dispatch_source_t timer));

//byte数组转Int
+(int) lBytesToInt:(Byte[]) byte;

//判断手机型号 为做兼容准备
+ (NSString*)iphoneType;

//model转data
+ (NSData*)getJSON:(id)obj options:(NSJSONWritingOptions)options error:(NSError**)error;


/**
 当前时间分类

 @return 上午?下午?凌晨?中午?
 */
+ (NSString *)getPeriodOfNowTime;


/**
 生成二维码

 @param content 要生成的内容
 @param size 宽高多少?
 @return 生成的二维码
 */
+ (UIImage *)createQrCodeWithContent:(NSString *)content andWidth:(CGFloat)size;




@end
