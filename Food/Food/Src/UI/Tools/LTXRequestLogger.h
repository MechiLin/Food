//
//  LTXRequestLogger.h
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/12/8.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YTKBaseRequest, LTXBaseRequest;
@interface LTXRequestLogger : NSObject

/** 打印request信息 */
+ (void)logRequestWithYCLRequest:(LTXBaseRequest *)YCLrequest YTKRequest:(YTKBaseRequest *)YTKRequest error:(NSError *)error;

/** 打印请求返回的信息 */
+ (void)logResponseWithYCLRequest:(LTXBaseRequest *)YCLrequest YTKRequest:(YTKBaseRequest *)YTKRequest error:(NSError *)error;

@end

