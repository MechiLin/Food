//
//  UINavigationItem+YCLMargin.m
//  Lottery
//
//  Created by IAMEASON on 2017/9/19.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "UINavigationItem+YCLMargin.h"
#import "NSObject+SXRuntime.h"

#ifndef deviceVersion
#define deviceVersion [[[UIDevice currentDevice] systemVersion] floatValue]
#endif

#ifndef sx_defaultFixSpace
#define sx_defaultFixSpace 0
#endif

static BOOL sx_disableFixSpace = NO;

@implementation UINavigationBar (YCLMargin)

+(void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleInstanceMethodWithOriginSel:@selector(layoutSubviews)
                                     swizzledSel:@selector(sx_layoutSubviews)];
    });
}

-(void)sx_layoutSubviews{
    [self sx_layoutSubviews];
    
    if (deviceVersion >= 11 && !sx_disableFixSpace) {//需要调节
        self.layoutMargins = UIEdgeInsetsZero;
        CGFloat space = sx_defaultFixSpace;
        for (UIView *subview in self.subviews) {
            if ([NSStringFromClass(subview.class) containsString:@"ContentView"]) {
                subview.layoutMargins = UIEdgeInsetsMake(0, space, 0, subview.layoutMargins.right);//可修正iOS11之后的偏移
                break;
            }
        }
    }
}

@end



@implementation UINavigationItem (YCLMargin)

- (void)setupLeftBarButtonItem:(UIBarButtonItem *)leftBarButtonItem
{
    if (leftBarButtonItem) {
        [self setupLeftBarButtonItems:@[leftBarButtonItem]];
    }
}

- (void)setupLeftBarButtonItems:(NSArray *)leftItems
{
    if (leftItems.count <= 0) {
        return;
    }
    
    NSMutableArray *itemArray = [NSMutableArray arrayWithArray:leftItems];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 11.0) {
        UIBarButtonItem *spaceButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        spaceButtonItem.width = -15;
        [itemArray insertObject:spaceButtonItem atIndex:0];
    }
    
    self.leftBarButtonItems = itemArray;
}

- (void)setupRightBarButtonItem:(UIBarButtonItem *)rightBarButtonItem
{
    if (rightBarButtonItem) {
        [self setupRightBarButtonItems:@[rightBarButtonItem]];
    }
}

- (void)setupRightBarButtonItems:(NSArray *)rightItems
{
    if (rightItems.count <= 0) {
        return;
    }
    
    NSMutableArray *itemArray = [NSMutableArray arrayWithArray:rightItems];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 11.0) {
        UIBarButtonItem *spaceButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
       // spaceButtonItem.width = -15;
        [itemArray insertObject:spaceButtonItem atIndex:0];
    }
    
    self.rightBarButtonItems = itemArray;
}

+ (UIBarButtonItem *)itemWithImage:(NSString *)imageName highImage:(NSString *)highImageName target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:highImageName] forState:UIControlStateHighlighted];
    button.frame = (CGRect){CGPointZero, button.currentBackgroundImage.size};
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

@end
