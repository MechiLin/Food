//
//  NSData+Upload.h
//  Lottery
//
//  Created by 李宗帅 on 2017/6/19.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

@interface NSData (Upload)


/**
 上传文件

 @param data 文件
 @param succe 成功的回调
 @param failure 失败的回调
 */
+ (void)upLoadWithContenData:(NSData *)data succe:(void(^)(NSDictionary *dict))succe failure:(void(^)(NSError *error))failure;

+ (void)loadImageWithContenData:(NSString *)phoneNum succe:(void(^)(UIImage *image))succe progress:(void(^)(CGFloat Progress))progress failure:(void(^)(NSError *error))failure;


#pragma mark --- 上传头像
+ (void)p_updateFileData:(NSData *)fileData key:(NSString *)key token:(NSString *)token complete:(void (^)(NSError * _Nullable error,NSString * _Nullable key))complete;
@end

