//
//  NSData+Upload.m
//  Lottery
//
//  Created by 李宗帅 on 2017/6/19.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "NSData+Upload.h"
@implementation NSData (Upload)

+ (void)upLoadWithContenData:(NSData *)data succe:(void(^)(NSDictionary *dict))succe failure:(void(^)(NSError *error))failure;
{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 20;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    NSTimeInterval interv = [NSDate.date timeIntervalSince1970]*1000;       // ms
    interv = [NSDate.date timeIntervalSince1970]*1000000;
    NSMutableDictionary *bodyDict = [[NSMutableDictionary alloc]init];;
    [bodyDict setValue:[NSString stringWithFormat:@"imageFileName%lld",(long long)interv] forKey:@"filename"];

    [manager POST:[[NSString stringWithFormat:@"http://pub168.yclcp.com:9100/"] stringByAppendingString:@"public/file/upload"] parameters:bodyDict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        //按照表单格式把二进制文件写入formData表单  name必须是跟后台约定好的key 
        [formData appendPartWithFileData:data name:@"file" fileName:[NSString stringWithFormat:@"%@.jpeg", bodyDict[@"filename"]] mimeType:@"image/jpeg"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        //上传进度  用到了可以从这里拿
        TTDERROR(@"%f",uploadProgress.fractionCompleted);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        TTDERROR(@"上传成功:%@",responseObject);
        NSDictionary *dict = (NSDictionary *)responseObject;
        succe(dict);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        TTDERROR(@"上传失败:%@",error);
        failure(error);
    }];
}


+ (void)loadImageWithContenData:(NSString *)phoneNum succe:(void(^)(UIImage *image))succe progress:(void(^)(CGFloat Progress))progress failure:(void(^)(NSError *error))failure
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.timeoutInterval = 20;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    
    [manager GET:[[NSString stringWithFormat:@"http://pub168.yclcp.com:9100"] stringByAppendingString:@"/public/verify/getKaptchaImage"] parameters:@{@"source":phoneNum} progress:^(NSProgress * _Nonnull downloadProgress) {
        
        //下载进度  用到了可以从这里拿
        TTDERROR(@"%f",downloadProgress.fractionCompleted);
        progress(downloadProgress.fractionCompleted);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        TTDERROR(@"下载成功:%@",responseObject);
        NSData *dict = (NSData *)responseObject;
        UIImage *image = [UIImage imageWithData:dict];
        succe(image);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        TTDERROR(@"下载失败:%@",error);
        
        failure(error);
        
    }];
}



@end

