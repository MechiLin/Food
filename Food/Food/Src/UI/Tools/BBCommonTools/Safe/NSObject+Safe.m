//
//  NSObject+Safe.m
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "NSObject+Safe.h"

@implementation NSObject (Safe)


- (id)safeValueForKey:(NSString *)key
{
    
    if (!key){
        return @"";
    }
    
    id value = [self valueForKey:key];
    if(value == nil)
    {
        return @"";
    }
    return value;
}


- (void)safeSetValue:(id)value forKey:(NSString *)key
{
    if (!value || !key){
        return ;
    }
    
    [self setValue:value forKey:key];
}


@end
