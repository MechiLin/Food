//
//  NSIndexPath+safe.m
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "NSIndexPath+safe.h"

@implementation NSIndexPath (safe)

+ (instancetype)safeIndexPathForItem:(NSInteger)item inSection:(NSInteger)section
{
    if (item<0) {
        item = 0;
        return nil;
    }
    if (section<0) {
        section = 0;
        return nil;
    }
    
    return [self indexPathForItem:item inSection:section];
}

+ (instancetype)safeIndexPathForRow:(NSInteger)row inSection:(NSInteger)section
{
    if (row<0) {
        row = 0;
        return nil;
    }
    if (section<0) {
        section = 0;
        return nil;
    }
    return [self indexPathForRow:row inSection:section];
}


@end
