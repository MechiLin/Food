//
//  NSObject+Safe.h
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Safe)

/**
 *  Key-Value 安全取值
 *
 *  @param key key
 *
 *  @return value
 */
- (id)safeValueForKey:(NSString *)key;

/**
 *  Key-Value 安全赋值
 *
 *  @param value value
 *  @param key   key
 */
- (void)safeSetValue:(id)value forKey:(NSString *)key;

@end
