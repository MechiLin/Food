//
//  NSObject+swizzle.h
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (swizzle)

+ (BOOL)overrideMethod:(SEL)origSel withMethod:(SEL)altSel;
+ (BOOL)overrideClassMethod:(SEL)origSel withClassMethod:(SEL)altSel;
+ (BOOL)exchangeMethod:(SEL)origSel withMethod:(SEL)altSel;
+ (BOOL)exchangeClassMethod:(SEL)origSel withClassMethod:(SEL)altSel;

- (BOOL)safeCompareWithString:(NSString*)string;

@end
