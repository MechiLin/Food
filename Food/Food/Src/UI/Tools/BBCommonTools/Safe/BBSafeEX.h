//
//  SafeEX.h
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//


/**
 
 全局集合类使用安全扩展
 
 **/

#import "NSObject+swizzle.h"//selecter替换类
#import "NSArray+safe.h"
#import "NSDictionary+safe.h"
#import "NSMutableArray+safe.h"
#import "NSMutableDictionary+safe.h"
#import "NSNumber+safe.h"
#import "NSString+safe.h"
#import "UITableView+safe.h"
#import "NSIndexPath+safe.h"
#import "NSObject+Safe.h"

