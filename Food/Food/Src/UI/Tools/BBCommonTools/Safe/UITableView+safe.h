//
//  UITableView+safe.h
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UITableView (safe)

- (void)safeScrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated;

- (void)safeSelectRowAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated scrollPosition:(UITableViewScrollPosition)scrollPosition;

- (UITableViewCell *)safeCellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end
