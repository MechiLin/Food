//
//  NSNumber+safe.m
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "NSNumber+safe.h"

@implementation NSNumber (safe)

- (BOOL)safeIsEqualToNumber:(NSNumber *)number
{
    if (number == nil) {
        return NO;
    } else {
        return [self isEqualToNumber:number];
    }
}

- (NSComparisonResult)safeCompare:(NSNumber *)otherNumber
{
    if (otherNumber == nil) {
        return NSOrderedDescending;
    } else {
        return [self compare:otherNumber];
    }
}
// 服务端传number，我们用NSString接收，造成unrecognized，添加备用消息接受者,防止Crash
- (id)forwardingTargetForSelector:(SEL)aSelector
{
    return [self stringValue];
}

@end
