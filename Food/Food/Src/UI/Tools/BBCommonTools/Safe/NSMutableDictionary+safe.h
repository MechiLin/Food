//
//  NSMutableDictionary+safe.h
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary(safe)

+ (instancetype)safeWithDictionary:(NSDictionary *)dic;

+ (instancetype)safeDictionaryWithDictionary:(NSDictionary *)dic;

- (void)safeSetObject:(id)aObj forKey:(id<NSCopying>)aKey;

- (id)safeObjectForKey:(id<NSCopying>)aKey;

/** Dictionary add otherDictionary */
- (void)safeAddEntriesFromDictionary:(NSDictionary *)otherDictionary;

/** 移除aKey */
- (void)safeRemoveObjectForKey:(id<NSCopying>)aKey;

@end
