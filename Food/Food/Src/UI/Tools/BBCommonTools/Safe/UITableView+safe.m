//
//  UITableView+safe.m
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "UITableView+safe.h"

@implementation UITableView (safe)

- (void)safeScrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    if (indexPath.section >= self.numberOfSections) {
        return;
    } else if (indexPath.row >= [self numberOfRowsInSection:indexPath.section]) {
        return;
    } else {
        [self scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:animated];
    }
}

- (void)safeSelectRowAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated scrollPosition:(UITableViewScrollPosition)scrollPosition
{
    if (indexPath.section >= self.numberOfSections) {
        return;
    } else if (indexPath.row >= [self numberOfRowsInSection:indexPath.section]) {
        return;
    } else {
        [self selectRowAtIndexPath:indexPath animated:animated scrollPosition:scrollPosition];
    }
}

- (UITableViewCell *)safeCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section >= self.numberOfSections) {
        return nil;
    } else if (indexPath.row >= [self numberOfRowsInSection:indexPath.section]) {
        return nil;
    } else {
        return [self cellForRowAtIndexPath:indexPath];
    }
}

@end
