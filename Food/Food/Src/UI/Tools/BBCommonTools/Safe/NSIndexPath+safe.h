//
//  NSIndexPath+safe.h
//  Lottery
//
//  Created by eason on 2017/8/30.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface NSIndexPath (safe)

+ (instancetype)safeIndexPathForItem:(NSInteger)item inSection:(NSInteger)section;

+ (instancetype)safeIndexPathForRow:(NSInteger)row inSection:(NSInteger)section;

@end
