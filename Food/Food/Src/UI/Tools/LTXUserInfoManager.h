//
//  YCLUserInfoManager.h
//  Lottery
//
//  Created by 李宗帅 on 2017/6/16.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LTXUserInfoManager : NSObject

/**
 *  @brief  存储密码 用户名
 *
 *  @param  password    密码
 *  @param  userName    用户名
 */
+(void)savePassWord:(NSString *)password UserName:(NSString *)userName;

/**
 *  @brief  读取密码
 *
 *  @return 密码内容
 */
+(id)readPassWord;

/**
 读取用户名

 @return 用户名内容
 */
+(id)readUserName;

/**
 *  @brief  删除用户数据
 */
+(void)deleteUserInfo;


@end
