//
//  UIBarButtonItem+Extension.h
//  Lottery
//
//  Created by Aesthetic92 on 16/7/9.
//  Copyright © 2016年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Extension)

+ (instancetype)js_itemWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action;

+ (instancetype)js_itemWithDictionary:(NSDictionary*)dictonary target:(id)target action:(SEL)action;

@end
