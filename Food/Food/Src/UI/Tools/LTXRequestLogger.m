//
//  LTXRequestLogger.m
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/12/8.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import "LTXRequestLogger.h"
#import "LTXBaseRequest.h"
@implementation LTXRequestLogger



+ (void)logRequestWithYCLRequest:(LTXBaseRequest *)YCLrequest YTKRequest:(YTKBaseRequest *)YTKRequest error:(NSError *)error {
#ifdef DEBUG
    NSMutableString *logString = [NSMutableString stringWithString:@"\n\n**************************************************************\n*                          请求开始                           *\n**************************************************************\n"];
    
    [logString appendFormat:@"请求方式：%@\n", [self stringForMethod:YCLrequest.requestMethod]];
    [logString appendFormat:@"数据类型：%@\n", [self stringForDataType:YCLrequest.requestSerializerType]];
    [logString appendFormat:@"超时时间：%@\n", @(YCLrequest.requestTimeoutInterval)];
    [logString appendFormat:@"请求地址：%@\n", YCLrequest.requestUrl];
    [logString appendFormat:@"请求参数：%@\n", YCLrequest.requestNewArgument];
    [logString appendFormat:@"**************************************************************\n\n"];
    
    TTDPRINT(@"%@", logString);
#endif
    
}

+ (void)logResponseWithYCLRequest:(LTXBaseRequest *)YCLrequest YTKRequest:(YTKBaseRequest *)YTKRequest error:(NSError *)error {
    
#ifdef DEBUG
    BOOL shouldLogError = error ? YES : NO;
    
    NSMutableString *logString = [NSMutableString stringWithString:@"\n\n==============================================================\n=                           请求返回                          =\n==============================================================\n"];
    
    [logString appendFormat:@"请求地址：%@\n", YCLrequest.requestUrl];
    [logString appendFormat:@"返回json数据：\n\n%@\n", YTKRequest.responseString];
    if (shouldLogError) {
        [logString appendFormat:@"Error Domain:\t\t\t\t\t\t\t%@\n", error.domain];
        [logString appendFormat:@"Error Domain Code:\t\t\t\t\t\t%ld\n", (long)error.code];
        [logString appendFormat:@"Error Localized Description:\t\t\t%@\n", error.localizedDescription];
        [logString appendFormat:@"Error Localized Failure Reason:\t\t\t%@\n", error.localizedFailureReason];
        [logString appendFormat:@"Error Localized Recovery Suggestion:\t%@\n\n", error.localizedRecoverySuggestion];
    }
    
    [logString appendFormat:@"==============================================================\n\n"];
    
    TTDPRINT(@"%@", logString);
    
#endif
}

#pragma mark - Helper
+ (NSString *)stringForMethod:(YTKRequestMethod)method {
    switch (method) {
        case YTKRequestMethodGET:
            return @"GET";
            break;
        case YTKRequestMethodPOST:
            return @"POST";
            break;
            
        default:
            break;
    }
    return @"unknow";
}

+ (NSString *)stringForDataType:(YTKRequestSerializerType)dataType {
    switch (dataType) {
        case YTKRequestSerializerTypeHTTP:
            return @"Http";
            break;
        case YTKRequestSerializerTypeJSON:
            return @"Json";
            break;
        default:
            break;
    }
    return @"unknow";
}

@end
