//
//  UITextField+YCLBACK.h
//  Lottery
//
//  Created by 李宗帅 on 2017/7/25.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YCLTextFieldDelegate <UITextFieldDelegate>

@optional
- (void)textFieldDidDeleteBackward:(UITextField *)textField;

@end


@interface UITextField (YCLBACK)
@property (weak, nonatomic) id<YCLTextFieldDelegate> delegate;
@end
/**
 *  监听删除按钮
 *  object:UITextField
 */
extern NSString * const WJTextFieldDidDeleteBackwardNotification;
