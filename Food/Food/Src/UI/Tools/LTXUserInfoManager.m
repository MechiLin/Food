//
//  YCLUserInfoManager.m
//  Lottery
//
//  Created by 李宗帅 on 2017/6/16.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "LTXUserInfoManager.h"
#import "WQKeyChain.h"
@implementation LTXUserInfoManager

static NSString * const KEY_IN_KEYCHAIN = @"com.lottery.yicaile";
static NSString * const KEY_PASSWORD = @"com.lottery.yicaile.passWord";
static NSString * const KEY_USERNAME = @"com.lottery.yicaile.userName";

+(void)savePassWord:(NSString *)password UserName:(NSString *)userName;
{
    NSMutableDictionary *usernamepasswordKVPairs = [NSMutableDictionary dictionary];
    [usernamepasswordKVPairs setObject:password forKey:KEY_PASSWORD];
    [usernamepasswordKVPairs setValue:userName forKey:KEY_USERNAME];
    [WQKeyChain save:KEY_IN_KEYCHAIN data:usernamepasswordKVPairs];
}

+(id)readPassWord
{
    NSMutableDictionary *usernamepasswordKVPair = (NSMutableDictionary *)[WQKeyChain load:KEY_IN_KEYCHAIN];
    return [usernamepasswordKVPair objectForKey:KEY_PASSWORD];
}

+(id)readUserName;
{
    NSMutableDictionary *usernamepasswordKVPair = (NSMutableDictionary *)[WQKeyChain load:KEY_IN_KEYCHAIN];
    return [usernamepasswordKVPair objectForKey:KEY_USERNAME];
}

+(void)deleteUserInfo
{
    [WQKeyChain delete:KEY_IN_KEYCHAIN];
}

@end
