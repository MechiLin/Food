//
//  UIColor+ColorChange.h
//  Lottery
//
//  Created by 李宗帅 on 2017/5/31.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorChange)


// 颜色转换：iOS中（以#开头）十六进制的颜色转换为UIColor(RGB)
+ (UIColor *) colorWithHexString: (NSString *)color;

//颜色转换，可以输入透明度
+(UIColor *)colorWithHexString:(NSString *)hexString andAlpha:(CGFloat)alpha;

//当前app用到的颜色

/**
 主色

 @return 颜色
 */
+ (UIColor *)mainColor;

@end
