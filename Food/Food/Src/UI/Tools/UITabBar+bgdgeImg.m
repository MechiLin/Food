//
//  UITabBar+bgdgeImg.m
//  Lottery
//
//  Created by 李宗帅 on 2017/8/29.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "UITabBar+bgdgeImg.h"
#define TabbarItemNums 4.0    //tabbar的数量

@implementation UITabBar (bgdgeImg)

- (void)showBadgeOnItemIndex:(int)index Image:(UIImage *)image{
    
    //移除之前的小红点
    [self removeBadgeOnItemIndex:index];
    
    //新建小红点
    UIImageView *badgeView = [[UIImageView alloc]init];
    badgeView.tag = 888 + index;
    badgeView.image = image;
    [badgeView sizeToFit];
    CGRect tabFrame = self.frame;
    
    //确定小红点的位置
    float percentX = (index +0.6) / TabbarItemNums;
    CGFloat x = ceilf(percentX * tabFrame.size.width) - 7;
    CGFloat y = ceilf(0.1 * tabFrame.size.height);
    badgeView.frame = CGRectMake(x, y, badgeView.width, badgeView.height);
    [self addSubview:badgeView];
    
}

- (void)hideBadgeOnItemIndex:(int)index{
    
    //移除小红点
    [self removeBadgeOnItemIndex:index];
    
}

- (void)removeBadgeOnItemIndex:(int)index{
    
    //按照tag值进行移除
    for (UIImageView *subView in self.subviews) {
        
        if (subView.tag == 888+index) {
            
            [subView removeFromSuperview];
            
        }
    }
}


@end
