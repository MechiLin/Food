//
//  UIImage+Extension.h
//  Lottery
//
//  Created by Aesthetic92 on 16/7/9.
//  Copyright © 2016年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

- (UIImage *)js_circleImage;

+ (UIImage *)createImageWithColor:(UIColor *)color frame:(CGRect)rect;

@end
