//
//  NSDate+extension.h
//  LazySchoolmate
//
//  Created by 李宗帅 on 2018/1/4.
//  Copyright © 2018年 李宗帅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (extension)

//判断时间戳是否为当天,昨天,一周内,年月日
+ (NSString *)timeStringWithTimeInterval:(NSString *)timeInterval;



@end
