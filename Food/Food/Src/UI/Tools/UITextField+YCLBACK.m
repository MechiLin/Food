//
//  UITextField+YCLBACK.m
//  Lottery
//
//  Created by 李宗帅 on 2017/7/25.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "UITextField+YCLBACK.h"
#import <objc/runtime.h>
NSString * const WJTextFieldDidDeleteBackwardNotification = @"com.whojun.textfield.did.notification";

@implementation UITextField (YCLBACK)

+ (void)load {
    //交换2个方法中的IMP
    Method method1 = class_getInstanceMethod([self class], NSSelectorFromString(@"deleteBackward"));
    Method method2 = class_getInstanceMethod([self class], @selector(wj_deleteBackward));
    method_exchangeImplementations(method1, method2);
}

- (void)wj_deleteBackward {
    [self wj_deleteBackward];
    
    if ([self.delegate respondsToSelector:@selector(textFieldDidDeleteBackward:)])
    {
        id <YCLTextFieldDelegate> delegate  = (id<YCLTextFieldDelegate>)self.delegate;
        [delegate textFieldDidDeleteBackward:self];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:WJTextFieldDidDeleteBackwardNotification object:self];
}

@end
