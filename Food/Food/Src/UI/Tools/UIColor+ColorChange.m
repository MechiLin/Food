//
//  UIColor+ColorChange.m
//  Lottery
//
//  Created by 李宗帅 on 2017/5/31.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "UIColor+ColorChange.h"

@implementation UIColor (ColorChange)

+ (UIColor *) colorWithHexString: (NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    // 判断前缀
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    // 从六位数值中找到RGB对应的位数并转换
    NSRange range;
    range.location = 0;
    range.length = 2;
    //R、G、B
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}

//可以传入透明度
+(UIColor *)colorWithHexString:(NSString *)hexString andAlpha:(CGFloat)alpha{
    
    if ([hexString length] != 7) {
        return [UIColor whiteColor];
    }else{
        
        NSRange range = NSMakeRange(1, 2);
        NSString * red = [hexString substringWithRange:range];
        range.location = 3;
        NSString * green = [hexString substringWithRange:range];
        range.location = 5;
        NSString * blue = [hexString substringWithRange:range];
        unsigned int r,g,b;
        [[NSScanner scannerWithString:red] scanHexInt:&r];
        [[NSScanner scannerWithString:green] scanHexInt:&g];
        [[NSScanner scannerWithString:blue] scanHexInt:&b];
        return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:alpha];
    }
}


/**
 主色
 
 @return 颜色
 */
+ (UIColor *)mainColor
{
    return [UIColor colorWithHexString:@"#FF5A3D"];
//     [UIColor colorWithRed:251/255.f green:34/255.f blue:32/255.f alpha:1.0];
}


@end
