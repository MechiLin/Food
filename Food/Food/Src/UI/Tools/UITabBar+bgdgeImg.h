//
//  UITabBar+bgdgeImg.h
//  Lottery
//
//  Created by 李宗帅 on 2017/8/29.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBar (bgdgeImg)

- (void)showBadgeOnItemIndex:(int)index Image:(UIImage *)image;   //显示提示图片

- (void)hideBadgeOnItemIndex:(int)index; //隐藏提示图片

@end
