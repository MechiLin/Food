//
//  UIBarButtonItem+Extension.m
//  Lottery
//
//  Created by Aesthetic92 on 16/7/9.
//  Copyright © 2016年 Aesthetic92. All rights reserved.
//

#import "UIBarButtonItem+Extension.h"

@implementation UIBarButtonItem (Extension)

+ (instancetype)js_itemWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage* bgImage = [UIImage imageNamed:image];
    [button setBackgroundImage:bgImage forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:highImage] forState:UIControlStateHighlighted];
    button.size = CGSizeMake(bgImage.size.width, bgImage.size.height);
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [[self alloc] initWithCustomView:button];
}




+ (instancetype)js_itemWithDictionary:(NSDictionary*)dictonary target:(id)target action:(SEL)action
{
    NSString* image = dictonary[@"image"];
    NSString* fontSize = dictonary[@"fontSize"];
    NSString* tittle = dictonary[@"tittle"];
    
    NSString* width = dictonary[@"width"];
    NSString* height = dictonary[@"height"];
    
    NSString* tittleEdgeLeft = dictonary[@"tittleEdgeLeft"];
    NSString* imageEdgeLeft = dictonary[@"imageEdgeLeft"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
   
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    
    if (tittle.length > 0) {
        [button setTitle:tittle forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    }

    button.size = CGSizeMake([width integerValue], [height integerValue]);
    
    if (tittleEdgeLeft != nil) {
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, [tittleEdgeLeft integerValue], 0, 0)];
    }
    
    if (imageEdgeLeft != nil) {
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, [imageEdgeLeft integerValue], 0, 0)];
    }

    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[self alloc] initWithCustomView:button];
}








@end
