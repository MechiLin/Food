//
//  MBProgressHUD+quickAction.h
//  NEWHDSmart
//
//  Created by 李宗帅 on 2018/7/19.
//  Copyright © 2018年 hdzkong.com. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (quickAction)

+ (void)showSuccess:(NSString *)success;
+ (void)showSuccess:(NSString *)success toView:(UIView *)view;

+ (void)showError:(NSString *)error;
+ (void)showError:(NSString *)error toView:(UIView *)view;

+ (MBProgressHUD *)showMessage:(NSString *)message;
+ (MBProgressHUD *)showMessage:(NSString *)message toView:(UIView *)view;

+ (void)hideHUD;
+ (void)hideHUDForView:(UIView *)view;

@end
