//
//  NSString+LTXCheck.h
//  LazySchoolmate
//
//  Created by 李宗帅 on 2017/12/6.
//  Copyright © 2017年 李宗帅. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (LTXCheck)
//判断是否是邮箱
+(BOOL)isValidateEmail:(NSString *)email;
//正则表达式判断手机号
+ (BOOL)checkTel:(NSString *)str;

//判断密码规格
+ (BOOL)checkPassword:(NSString *)str;

//判断时间 是今天昨天还是明天  如果都不是就返回时间
+(NSString *)compareDate:(NSString *)dateStr;

//时间戳转时间
+ (NSString *)timeWithTimeIntervalString:(NSString *)timeString;

//判断是否有表情
+(BOOL)stringContainsEmoji:(NSString *)string;

//3位用逗号隔开
+(NSString *)countNumAndChangeformat:(NSString *)num;
//加密方法
-(NSString *)sha1;

/** 获取字符串Size */
- (CGSize)getUISize:(UIFont*)font limitWidth:(CGFloat)width;

/** 获取带行间距的字符串Size*/
- (CGSize)getUISize:(UIFont*)font WithParagraphSpace:(CGFloat)space limitWidth:(CGFloat)width;

/** 生成带“¥”符号的字符串*/
- (NSString *)addCurrencySymbol;

/** 中文字符长度 */
- (NSInteger)byteLength;

/** 判断字符串字符个数 */
+ (int)getCharacterFromStr:(NSString *)tempStr;

/** 判断@"http://" or @"https://"  return YES */
- (BOOL)isHTTPLink;

/** 给不同的内容赋不同的字体 */
+ (NSArray *)descripteDifferentLabelWithFirstString:(NSString *)firstString  AndFirstFont:(UIFont *)fontF WithFirstColor:(UIColor *)firstColor WithSecondString:(NSString *)secondString AndSecondFont:(UIFont *)fontL AndSecondColor:(UIColor *)secondColor;

//获取当地时间
+ (NSString *)getCurrentTime;
//传入今天的时间，返回明天的时间
+ (NSString *)GetTomorrowDay:(NSInteger)count;
@end
