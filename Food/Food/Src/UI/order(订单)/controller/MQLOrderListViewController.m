//
//  MQLOrderListViewController.m
//  Food
//
//  Created by 李宗帅 on 2018/8/11.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "MQLOrderListViewController.h"
#import "MQLOrderListModelManager.h"
#import "MQLOrderListTableViewCell.h"
#import "WTOSureOrderViewController.h"
@interface MQLOrderListViewController ()<MQLOrderListModelManagerDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) MQLOrderListModelManager *modelManager;

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation MQLOrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:NO];

    self.view.backgroundColor = LTXBgColor;
    
    [self.view addSubview:self.tableView];
}


- (void)createTitleView
{
    UILabel* label = [[UILabel alloc]init];
    label.textColor = JSRGBColor(0xff, 0xff, 0xff);
    label.text = @"订单";
    label.font = [UIFont boldSystemFontOfSize:18];
    CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:label.font,NSFontAttributeName,nil]];
    label.frame = CGRectMake(0, 0, size.width, 18);
    
    self.navigationItem.titleView = label;
}

- (void)createLeftView {
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc]initWithCustomView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    UIBarButtonItem *leftSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftSpacer.width = 15;//默认-5
    // [self.navigationItem setupLeftBarButtonItems:@[leftSpacer, leftButton]];
}
- (void)updateOrderList
{
    
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelManager.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    MQLOrderListTableViewCell *cell = [MQLOrderListTableViewCell cellWithTableView:tableView];
    
    cell.obj = self.modelManager.dataArr[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //根据订单状态  判断有木有下边的botom
    HDOrderObject *obj = self.modelManager.dataArr[indexPath.row];
    //进订单性情页面
    WTOSureOrderViewController *vc = [[WTOSureOrderViewController alloc]initWithState:0];
    vc.goodsArray = [self.modelManager orderArrWithFoodObjs:(NSArray *)obj.details];
    vc.orderObj = obj;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

//状态栏为白色
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (MQLOrderListModelManager *)modelManager
{
    if (!_modelManager) {
        _modelManager = [[MQLOrderListModelManager alloc]init];
        _modelManager.delegate = self;
    }
    
    return _modelManager;
}


- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, SafeAreaTopHeight, ScreenW, ScreenH - SafeAreaTopHeight - SafeAreaBottomHeight - self.tabBarController.tabBar.height) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 90;
        _tableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectZero];
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return _tableView;
}
@end
