//
//  MQLOrderListTableViewCell.h
//  Food
//
//  Created by 李宗帅 on 2018/8/11.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HDOrderObject.h"
@interface MQLOrderListTableViewCell : UITableViewCell

@property (nonatomic, strong) HDOrderObject *obj;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
