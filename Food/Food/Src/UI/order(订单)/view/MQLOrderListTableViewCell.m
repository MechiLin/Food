//
//  MQLOrderListTableViewCell.m
//  Food
//
//  Created by 李宗帅 on 2018/8/11.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "MQLOrderListTableViewCell.h"

@interface MQLOrderListTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *logoImg;
@property (weak, nonatomic) IBOutlet UILabel *orderName;

@property (weak, nonatomic) IBOutlet UIButton *orderStateBtn;
@property (weak, nonatomic) IBOutlet UILabel *orderTime;
@end

@implementation MQLOrderListTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    MQLOrderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([self class])];
    if (!cell) {
        
        cell = [[[NSBundle  mainBundle]  loadNibNamed:@"MQLOrderListTableViewCell" owner:self options:nil]  lastObject];
    }
    
    return cell;
}

- (void)setObj:(HDOrderObject *)obj
{
    _obj = obj;
    
    [self.logoImg sd_setImageWithURL:[NSURL URLWithString:obj.logoUrl] placeholderImage:LoadImage(@"shop_default_logo")];
    
    [self.orderName setText:[NSString stringWithFormat:@"%@  Tel:%@",obj.realName,obj.mobile]];
    
    [self.orderTime setText:[NSString stringWithFormat:@"总消费: $:%.2f",obj.amount]];
    
    self.orderStateBtn.selected = !obj.orderStatus;
}


@end
