//
//  MQLOrderListModelManager.h
//  Food
//
//  Created by 李宗帅 on 2018/8/11.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "LTXBaseModelManager.h"
#import "HDOrderObject.h"
#import "MQLFoodModel.h"
@protocol MQLOrderListModelManagerDelegate <NSObject>

- (void)updateOrderList;

@end

@interface MQLOrderListModelManager : LTXBaseModelManager

@property (nonatomic, weak) id<MQLOrderListModelManagerDelegate>delegate;

@property (nonatomic, strong) NSArray *dataArr;

- (NSArray *)orderArrWithFoodObjs:(NSArray *)arr;
@end
