//
//  MQLOrderListModelManager.m
//  Food
//
//  Created by 李宗帅 on 2018/8/11.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "MQLOrderListModelManager.h"

@interface MQLOrderListModelManager ()

@property (nonatomic, strong) RLMNotificationToken *orderToken;

@property (nonatomic, copy) RLMResults *allOrderObj;
@end

@implementation MQLOrderListModelManager

- (instancetype)init
{
    if (self = [super init])
    {
        _allOrderObj = [HDOrderObject allObjects];
        _orderToken = self.foodToken;
    }
    return self;
}

- (RLMNotificationToken *)foodToken
{
    
    if (!_orderToken) {
        WS(weakSelf);
        _orderToken = [self.allOrderObj addNotificationBlock:^(RLMResults * _Nullable results, RLMCollectionChange * _Nullable change, NSError * _Nullable error) {
            
            if ([weakSelf.delegate respondsToSelector:@selector(updateOrderList)])
            {
                RLMResults *results = [HDOrderObject allObjects];
                NSMutableArray *arr = [NSMutableArray array];
                
                for (HDOrderObject *obj in results) {

                    HDOrderObject *model = [[HDOrderObject alloc]init];
                    model.objectId = obj.objectId;
                    model.logoUrl = obj.logoUrl;
                    model.amount = obj.amount;
                    model.updatedAt = obj.updatedAt;
                    model.details = obj.details;
                    
                    model.remark = obj.remark;
                    model.realName = obj.realName;
                    model.mobile = obj.mobile;
                    model.detailAddress = obj.detailAddress;
                    model.orderStatus = obj.orderStatus;
                    
                    [arr addObject:model];
                    
                }
                weakSelf.dataArr = arr;
                [weakSelf.delegate updateOrderList];
            }
        }];
    }
    return _orderToken;
}


- (NSArray *)orderArrWithFoodObjs:(NSArray *)arr
{
    NSMutableArray *dataArr = [NSMutableArray arrayWithCapacity:20];
    for (HDOrderDetailObject *obj in arr) {

        MQLFoodModel *model = [[MQLFoodModel alloc]init];
        model.objectId = obj.objectId;
        model.name = obj.name;
        model.price = obj.price;
        model.buyCount = obj.buyCount;
        model.imgUrl = obj.imgUrl;
        [dataArr addObject:model];
    }
    return dataArr;
}
@end
