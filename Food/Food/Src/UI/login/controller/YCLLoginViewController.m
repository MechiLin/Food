//
//  YCLLoginViewController.m
//  Lottery
//
//  Created by 李宗帅 on 2017/5/31.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLLoginViewController.h"
#import "YCLAccountPasswordView.h"
#import "YCLLoginModelManager.h"

@interface YCLLoginViewController ()<UIScrollViewDelegate,YCLLoginSubViewDelegate>

@property (nonatomic, strong) UIScrollView *segementScrollView;

@property (nonatomic, strong) UIImageView *bgImageView;

@property (nonatomic,strong) YCLLoginModelManager *modelManager;

@property (nonatomic, strong) YCLAccountPasswordView *accountLoginView;

@end

@implementation YCLLoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNav];
    [self setupContentView];
}

//状态栏为白色
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;

}

/**
 创建导航栏
 */
- (void)setupNav
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden = YES;
    UIImageView *navView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 270)];
    navView.image = [UIImage imageNamed:@"bg_denglu1"];
    [self.view addSubview:navView];
    
    UILabel *titleL = [[UILabel alloc]init];
    titleL.text = @"登录";
    titleL.textColor = [UIColor whiteColor];
    titleL.font = [UIFont systemFontOfSize:18];
    [titleL sizeToFit];
    titleL.center = CGPointMake(navView.centerX, StatusBarHeight + 44/2);
    [self.view addSubview:titleL];
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setTitle:@"关闭" forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    backBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, StatusBarHeight, 44, 44);
    [self.view addSubview:backBtn];
    
    UIImageView *bgImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_denglu"]];
    self.bgImageView = bgImg;
    [self.view addSubview:bgImg];
    
    [bgImg sizeToFit];
    CGFloat bgImgW = ScreenW - 22;
    CGFloat bgImgH = (bgImgW/bgImg.width) * bgImg.height;
    
    bgImg.frame = CGRectMake(11, 87, bgImgW, bgImgH);
}

//创建内容View
- (void)setupContentView
{
    YCLAccountPasswordView *accountLoginView = [[YCLAccountPasswordView alloc]initWithModelManager:self.modelManager];
    accountLoginView.delegate = self;
    [self.view addSubview:accountLoginView];
    CGFloat segmentControY;
    if (IS_IPHONE_5) {
        segmentControY = self.bgImageView.y + 70;
        
    }else if (IS_IPHONE_6P)
    {
        segmentControY = self.bgImageView.y + 102;
        
    }else
    {
        segmentControY = self.bgImageView.y + 92;
    }
    accountLoginView.frame = CGRectMake(38, segmentControY, self.bgImageView.width - 27*2, 355);
    [accountLoginView updateLayout];
    self.accountLoginView = accountLoginView;
}


//返回按钮的点击方法
- (void)backAction
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//注册按钮的点击方法
- (void)clickRegBtn:(UIButton *)Btn
{
    //点击了注册
}

//点击并且登录成功
- (void)clickLoginSucceBtn:(UIButton *)btn
{
    //登录成功
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeMoneyIcon" object:nil];
    [self backAction];
}


#pragma mark -- lazy
- (YCLLoginModelManager *)modelManager
{
    if (!_modelManager) {
        _modelManager = [[YCLLoginModelManager alloc]init];
    }
    return _modelManager;
}

@end
