
//
//  YCLLoginModelManager.m
//  Lottery
//
//  Created by 李宗帅 on 2017/5/31.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLLoginModelManager.h"

@implementation YCLLoginModelManager

- (YCLInputmodel*)obtainSMSPhoneInputModel
{
    YCLInputmodel *model = [[YCLInputmodel alloc]init];
    model.imageStr = @"icon_phone";
    model.titleStr = @"请输入手机号";
    model.type = YCL_LOGIN_INPUT_PHONE;
    return model;
}

- (YCLInputmodel *)obtainSMSAuthInputModel
{
    YCLInputmodel *model = [[YCLInputmodel alloc]init];
    model.imageStr = @"icon_validation";
    model.titleStr = @"请输入短信验证码";
    model.type = YCL_LOGIN_INPUT_AUTHCODE;
    return model;
}

- (YCLInputmodel *)obtainAccountPhoneInputModel
{
    YCLInputmodel *model = [[YCLInputmodel alloc]init];
    model.imageStr = @"icon_phone";
    model.titleStr = @"请输入手机号";
    model.type = YCL_LOGIN_INPUT_PHONE;
    return model;
}

- (YCLInputmodel *)obtainAccountPassWordInputModel
{
    YCLInputmodel *model = [[YCLInputmodel alloc]init];
    model.imageStr = @"icon_password";
    model.titleStr = @"请输入登录密码";
    model.type = YCL_LOGIN_INPUT_PASSWORD;
    return model;
}

- (YCLInputmodel *)obtainSetAccountPassWordInputModel
{
    YCLInputmodel *model = [[YCLInputmodel alloc]init];
    model.imageStr = @"icon_password";
    model.titleStr = @"请设置登录密码";
    model.type = YCL_LOGIN_INPUT_PASSWORD;
    return model;
}

/**
 账号密码登录方法
 
 @param phone 手机号
 @param authCode 验证码
 @param succe 成功的回调
 @param failure 失败的回调
 */
- (void)loginWithPhone:(NSString *)phone
              password:(NSString *)pwd
                 succe:(void(^)(NSString *message,NSDictionary *data))succe
               failure:(void(^)(NSString *message))failure
{

}





- (void)setPasswordWithUserID:(NSString *)userid
                       passwd:(NSString *)passswd
                        token:(NSString *)token
                        succe:(void(^)(NSString *message))succe
                      failure:(void(^)(NSString *message))failure
{

}
@end
