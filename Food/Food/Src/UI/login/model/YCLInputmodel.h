//
//  YCLInputmodel.h
//  Lottery
//
//  Created by 李宗帅 on 2017/5/31.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    YCL_LOGIN_INPUT_REG, //注册手机号输入框
    YCL_LOGIN_INPUT_PHONE,//登录手机号输出框
    YCL_LOGIN_INPUT_AUTHCODE,//验证码输入框
    YCL_LOGIN_INPUT_PASSWORD,//密码输入框
} YCL_LOGIN_INPUT_TYPE;

@interface YCLInputmodel : NSObject

@property (nonatomic, copy) NSString *imageStr;//标识图

@property (nonatomic, copy) NSString *titleStr;//占位文字

@property (nonatomic, assign) YCL_LOGIN_INPUT_TYPE type;//输入框类型

@end
