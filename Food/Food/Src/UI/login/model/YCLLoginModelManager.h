//
//  YCLLoginModelManager.h
//  Lottery
//
//  Created by 李宗帅 on 2017/5/31.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YCLInputmodel.h"
@protocol YCLLoginSubViewDelegate <NSObject>

//点击登录按钮
- (void)clickLoginSucceBtn:(UIButton *)btn;


@end

@interface YCLLoginModelManager : NSObject

@property (nonatomic,copy) NSString *phoneNum;

@property (nonatomic,copy) NSString *authCode;

@property (nonatomic,copy) NSString *password;


/**
 设置密码

 @param userid 用户ID
 @param passswd 密码
 @param token token
 @param succe 成功
 @param failure 失败
 */
- (void)setPasswordWithUserID:(NSString *)userid
                       passwd:(NSString *)passswd
                        token:(NSString *)token
                        succe:(void(^)(NSString *message))succe
                      failure:(void(^)(NSString *message))failure;




//账号密码登录
- (void)loginWithPhone:(NSString *)phone
              password:(NSString *)pwd
                 succe:(void(^)(NSString *message,NSDictionary *data))succe
               failure:(void(^)(NSString *message))failure;




/**
 验证验证码是否正确

 @param phone 手机号
 @param authCode 验证码
 @param succe 成功的回调
 @param failure 失败的回调
 */
- (void)checkAuthCodeWithPhone:(NSString *)phone
                      authCode:(NSString *)authCode
                         succe:(void(^)(NSString *message))succe
                       failure:(void(^)(NSString *message))failure;


/**
 短信验证登录手机号输入框模型

 @return <#return value description#>
 */
- (YCLInputmodel*)obtainSMSPhoneInputModel;


/**
 短信验证登录验证码输入框模型

 @return <#return value description#>
 */
- (YCLInputmodel *)obtainSMSAuthInputModel;


/**
 账号登录账号输入框模型

 @return <#return value description#>
 */
- (YCLInputmodel *)obtainAccountPhoneInputModel;


/**
 账号登录密码输入框模型

 @return <#return value description#>
 */
- (YCLInputmodel *)obtainAccountPassWordInputModel;


/**
 获取设置登录密码的模型

 @return <#return value description#>
 */
- (YCLInputmodel *)obtainSetAccountPassWordInputModel;

@end
