//
//  YCLLoginInputBoxView.m
//  Lottery
//
//  Created by 李宗帅 on 2017/5/31.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLLoginInputBoxView.h"

#define kPHONEMaxLength 0  //手机号限制长度
#define kCODEMaxLength 0 //密码限制长度
#define kAUTHCODELength 4 //验证码限制长度

@interface YCLLoginInputBoxView ()<UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *imageView;//标题图片
@property (nonatomic, strong) UIButton *authBtn;//密文按钮

@property (nonatomic, strong) YCLInputmodel *model;

@end
@implementation YCLLoginInputBoxView

- (instancetype)initWithModel:(YCLInputmodel *)model
{
    if (self = [super init]) {
        
        [self setupSubViewWithModel:model];
    }
    
    return self;
}

//返回当前输入文本的方法
- (NSString *)getCurrenInputText
{
    return self.descTextFiled.text;
}

- (void)setupSubViewWithModel:(YCLInputmodel *)model
{
    self.model = model;
    _imageView = [[UIImageView alloc]init];
    _imageView.image = [UIImage imageNamed:model.imageStr];
    [self addSubview:_imageView];
    [_imageView sizeToFit];
    
    _descTextFiled = [[UITextField alloc]init];
    _descTextFiled.placeholder = model.titleStr;
    _descTextFiled.delegate = self;
    if (IS_IPHONE_5) {
        _descTextFiled.font = [UIFont systemFontOfSize:12];

    }else
    {
        _descTextFiled.font = [UIFont systemFontOfSize:14];
    }
    [self addSubview:_descTextFiled];
    
    _timeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //给按钮加一个白色的板框
    //给按钮设置弧度,这里将按钮变成了圆形
    _timeBtn.layer.cornerRadius = 15.0f;
    _timeBtn.backgroundColor = LTXDefaultsLineColor;
    _timeBtn.layer.masksToBounds = YES;
    [_timeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    _timeBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [_timeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_timeBtn addTarget:self action:@selector(getAuthCode) forControlEvents:UIControlEventTouchUpInside];
    _timeBtn.enabled = NO;
    [self addSubview:_timeBtn];
    
    _authBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_authBtn setImage:[UIImage imageNamed:@"icon_eyes-1"] forState:UIControlStateSelected];
    [_authBtn setImage:[UIImage imageNamed:@"icon_eyes1"] forState:UIControlStateNormal];
    [_authBtn addTarget:self action:@selector(eyeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_authBtn];
    
    _clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_clearBtn setImage:[UIImage imageNamed:@"icon_delete"] forState:UIControlStateNormal];
    [_clearBtn addTarget:self action:@selector(clickClearBtn:) forControlEvents:UIControlEventTouchUpInside];
    _clearBtn.hidden = YES;
    [self addSubview:_clearBtn];
    
}

- (void)updateLayout
{
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.width.equalTo(@(self.imageView.width));
        make.centerY.equalTo(self);
    }];
    
    [_authBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(13));
        make.width.equalTo(@(20));
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(- 20);
    }];
    
    
    [_timeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self).offset(-2);
        make.height.equalTo(@(30));
        make.width.equalTo(@(90));
        make.right.equalTo(self).offset(-20);
    }];
    
    [_clearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        if (self.model.type == YCL_LOGIN_INPUT_AUTHCODE) {
            //验证码输入框
            make.right.equalTo(self.timeBtn.mas_left).offset(- 10);
        }
        else if (self.model.type == YCL_LOGIN_INPUT_PASSWORD)
        {
            //密码输入框
            make.right.equalTo(self.authBtn.mas_left).offset(- 10);
        }else
        {
            make.right.equalTo(self).offset(- 20);
        }
        make.width.height.equalTo(@(20));
        make.centerY.equalTo(self);
    }];
    
    [_descTextFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.equalTo(self);
        make.left.equalTo(self.imageView.mas_right).offset(10);
        make.right.equalTo(self.clearBtn.mas_left).offset(10);
    }];
    
    if (YCL_LOGIN_INPUT_AUTHCODE == self.model.type) {
      //_descTextFiled.keyboardType = UIKeyboardTypeNumberPad;
        _authBtn.hidden = YES;
    }else if (YCL_LOGIN_INPUT_PASSWORD == self.model.type)
    {
        _descTextFiled.secureTextEntry = YES;
        _descTextFiled.keyboardType = UIKeyboardTypeDefault;
        _timeBtn.hidden = YES;
    }else
    {
      //  _descTextFiled.keyboardType = UIKeyboardTypeNumberPad;
        _authBtn.hidden = YES;
        _timeBtn.hidden = YES;
    }
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = LTXDefaultsLineColor;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.height.equalTo(@(0.5));
        make.bottom.equalTo(self);
    }];
}


/**
 清除按钮的点击方法

 @param btn <#btn description#>
 */
- (void)clickClearBtn:(UIButton *)btn
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textChangeDidWithLength:textFiled:)]) {
        [self.delegate textChangeDidWithLength:@"" textFiled:self.descTextFiled];
    }
    self.descTextFiled.text = @"";
    self.clearBtn.hidden = YES;
}


/**
 获取验证码
 */
- (void)getAuthCode
{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputBoxView:clickGetAuthCodeBtn:)]) {
        [self.delegate inputBoxView:self clickGetAuthCodeBtn:_timeBtn];
    }
}



/**
 明密文转换方法

 @param sender 转换按钮
 */
- (void)eyeBtnClick:(UIButton *)sender
{
    sender.selected = !sender.selected;
    self.descTextFiled.secureTextEntry = !sender.selected;
    
    NSString *text = self.descTextFiled.text;
    self.descTextFiled.text = @" ";
    self.descTextFiled.text = text;
    
    if (self.descTextFiled.secureTextEntry)
    {
        [self.descTextFiled insertText:self.descTextFiled.text];
    }
}

#pragma mark -- delegate


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.descTextFiled)
    {
        if (self.descTextFiled.secureTextEntry)
        {
            [textField insertText:self.descTextFiled.text];
        }
    }
}

//当用户按下return键或者按回车键，keyboard消失
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}



//监听输入长度
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    return YES;
    TTDPRINT(@"输入的长度 = %ld",textField.text.length);
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (toBeString.length > 0) {
        _clearBtn.hidden = NO;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textChangeDidWithLength:textFiled:)]) {
        NSString *tempStr = toBeString;
//        if (toBeString.length > kPHONEMaxLength - 1 && range.length!=1)
//        {
//            tempStr = [toBeString substringToIndex:kPHONEMaxLength];
//
//        }
        [self.delegate textChangeDidWithLength:tempStr textFiled:textField];
    }
    switch (self.model.type)
    {
        case YCL_LOGIN_INPUT_AUTHCODE:
        {
//            if (toBeString.length > kAUTHCODELength && range.length !=1) {
//
//                textField.text = [toBeString substringToIndex:kAUTHCODELength];
//                return NO;
//            }
            return YES;
        }
            break;
        case YCL_LOGIN_INPUT_REG:
        case YCL_LOGIN_INPUT_PHONE:
        {
            if (toBeString.length > kPHONEMaxLength && range.length!=1)
            {
                textField.text = [toBeString substringToIndex:kPHONEMaxLength];
                return NO;
                
            }
            return YES;
        }
            break;
            
        case YCL_LOGIN_INPUT_PASSWORD:
        {
            if (toBeString.length > kCODEMaxLength && range.length !=1) {
                
                textField.text = [toBeString substringToIndex:kCODEMaxLength];
                return NO;
            }
            return YES;
        }
            break;
            
        default:
            break;
    }
    
    return YES;

}


@end
