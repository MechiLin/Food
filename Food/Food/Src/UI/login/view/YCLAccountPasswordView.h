//
//  YCLAccountPasswordView.h
//  Lottery
//
//  Created by 李宗帅 on 2017/6/1.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCLLoginModelManager.h"

@interface YCLAccountPasswordView : UIView

@property (nonatomic, weak) id<YCLLoginSubViewDelegate>delegate;

@property (nonatomic, copy) NSString *phoneNum;

//初始化
- (instancetype)initWithModelManager:(YCLLoginModelManager *)modelManager;
//更新布局
- (void)updateLayout;
@end
