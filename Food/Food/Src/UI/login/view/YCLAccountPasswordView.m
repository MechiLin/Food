//
//  YCLAccountPasswordView.m
//  Lottery
//
//  Created by 李宗帅 on 2017/6/1.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLAccountPasswordView.h"
#import "YCLLoginInputBoxView.h"
#import "HDBmobFramework.h"
@interface YCLAccountPasswordView ()<YCLLoginInputBoxViewDelegate>

@property (nonatomic, strong) YCLLoginModelManager *modelManager;

@property (nonatomic, strong) YCLLoginInputBoxView *phoneInput;

@property (nonatomic, strong) YCLLoginInputBoxView *authCodeView;

@property (nonatomic, strong) UIButton *loginBtn;

@property (nonatomic, strong) UIButton *regBtn;//注册按钮
@end

@implementation YCLAccountPasswordView



- (instancetype)initWithModelManager:(YCLLoginModelManager *)modelManager
{
    if (self = [super init]) {
        
        self.modelManager = modelManager;
        [self setupSubView];
    }
    return self;
}

- (void)setupSubView
{
    //短信验证 手机输入框
    _phoneInput = [[YCLLoginInputBoxView alloc]initWithModel:[self.modelManager obtainAccountPhoneInputModel]];
    _phoneInput.delegate = self;
    [self addSubview:_phoneInput];
    
    _authCodeView = [[YCLLoginInputBoxView alloc]initWithModel:[self.modelManager obtainAccountPassWordInputModel]];
    _authCodeView.delegate = self;
    [self addSubview:_authCodeView];
    
    _loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    _loginBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [_loginBtn setBackgroundImage:[UIImage imageNamed:@"btn_denglu"] forState:UIControlStateNormal];
    [_loginBtn addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    [_loginBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#FFC7AE"] frame:CGRectMake(0, 0, 300, 40)] forState:UIControlStateDisabled];
    //给按钮设置弧度,这里将按钮变成了圆形
    _loginBtn.layer.cornerRadius = 22.0f;
    _loginBtn.layer.masksToBounds = YES;
    _loginBtn.enabled = YES;
    [self addSubview:_loginBtn];
    
    _regBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_regBtn addTarget:self action:@selector(clickRegisterBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_regBtn setTitle:@"快速注册" forState:UIControlStateNormal];
    _regBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16];

    [_regBtn setTitleColor:LTXDefaultsColor forState:UIControlStateNormal];
    _regBtn.layer.borderColor = [LTXDefaultsColor CGColor];
    _regBtn.layer.borderWidth = 1.0f;
    _regBtn.layer.cornerRadius = 22.0f;
    _regBtn.layer.masksToBounds = YES;
//    [self addSubview:_regBtn];
    
}

- (void)updateLayout
{
    [_phoneInput mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        if (IS_IPHONE_5) {
            make.top.equalTo(self).offset(25);
        }
        else if(IS_IPHONE_6P)
        {
            make.top.equalTo(self).offset(45);
        }else
        {
            make.top.equalTo(self).offset(35);
        }
        make.height.equalTo(@(50));
    }];
    
    [_phoneInput updateLayout];
    
    [_authCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.height.equalTo(self.phoneInput);
        if (IS_IPHONE_5) {
            
            make.top.equalTo(self.phoneInput.mas_bottom).offset(20);
        }else
        {
            make.top.equalTo(self.phoneInput.mas_bottom).offset(25);
        }
    }];
    [_authCodeView updateLayout];
    
    
    [_loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.height.equalTo(@(50));
        if (IS_IPHONE_5) {
            make.top.equalTo(self.authCodeView.mas_bottom).offset(20);
        }else if(IS_IPHONE_6P)
        {
            make.top.equalTo(self.authCodeView.mas_bottom).offset(40);
        }else
        {
            make.top.equalTo(self.authCodeView.mas_bottom).offset(30);

        }
    }];

//    [_regBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.width.centerX.equalTo(self.loginBtn);
//        make.top.equalTo(self.loginBtn.mas_bottom).offset(15);
//    }];
}


/**
 登录的方法
 
 @param loginBtn 登录按钮
 */
- (void)loginAction:(UIButton *)loginBtn
{
//    if (![NSString checkTel:[self.phoneInput getCurrenInputText]]) return;
//    if (![NSString checkPassword:[self.authCodeView getCurrenInputText]])return;
//    //验证手机号 之后发送登录请求
//
//
//    //通知上层控制器dissmiss
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickLoginSucceBtn:)]) {

        [self.delegate clickLoginSucceBtn:loginBtn];
    }
    NSString *userName = [self.phoneInput getCurrenInputText];
    NSString *pwd = [self.authCodeView getCurrenInputText];
    
    [BmobDataInstance loginWithName:userName pwd:pwd complete:^(NSError * _Nullable error) {
        if (error) {
            return;
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickLoginSucceBtn:)]) {
            
            [self.delegate clickLoginSucceBtn:loginBtn];
        }
    }];

}

- (void)textChangeDidWithLength:(NSString *)string textFiled:(UITextField *)textFiled
{
    
    //手机号满11位 而且密码或者验证码大于要求  登录按钮才可点击
    if (self.phoneInput.descTextFiled == textFiled)
    {
        self.modelManager.phoneNum = string;

        if ((self.authCodeView.descTextFiled.text.length>=6 && string.length >= 11)) {
            
            self.loginBtn.enabled = YES;
            
        }else
        {
            self.loginBtn.enabled = NO;
        }
    }
    
    if (self.authCodeView.descTextFiled ==textFiled) {
        
        if ((self.phoneInput.descTextFiled.text.length>=11 && string.length >= 6)) {
            
            self.loginBtn.enabled = YES;
            
        }else
        {
            self.loginBtn.enabled = NO;
        }
    }
    
    if (string.length >= 1) {
        textFiled.font = [UIFont systemFontOfSize:16];
    }else
    {
        textFiled.font = [UIFont systemFontOfSize:14];
    }
    
}

- (void)clickRegisterBtn:(UIButton *)btn
{
    //点击了注册
}

- (void)clickForgetPwdBtn:(UIButton *)Btn
{
    //点击了忘记密码
}

- (void)inputBoxView:(YCLLoginInputBoxView *)inputView clickGetAuthCodeBtn:(UIButton *)btn
{
    //这个view没有获取验证码  所以为空
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}

- (void)setPhoneNum:(NSString *)phoneNum
{
    _phoneNum = phoneNum;
    

    self.phoneInput.descTextFiled.text = phoneNum;
}
@end
