//
//  YCLLoginInputBoxView.h
//  Lottery
//
//  Created by 李宗帅 on 2017/5/31.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCLInputmodel.h"
@class YCLLoginInputBoxView;
@protocol YCLLoginInputBoxViewDelegate <NSObject>

- (void)textChangeDidWithLength:(NSString *)string textFiled:(UITextField *)textFiled;

@optional

//点击获取验证码的代理方法
- (void)inputBoxView:(YCLLoginInputBoxView *)inputView clickGetAuthCodeBtn:(UIButton *)btn;

@end

@interface YCLLoginInputBoxView : UIView

@property (nonatomic, strong) UIButton *clearBtn;//清除按钮

@property (nonatomic, strong) UIButton *timeBtn;//计时按钮


//代理
@property (nonatomic, weak) id<YCLLoginInputBoxViewDelegate>delegate;

@property (nonatomic, strong) UITextField *descTextFiled;//占位文字

//初始化的方法
- (instancetype)initWithModel:(YCLInputmodel *)model;

//布局
- (void)updateLayout;

//返回当前输入文本的方法
- (NSString *)getCurrenInputText;
@end
