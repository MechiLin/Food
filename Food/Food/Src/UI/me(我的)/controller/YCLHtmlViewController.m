//
//  YCLHtmlViewController.m
//  Lottery
//
//  Created by szdl on 2017/7/3.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLHtmlViewController.h"

@interface YCLHtmlViewController ()


@end

@implementation YCLHtmlViewController

- (void)viewDidLoad {
    [super viewDidLoad];

   }


- (void)createLeftView
{
    self.navigationController.navigationBar.barTintColor = APP_DEFAULT_COLOR;
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 44, 44);
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    [button addTarget:self action:@selector(closeWindow:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"关闭" forState:UIControlStateNormal];
    [button setTitleColor:JSRGBColor(0xff, 0xff, 0xff) forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button];
}


- (void)closeWindow:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
