//
//  YCLAbstractViewController.m
//  Lottery
//
//  Created by szdl on 2017/6/7.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLAbstractViewController.h"
#import "YCLEmptyDataView.h"
#import "UINavigationItem+YCLMargin.h"

@interface YCLAbstractViewController ()

@property (nonatomic, strong) UIButton *backBtn;

@end

@implementation YCLAbstractViewController

#pragma mark - LifeCycle

- (void)dealloc {
    [self unobserveAllNotifications];
    TTDPRINT(@"\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"
             "     %@   ------->   dealloc                  \n"
             "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n",[self class]);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc]init]];
    self.view.backgroundColor = JSRGBColor(0xf7, 0xf7, 0xf7);
    
    [self setupNavigation];
    
}


#pragma mark - Private methods

- (void)setupNavigation {
    [self createLeftView];
    [self createTitleView];
    [self createRightView];
}



- (void)createLeftView
{
    UIBarButtonItem *itemBar = [[UIBarButtonItem alloc] initWithCustomView:self.leftItemView];
    [self.navigationItem setupLeftBarButtonItem:itemBar];
}

- (void)createRightView
{
    UIBarButtonItem *itemBarB = [[UIBarButtonItem alloc]initWithCustomView:self.rightItemView];
    [self.navigationItem setupRightBarButtonItem:itemBarB];
}

- (void)createTitleView
{
    [self.navigationItem setTitleView:self.parentNavView];
}

- (void)setNavigationBackground:(BOOL)isTranparent {
    
    if (isTranparent) {
        //自定义背景
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
        //消除阴影
        self.navigationController.navigationBar.shadowImage = [UIImage new];
    }
    else {
        [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        //消除阴影
        self.navigationController.navigationBar.shadowImage = nil;
        
    }
    
}

- (void)addEmptyDataView {
    
    YCLEmptyDataView* view = [self.view viewWithTag:EMPTY_DATA_VIEW_TAG];
    if (view == nil) {
        NSString* fileName = [self getEmptyImageFileName];
        view = [[YCLEmptyDataView alloc]initWithImageFile:fileName];
        view.backgroundColor = JSRGBColor(0xff, 0xff, 0xff);
        view.tag = EMPTY_DATA_VIEW_TAG;
        [self.view addSubview:view];
        
        __weak typeof(self)weakSelf = self;
        
        view.reloadDataBlock = ^{
            [weakSelf reloadData];
        };
    
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.view.mas_left).offset(0);
            make.right.equalTo(weakSelf.view.mas_right).offset(0);
            
            make.top.equalTo(weakSelf.view.mas_top).offset(SafeAreaTopHeight);
            make.bottom.equalTo(weakSelf.view.mas_bottom).offset(0);
        }];
        
    }
    
    [self.view bringSubviewToFront:view];
}


- (NSString*)getEmptyImageFileName {
    //@"pic_empty"
    return @"";
}


- (void)removeEmptyDataView {
    
    YCLEmptyDataView* view = [self.view viewWithTag:EMPTY_DATA_VIEW_TAG];
    if (view != nil) {
        [view removeFromSuperview];
    }
}

- (void)reloadData {
    
    
}

- (void)addRightItemView:(UIView *)rightView{
    if (rightView) {
        [self.rightItemView addSubview:rightView];
        [self.rightItemView setFrame:rightView.frame];
        [self.rightItemView setNeedsLayout];
        [self adjustTitleFrame];
        UIBarButtonItem *itemBarB = [[UIBarButtonItem alloc]initWithCustomView:self.rightItemView];
        [self.navigationItem setupRightBarButtonItem:itemBarB];
    }
}

- (void)addLeftItemView:(UIView *)leftView{
    if (leftView) {
        [self.leftItemView setFrame:leftView.frame];
        [self.leftItemView addSubview:leftView];
        [self.leftItemView setNeedsLayout];
        [self adjustTitleFrame];
        UIBarButtonItem *itemBar = [[UIBarButtonItem alloc] initWithCustomView:self.leftItemView];
        [self.navigationItem setupLeftBarButtonItem:itemBar];
    }
}

- (void)adjustTitleFrame {
    if (self.rightItemView.width > self.leftItemView.width) {
        self.leftItemView.width = self.rightItemView.width;
        [self.leftItemView setNeedsLayout];
    } else if(self.rightItemView.width < self.leftItemView.width){
        self.rightItemView.width = self.leftItemView.width;
        [self.rightItemView setNeedsLayout];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

#pragma mark - Events

- (void)popupViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Properties

- (UIView *)parentNavView{
    if (_parentNavView) {
        return _parentNavView;
    }
    _parentNavView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 180, 44)];
    [_parentNavView setBackgroundColor:[UIColor clearColor]];
    _parentNavView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    return _parentNavView;
}

- (UIView *)leftItemView{
    if (_leftItemView) {
        return _leftItemView;
    }
    _leftItemView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [_leftItemView addSubview:self.backBtn];
    return _leftItemView;
}

- (UIView *)rightItemView{
    if (_rightItemView) {
        return _rightItemView;
    }
    _rightItemView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    return _rightItemView;
}

- (UIButton *)backBtn
{
    if (!_backBtn) {
        
        UIImage *image = [UIImage imageNamed:@"navSsqBackButtonTip"];
        UIImage *pressImage =[UIImage imageNamed:@"navSsqBackButtonTip"];
        _backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [_backBtn setImage:image forState:UIControlStateNormal];
        [_backBtn setImage:pressImage forState:UIControlStateHighlighted];
        [_backBtn addTarget:self action:@selector(popupViewController) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}

@end
