//
//  ReceiveGoodsVC.h
//  Food
//
//  Created by 李宗帅 on 2018/8/10.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "LTXBaseViewController.h"
#import "WPAddressListCell.h"
typedef enum : NSUInteger {
    ChooseAddress,//选择
    LookAddress//编辑
} AddressViewType;



@interface ReceiveGoodsVC : LTXBaseViewController

@property(nonatomic,copy)void(^settingBack)(HDAddressObject * model);
@property(nonatomic,copy)void(^settingBlock)(HDAddressObject * model);
@property(nonatomic,copy)void(^editBlock)(void);//如果编辑过，点击返回就会调用这个block

/** 地址被清空时调用 */
@property (nonatomic, copy) void(^addressesClearedBlcok)(void);
@property (nonatomic, assign) AddressViewType type;
@property (nonatomic, assign) PUSH_FROM_TYPE pushType;//从哪里跳入的


@end
