//
//  ReceiveGoodsVC.m
//  Food
//
//  Created by 李宗帅 on 2018/8/10.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "ReceiveGoodsVC.h"
#import "WPAddressListCell.h"
#import "LTXAddressListModelManager.h"
#import "WPAddressManagmentViewController.h"
static NSString *const cellID = @"WPAddressListCell";

@interface ReceiveGoodsVC ()
<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate, WPAddressListCellDelegate,LTXAddressListModelManagerDelegate>
{
    NSString * _currentIsOn;
    int _page;
    int _deleNum;
}

@property (nonatomic, weak) UIView *tipsView;
@property(nonatomic,assign)BOOL isEdit;//是否编辑
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) LTXAddressListModelManager *modelManager;

@end

@implementation ReceiveGoodsVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = NO;
    _page=1;
    self.title=@"管理收货地址";
    self.view.backgroundColor = ZQColor(238, 238, 238);
    
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    //新增地址
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"添加" style:UIBarButtonItemStyleBordered target:self action:@selector(addAddressAction)];
    self.navigationItem.rightBarButtonItem = item;
    
    [self createTableView];
    [self createTipsView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}


- (void)updateListData
{
    [self.tableView reloadData];
    if (self.modelManager.dataArr.count == 0) {
        self.tipsView.hidden = NO;
        if (self.addressesClearedBlcok) {
            self.addressesClearedBlcok();
        }
    }else
    {
        self.tipsView.hidden = YES;
    }
}

- (void)createTableView
{
    _tableView=[[UITableView alloc]init];
    [_tableView registerClass:[WPAddressListCell class] forCellReuseIdentifier:cellID];
    _tableView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
    _tableView.frame = CGRectMake(0, 0, ScreenW, ScreenH);
    _tableView.height += 50;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
}

- (void)createTipsView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH)];
    
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(ScreenW/2-38, 140, 76, 76)];
    img.image = [UIImage imageNamed:@"no_address"];
    [view addSubview:img];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(ScreenW/2-100, CGRectGetMaxY(img.frame) + 20, 200, 25)];
    lb.text = @"您还没有收货地址哦~";
    lb.font = [UIFont systemFontOfSize:13];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.textColor = ZQColor(102, 102, 102);
    [view addSubview:lb];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(ScreenW/2-45, CGRectGetMaxY(lb.frame)+20, 90, 30);
    btn.backgroundColor = ZQColor(252, 80, 69);
    btn.layer.cornerRadius = 4;
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn setTitle:@"添加新地址" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(addAddressAction) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    
    self.tipsView = view;
    [self.view addSubview:_tipsView];
    _tipsView.hidden = YES;
}

- (void)leftButtonPressed
{
    if(self.modelManager.dataArr.count != 0)
    {
        HDAddressObject * model = self.modelManager.dataArr[0];
        if (self.settingBack) {
            self.settingBack(model);
        }
        [MBProgressHUD hideHUD];
        
    }
    
    //判断是否被编辑过,如果编辑过要回调此block，让上级页面刷新数据
    if (self.isEdit) {
        
        if (self.editBlock) {
            self.editBlock();
        }
    }
    
    [self.navigationController  popViewControllerAnimated:YES];
}

- (void)addAddressAction
{
    
    WPAddressManagmentViewController *userAddressVC = [[WPAddressManagmentViewController alloc] init];
    userAddressVC.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:userAddressVC animated:YES];
}


#pragma mark - WPAddressListCellDelegate
- (void)addressListCell:(WPAddressListCell *)cell operationWithType:(WPAddressLisetCellOperationType)type {
    switch (type) {
        case WPAddressLisetCellOperationTypeEdit:{
            // 编辑
        }
            break;
        case WPAddressLisetCellOperationTypeDelete:{
            // 删除
            [LTX_Common showAlertWithTitle:nil msg:@"确认要删除该收货地址吗？" actions:@[@"取消",@"确定"] viewController:self callBack:^(NSString *title) {
                
                if ([title isEqualToString:@"确定"]) {
                    HDAddressObject *model = self.modelManager.dataArr[cell.currentIndexPath.row];
                    //删除操作
                    [HDAddressObject deleteAddressObjectWithObjectId:model.objectId];
                }
            }];
            
        }
            break;
            
        default:{
        }
            break;
    }
}

#pragma mark - tableview的数据源方法

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelManager.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WPAddressListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.operationDelegate = self;
    cell.currentIndexPath = indexPath;
    cell.cellType = kWTOType;
    cell.pushType = self.pushType;
    HDAddressObject *entity = self.modelManager.dataArr[indexPath.row];
    [cell updaCellWithAddressEntity:entity];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_pushType == PUSH_FROM_CHANNEL) {
        
        return;
    }
    
    if (_type == ChooseAddress) {
        
        HDAddressObject *model = self.modelManager.dataArr [indexPath.row];
        if (self.settingBack) {
            self.settingBack(model);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - ----- 数据请求方法 ------

//删除或者设置为默认收货地址
- (void)deleteChooseAddress:(int)index
{
    HDAddressObject *model = self.modelManager.dataArr[index];
    [HDAddressObject deleteAddressObjectWithObjectId:model.objectId];
    if (self.modelManager.dataArr.count == 0) {
        _tipsView.hidden = NO;
    }
}


#pragma mark - ----- UIAlertViewDelegate -----

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        NSLog(@"%d",_deleNum);
    }else if (buttonIndex==1) {
        NSLog(@"!!!!!!!%d",_deleNum);
        [self deleteChooseAddress:_deleNum];
    }
}

- (LTXAddressListModelManager *)modelManager
{
    if (!_modelManager) {
        _modelManager = [[LTXAddressListModelManager alloc]init];
        _modelManager.delegate = self;
    }
    return _modelManager;
}

@end
