//
//  WPAddressManagmentViewController.m
//  Food
//
//  Created by 李宗帅 on 2018/8/11.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "WPAddressManagmentViewController.h"
#import "HDAddressObject.h"
@interface WPAddressManagmentViewController ()

@property (nonatomic, strong) UITextField *nickNameField;

@property (nonatomic, strong) UITextField *telNumField;

@property (nonatomic, strong) UITextField *addressField;

@property (nonatomic, strong) UIButton *addBtn;//添加按钮

@end

@implementation WPAddressManagmentViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = NO;
    
    self.view.backgroundColor = LTXBgColor;
    
    self.title = @"添加地址";
    [self.view addSubview:self.nickNameField];
    [self.view addSubview:self.telNumField];
    [self.view addSubview:self.addressField];
    [self.view addSubview:self.addBtn];
}


- (void)clickAddAction
{
    
    if (self.nickNameField.text.length <= 0) {
        showMessageAutoMiss(@"请输入您的姓名");
        return;
    }
    
    
    if (self.telNumField.text.length <= 0) {
        showMessageAutoMiss(@"请输入您的联系方式");
        return;
    }
    
    
    if (self.addressField.text.length <= 0) {
        showMessageAutoMiss(@"请输入您的地址");
        return;
    }
    [[HDBmobFramework sharedInstance] addAddressWithRealName:self.nickNameField.text mobile:self.telNumField.text detailAddress:self.addressField.text complete:^(NSError * _Nullable error) {
        
        showMessageAutoMiss(@"添加成功");
        [self.navigationController popViewControllerAnimated:YES];
    }];
}


- (UITextField *)nickNameField
{
    if (!_nickNameField) {
        _nickNameField = [[UITextField alloc]initWithFrame:CGRectMake(15, SafeAreaTopHeight + 15, ScreenW - 30, 50)];
        _nickNameField.placeholder = @"请输入姓名";
        _nickNameField.keyboardType = UIKeyboardTypeDefault;
        
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, _nickNameField.height - 1, _nickNameField.width, 1)];
        lineView.backgroundColor = LTXDefaultsLineColor;
        [_nickNameField addSubview:lineView];
    }
    return _nickNameField;
}

- (UITextField *)telNumField
{
    if (!_telNumField) {
        _telNumField = [[UITextField alloc]initWithFrame:CGRectMake(self.nickNameField.x, MaxY(self.nickNameField.frame) + 10, self.nickNameField.width, self.nickNameField.height)];
        _telNumField.placeholder = @"请输入联系电话";
        _telNumField.keyboardType = UIKeyboardTypeDefault;
        
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, _telNumField.height - 1, _telNumField.width, 1)];
        lineView.backgroundColor = LTXDefaultsLineColor;
        [_telNumField addSubview:lineView];
    }
    
    return _telNumField;
}

- (UITextField *)addressField
{
    if (!_addressField) {
        _addressField = [[UITextField alloc]initWithFrame:CGRectMake(self.nickNameField.x, MaxY(self.telNumField.frame) + 10, self.nickNameField.width, self.nickNameField.height)];
        
        _addressField.placeholder = @"请输入详细地址";
        _addressField.keyboardType = UIKeyboardTypeDefault;
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, _addressField.height - 1, _addressField.width, 1)];
        lineView.backgroundColor = LTXDefaultsLineColor;
        [_addressField addSubview:lineView];
    }
    
    return _addressField;
}

- (UIButton *)addBtn
{
    if (!_addBtn) {
        _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addBtn setTitle:@"添加地址" forState:UIControlStateNormal];
        [_addBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _addBtn.backgroundColor = LTXDefaultsColor;
        [_addBtn addTarget:self action:@selector(clickAddAction) forControlEvents:UIControlEventTouchUpInside];
        _addBtn.frame = CGRectMake(20, MaxY(self.addressField.frame) + 40, ScreenW - 40, 40);
        ViewRadius(_addBtn, 20);
    }
    return _addBtn;
}

@end
