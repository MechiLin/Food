//
//  YCLAbstractViewController.h
//  Lottery
//
//  Created by szdl on 2017/6/7.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>

#define EMPTY_DATA_VIEW_TAG 2311

@interface YCLAbstractViewController : UIViewController

- (void)setNavigationBackground:(BOOL)isTranparent;

- (void)popupViewController;
- (void)createLeftView;

- (void)addEmptyDataView;
- (void)removeEmptyDataView;

/**
 *  navigationBar,自定义title视图
 */
@property (nonatomic, strong) UIView *parentNavView;

/**
 *  左边按钮
 */
@property (nonatomic, strong) UIView *leftItemView;

/**
 *  右边按钮
 */
@property (nonatomic, strong) UIView *rightItemView;

//自定义左边按钮
- (void)addLeftItemView:(UIView *)leftView;

//自定义右边按钮
- (void)addRightItemView:(UIView *)rightView;

@end
