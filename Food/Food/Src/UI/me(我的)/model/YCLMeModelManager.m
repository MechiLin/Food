//
//  YCLMeModelManager.m
//  Lottery
//
//  Created by 李宗帅 on 2017/5/27.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLMeModelManager.h"

@implementation YCLMeModelManager

/**
 收货地址和优惠
 
 @return 模型数组
 */
- (NSArray *)obtainMyIncomeModels
{
    YCLMeDescModel *model1 = [[YCLMeDescModel alloc]init];
    model1.icon = @"icon_pushsettings_80x80";
    model1.title = @"收货地址";
    model1.type = MyClickType_Income;
    
    YCLMeDescModel *model2 = [[YCLMeDescModel alloc]init];
    model2.icon = @"icon_redpacket_80x80";
    model2.title = @"我的优惠";
    model2.type = MyClickType_Gathering_Code;
    return @[model1,model2];
}


@end
