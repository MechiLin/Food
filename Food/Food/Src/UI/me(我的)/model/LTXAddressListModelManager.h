//
//  LTXAddressListModelManager.h
//  Food
//
//  Created by 李宗帅 on 2018/8/10.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "LTXBaseModelManager.h"
#import "HDAddressObject.h"

@protocol LTXAddressListModelManagerDelegate <NSObject>

- (void)updateListData;

@end

@interface LTXAddressListModelManager : LTXBaseModelManager

@property (nonatomic, weak) id<LTXAddressListModelManagerDelegate>delegate;


@property (nonatomic, strong) NSArray *dataArr;

@end
