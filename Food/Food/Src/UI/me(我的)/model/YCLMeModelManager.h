//
//  YCLMeModelManager.h
//  Lottery
//
//  Created by 李宗帅 on 2017/5/27.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YCLMeDescView.h"

@interface YCLMeModelManager : NSObject

/**
 收货地址和优惠
 
 @return 模型数组
 */
- (NSArray *)obtainMyIncomeModels;



@end
