//
//  LTXAddressListModelManager.m
//  Food
//
//  Created by 李宗帅 on 2018/8/10.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "LTXAddressListModelManager.h"

@interface LTXAddressListModelManager ()

@property (nonatomic, strong) RLMNotificationToken *addressToken;

@property (nonatomic, copy) RLMResults *allAddressObj;

@end

@implementation LTXAddressListModelManager



- (instancetype)init
{
    if (self = [super init])
    {
        _allAddressObj = [HDAddressObject allObjects];
        _addressToken = self.addressToken;
    }
    return self;
}

- (RLMNotificationToken *)addressToken
{
    
    if (!_addressToken) {
        WS(weakSelf);
        _addressToken = [self.allAddressObj addNotificationBlock:^(RLMResults * _Nullable results, RLMCollectionChange * _Nullable change, NSError * _Nullable error) {
            
            if ([weakSelf.delegate respondsToSelector:@selector(updateListData)])
            {
                RLMResults *results = [HDAddressObject allObjects];
                NSMutableArray *arr = [NSMutableArray array];
                
                for (HDAddressObject *obj in results) {

                    HDAddressObject *model = [[HDAddressObject alloc]init];
                    model.objectId = obj.objectId;
                    model.realName = obj.realName;
                    model.mobile = obj.mobile;
                    model.detailAddress = obj.detailAddress;
                    model.userObjectId = obj.userObjectId;
                    [arr addObject:model];
                }
                weakSelf.dataArr = arr;
                [weakSelf.delegate updateListData];
            }
        }];
    }
    return _addressToken;
}


@end
