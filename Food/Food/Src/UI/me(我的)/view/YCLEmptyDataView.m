//
//  YCLEmptyDataCell.m
//  Lottery
//
//  Created by szdl on 2017/7/21.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLEmptyDataView.h"

@interface YCLEmptyDataView ()<UIGestureRecognizerDelegate>
{
    UIImageView* _imageView;
    UILabel* _tipLabel;
}
@end


@implementation YCLEmptyDataView


- (instancetype)initWithImageFile:(NSString*)fileName {
    
    
    self = [super init];
    if (self != nil) {

        _imageView = [[UIImageView alloc]init];
        _imageView.image = [UIImage imageNamed:fileName];
        [self addSubview:_imageView];
        
        _tipLabel = [[UILabel alloc]init];
        _tipLabel.textAlignment = NSTextAlignmentCenter;
        _tipLabel.text = @"";
        _tipLabel.textColor = JSRGBColor(0x33, 0x33, 0x33);
        _tipLabel.font = [UIFont systemFontOfSize:16];
        
        [self addSubview:_tipLabel];
        
        
        UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [self addGestureRecognizer:singleTap];
        
        singleTap.delegate = self;
        singleTap.cancelsTouchesInView = NO;
        
    }
    
    return self;
}



-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGFloat width = _imageView.image.size.width;
    CGFloat height = _imageView.image.size.height;
    
    CGFloat xpos = (self.frame.size.width - width) / 2;
    CGFloat yPos = height + 10 + 16;
    
    yPos = (self.frame.size.height - yPos) / 2;
    _imageView.frame = CGRectMake(xpos, yPos, width, height);

    _tipLabel.frame = CGRectMake(xpos, _imageView.frame.origin.y + _imageView.frame.size.height + 10, width, 16);

}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}



-(void)handleSingleTap:(UITapGestureRecognizer *)sender {
    //CGPoint point = [sender locationInView:self];
    if (_reloadDataBlock != nil) {
        _reloadDataBlock();
    }
}


@end
