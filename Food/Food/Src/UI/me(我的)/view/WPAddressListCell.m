//
//  WPAddressListCell.m
//  WolianwPro
//
//  Created by 邓雨来 on 2016/12/22.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WPAddressListCell.h"
#import <Masonry/Masonry.h>


@interface WPAddressListCell ()

@property (nonatomic, weak) UILabel *nameLabel;
@property (nonatomic, weak) UILabel *phoneLabel;
@property (nonatomic, weak) UILabel *addLabel;
@property (nonatomic, weak) UILabel *promptLabel;
@property (nonatomic, weak) UIView *lineView;
@property (nonatomic, weak) UIButton *defaultBtn;

@end

@implementation WPAddressListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).offset(10);
            make.left.right.bottom.mas_equalTo(self);
        }];
        
        CGFloat leftMargin = 16;
//        CGFloat buttonGap = 5;
        //姓名
        UILabel *nameLabel = [[UILabel alloc] init];
        nameLabel.font = [UIFont systemFontOfSize:16];
        nameLabel.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
        [self.contentView addSubview:nameLabel];
        self.nameLabel = nameLabel;
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(19);
            make.left.mas_equalTo(leftMargin);
        }];
        
        //电话
        UILabel *phoneLabel = [[UILabel alloc] init];
        phoneLabel.font=[UIFont systemFontOfSize:16];
        phoneLabel.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
        [self.contentView addSubview:phoneLabel];
        self.phoneLabel = phoneLabel;
        [phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(nameLabel.mas_right).offset(5);
            make.top.mas_equalTo(nameLabel);
            make.right.mas_lessThanOrEqualTo(self.contentView).offset(-leftMargin);
        }];
        
        //地址
        UILabel *addLabel = [[UILabel alloc] init];
        addLabel.font=[UIFont systemFontOfSize:14];
        addLabel.textColor=[UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0];
        [self.contentView addSubview:addLabel];
        self.addLabel = addLabel;
        [addLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(nameLabel.mas_bottom).offset(20);
            make.left.mas_equalTo(nameLabel);
            make.right.mas_equalTo(-leftMargin);
        }];
        
        // 提示升级文字
        UILabel *promptLabel = [[UILabel alloc] init];
        promptLabel.text = @"地址库升级，旧地址不可用，请重新编辑";
        promptLabel.font=[UIFont systemFontOfSize:12];
        promptLabel.textColor=[UIColor redColor];
        [self.contentView addSubview:promptLabel];
        self.promptLabel = promptLabel;
        [promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(addLabel);
            make.top.mas_equalTo(addLabel.mas_bottom).offset(12);
        }];
        
        // 分割线
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = [UIColor colorWithRed:228/255.0 green:228/255.0 blue:228/255.0 alpha:1.0];
        [self.contentView addSubview:lineView];
        self.lineView = lineView;
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.contentView);
            make.height.mas_equalTo(1);
            make.top.mas_equalTo(promptLabel.mas_bottom).offset(12);
        }];
        
        // 设为默认
        UIButton *defaultBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        defaultBtn.tag = WPAddressLisetCellOperationTypeDefault;
        [defaultBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [defaultBtn setImage:[UIImage imageNamed:@"receivenoselect.png"] forState:UIControlStateNormal];
        [defaultBtn setImage:[UIImage imageNamed:@"receiveselect.png"] forState:UIControlStateSelected];
        defaultBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [defaultBtn setTitle:@"设为默认" forState:UIControlStateNormal];
        [defaultBtn setTitleColor:[UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
        defaultBtn.imageEdgeInsets = UIEdgeInsetsMake(10, 5, 10, 65);

        [self.contentView addSubview:defaultBtn];
        self.defaultBtn = defaultBtn;
        [defaultBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(90);
            make.top.mas_equalTo(lineView.mas_bottom).offset(4);
        }];
        
        // 设为默认label
//        UILabel *defaultLabel = [[UILabel alloc] init];
//        defaultLabel.text = @"设为默认";
//        defaultLabel.font=[UIFont systemFontOfSize:14];
//        defaultLabel.textColor=[UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0];;
//        [self.contentView addSubview:defaultLabel];
//        [defaultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(defaultBtn.mas_right).offset(buttonGap);
//            make.top.bottom.mas_equalTo(defaultBtn);
//        }];

        // 删除
//        UILabel *deleteLabel = [[UILabel alloc] init];
//        deleteLabel.text = @"删除";
//        deleteLabel.font=[UIFont systemFontOfSize:14];
//        deleteLabel.textColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0];
//        [self.contentView addSubview:deleteLabel];
//        [deleteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.bottom.mas_equalTo(defaultBtn);
//            make.right.mas_equalTo(self.contentView).offset(-leftMargin);
//        }];

        
        UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        deleteBtn.tag = WPAddressLisetCellOperationTypeDelete;
        [deleteBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [deleteBtn setImage:[UIImage imageNamed:@"address_del"] forState:UIControlStateNormal];
        deleteBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        [deleteBtn setTitleColor:[UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
        deleteBtn.imageEdgeInsets = UIEdgeInsetsMake(11, 5, 12, 35);
        [self.contentView addSubview:deleteBtn];
        [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(defaultBtn);
            make.height.mas_equalTo(defaultBtn.mas_height);
            make.right.mas_equalTo(self.contentView).offset(-leftMargin);
            make.width.mas_equalTo(60);
        }];
        
        // 编辑
//        UILabel *editLabel = [[UILabel alloc] init];
//        editLabel.text = @"编辑";
//        editLabel.font=[UIFont systemFontOfSize:14];
//        editLabel.textColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0];
//        [self.contentView addSubview:editLabel];
//        [editLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.top.mas_equalTo(deleteBtn);
//            make.right.mas_equalTo(deleteBtn.mas_left).offset(-30);
//        }];

//        UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        editBtn.tag = WPAddressLisetCellOperationTypeEdit;
//        [editBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
//        [editBtn setImage:[UIImage imageNamed:@"address_edit"] forState:UIControlStateNormal];
//
//        editBtn.titleLabel.font = [UIFont systemFontOfSize:14];
//        [editBtn setTitle:@"编辑" forState:UIControlStateNormal];
//        [editBtn setTitleColor:[UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
//        editBtn.imageEdgeInsets = UIEdgeInsetsMake(11, 5, 12, 35);
//
//        [self.contentView addSubview:editBtn];
//        [editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.mas_equalTo(deleteBtn.mas_left).offset(-30);
//            make.centerY.mas_equalTo(deleteBtn);
//            make.height.mas_equalTo(defaultBtn.mas_height);
//            make.width.mas_equalTo(60);
//        }];

    }
    return self;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    WPAddressListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WPAddressListCell"];
    return cell;
}

- (void)buttonClick:(UIButton *)button {
    WPAddressLisetCellOperationType type = button.tag;
    if (self.operationDelegate && [self.operationDelegate respondsToSelector:@selector(addressListCell:operationWithType:)]) {
        [self.operationDelegate addressListCell:self operationWithType:type];
    }
}

- (void)updaCellWithAddressEntity:(HDAddressObject *)addressEntity {
    
    if (self.cellType == kNormalType) {
        
        self.defaultBtn.hidden = NO;
        
    }else{
        
        self.defaultBtn.hidden = YES;
    }
    
    //昵称

    self.nameLabel.text = addressEntity.realName;
    
    self.addLabel.text = addressEntity.detailAddress;
    self.phoneLabel.text = addressEntity.mobile;
    self.defaultBtn.selected = NO;
    
    //无需考虑是否在配送范围内
    self.promptLabel.hidden = YES;
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.addLabel.mas_bottom).offset(21);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];    
}


@end
