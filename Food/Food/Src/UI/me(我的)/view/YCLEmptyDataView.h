//
//  YCLEmptyDataCell.h
//  Lottery
//
//  Created by szdl on 2017/7/21.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^payButtonBlock) (void);


@interface YCLEmptyDataView : UIView
- (instancetype)initWithImageFile:(NSString*)fileName;

@property(copy, nonatomic)payButtonBlock reloadDataBlock;
@end
