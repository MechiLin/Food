//
//  YCLMeDescView.h
//  Lottery
//
//  Created by 李宗帅 on 2017/5/27.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YCLMeDescModel,YCLMeDescView;

typedef NS_ENUM(NSUInteger, MyClickType) {
    MyClickType_TopUp = 0, //充值
    MyClickType_Extract, //提款
    MyClickType_AllOrder, //全部订单
    MyClickType_FailOrder, //失败订单
    MyClickType_PushSet, //推送设置
    MyClickType_SecuCenter, //安全中心
    MyClickType_Problem, //常见问题
    MyClickType_MoreSet, //更多设置
    MyClickType_Income,
    MyClickType_Gathering_Code,
    MyClickType_redEnvelope,//红包
};

@protocol YCLMeDescViewDelegate <NSObject>


/**
 点击了某个模块的代理方法

 @param meDescView 当前的view
 @param btn 点击的板块
 */
- (void)meDescView:(YCLMeDescView *)meDescView clickCurrenPlateType:(MyClickType)type;

@end
@interface YCLMeDescView : UIView

@property (nonatomic, strong) UILabel *descLable1;//前边的描述

@property (nonatomic, strong) UILabel *descLable2;//后边的描述

@property (nonatomic, weak) id<YCLMeDescViewDelegate>delegate;

/**
 生成我的内容栏的方法

 @param models 包含2个'YCLMeDescModel'元素的数组
 @return YCLMeDescView
 */
- (instancetype)initWithModels:(NSArray <YCLMeDescModel *>*)models;

@end

@interface YCLMeDescModel : NSObject

/**
 图片
 */
@property (nonatomic, copy) NSString *icon;


/**
 标题
 */
@property (nonatomic, copy) NSString *title;


/**
 描述
 */
@property (nonatomic, copy) NSString *descStr;


/**
 点击的类型
 */
@property (nonatomic, assign) MyClickType type;

@end
