//
//  YCLMeDescView.m
//  Lottery
//
//  Created by 李宗帅 on 2017/5/27.
//  Copyright © 2017年 Aesthetic92. All rights reserved.
//

#import "YCLMeDescView.h"

@interface YCLMeDescView ()

@property (nonatomic, strong) UIImageView *iconView1;//标题图1


@property (nonatomic, strong) UILabel *titleLable1;//标题1

@property (nonatomic, strong) UIImageView *iconView2;//标题图2


@property (nonatomic, strong) UILabel *titleLable2;//标题2

@property (nonatomic, strong) NSArray *dataSource;//数据源

@property (nonatomic, strong) UIButton *leftBtn;

@property (nonatomic, strong) UIButton *rightBtn;

//@property (nonatomic, strong) UIImageView *firstView;
//
//@property (nonatomic, strong) UIImageView *secondView;

@end

@implementation YCLMeDescView

//初始化方法
- (instancetype)initWithModels:(NSArray<YCLMeDescModel *> *)models
{
    if (self = [super init]) {
        
        self.dataSource = models;
        [self setupView];
        [self setupLayout];
    }
    return self;
}

//创建view
- (void)setupView
{
    if (self.dataSource.count < 2) {
        return;
    }
    self.backgroundColor = [UIColor clearColor];
    
    YCLMeDescModel *model1 = self.dataSource[0];
    
    UIImageView *firstImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"content_bg_140"]];
    [self addSubview:firstImg];
    
    UIImageView *secondImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"content_bg_140"]];
    [self addSubview:secondImg];
    
    self.iconView1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:model1.icon]];
    [self addSubview:self.iconView1];
    self.titleLable1 = [[UILabel alloc]init];
    self.titleLable1.textColor = [UIColor blackColor];
    self.titleLable1.font = [UIFont systemFontOfSize:15];
    self.titleLable1.textAlignment = NSTextAlignmentLeft;
    self.titleLable1.text = model1.title;
    [self.titleLable1 sizeToFit];
    [self addSubview:self.titleLable1];
    
    self.descLable1 = [[UILabel alloc]init];
    self.descLable1.textColor = LTXDefaultsTextColor;
    self.descLable1.font = [UIFont systemFontOfSize:12];
    self.descLable1.textAlignment = NSTextAlignmentLeft;
    self.descLable1.text = model1.descStr;
    [self.descLable1 sizeToFit];
    [self addSubview:self.descLable1];
    
    UIButton *firstBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:firstBtn];
    [firstBtn addTarget:self action:@selector(clickFirstBtn) forControlEvents:UIControlEventTouchUpInside];
    self.leftBtn = firstBtn;
    [firstBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.top.height.equalTo(self);
        make.width.equalTo(@(ScreenW/2));
    }];
    
    
    YCLMeDescModel *model2 = self.dataSource[1];
    self.iconView2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:model2.icon]];
    [self addSubview:self.iconView2];
    self.titleLable2 = [[UILabel alloc]init];
    self.titleLable2.textColor = [UIColor blackColor];
    self.titleLable2.font = [UIFont systemFontOfSize:14];
    self.titleLable2.textAlignment = NSTextAlignmentLeft;
    self.titleLable2.text = model2.title;
    [self.titleLable2 sizeToFit];
    [self addSubview:self.titleLable2];
    
    self.descLable2 = [[UILabel alloc]init];
    self.descLable2.textColor = LTXDefaultsTextColor;
    self.descLable2.font = [UIFont systemFontOfSize:12];
    self.descLable2.textAlignment = NSTextAlignmentLeft;
    self.descLable2.text = model2.descStr;
    [self.descLable2 sizeToFit];
    [self addSubview:self.descLable2];
    
    UIButton *secondBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:secondBtn];
    [secondBtn addTarget:self action:@selector(clickSecondBtn) forControlEvents:UIControlEventTouchUpInside];
    
    self.rightBtn = secondBtn;
    [secondBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.top.height.equalTo(self);
        make.width.equalTo(@(ScreenW/2));
    }];
    
    [firstImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(firstBtn);
        make.left.equalTo(firstBtn).offset(5);
        make.right.equalTo(firstBtn).offset(-0.5);
    }];
    
    [secondImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(secondBtn);
        make.left.equalTo(secondBtn).offset(0.5);
        make.height.width.equalTo(firstImg);
    }];
}

- (void)setupLayout
{
    if (self.dataSource.count < 2)
    {
        return;
    }
    YCLMeDescModel *model1 = self.dataSource[0];
    [self.iconView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        if (IS_IPHONE_5)
        {
            make.left.equalTo(self).offset(10);
        }else
        {
            make.left.equalTo(self).offset(26);
        }
        make.top.equalTo(self).offset(19);
        make.width.equalTo(@(47));
        make.height.equalTo(@(48));
    }];
    
    
    
    if (model1.descStr.length) {
        //有描述的布局
        [self.titleLable1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_centerY).offset(- 4);
            make.left.equalTo(self.iconView1.mas_right).offset(17);
        }];
        [self.descLable1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_centerY).offset(4);
            make.left.equalTo(self.titleLable1);
            make.right.equalTo(self.leftBtn);
        }];
        
    }else
    {
        //没有描述的布局
        [self.titleLable1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.iconView1.mas_right).offset(17);
        }];
        self.descLable1.frame = CGRectZero;
    }
    
    
    YCLMeDescModel *model2 = self.dataSource[1];
    [self.iconView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.iconView1);
        if (IS_IPHONE_5) {
            make.left.equalTo(self.leftBtn.mas_right).offset(10);
        }else
        {
            make.left.equalTo(self.leftBtn.mas_right).offset(21);
        }
        make.width.equalTo(@(47));
        make.height.equalTo(@(48));
    }];
    
    if (model2.descStr.length) {
        //有描述布局
        [self.titleLable2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_centerY).offset(- 4);
            make.left.equalTo(self.iconView2.mas_right).offset(17);
        }];
        
        [self.descLable2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_centerY).offset(4);
            make.left.equalTo(self.titleLable2);
            make.right.equalTo(self.rightBtn).offset(- 10);
        }];
    }else
    {
        //没有描述布局
        //没有描述的布局
        [self.titleLable2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.iconView2.mas_right).offset(17);
        }];
        self.descLable2.frame = CGRectZero;
    }
}


/**
 点击了第一个按钮
 */
- (void)clickFirstBtn
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(meDescView:clickCurrenPlateType:)])
    {
        YCLMeDescModel *model1 = self.dataSource[0];
        [self.delegate meDescView:self clickCurrenPlateType:model1.type];
    }
}


/**
 点击第二个按钮
 */
- (void)clickSecondBtn
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(meDescView:clickCurrenPlateType:)])
    {
        YCLMeDescModel *model2 = self.dataSource[1];
        [self.delegate meDescView:self clickCurrenPlateType:model2.type];
    }
}

@end

@implementation YCLMeDescModel


@end
