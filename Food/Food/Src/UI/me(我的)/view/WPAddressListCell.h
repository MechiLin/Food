//
//  WPAddressListCell.h
//  WolianwPro
//
//  Created by 邓雨来 on 2016/12/22.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HDAddressObject.h"

typedef NS_ENUM(NSInteger,WPAddressCellType) {
    
    kNormalType = 0,//普通类型
    kWTOType,//外卖类型
};

typedef NS_ENUM(NSInteger, WPAddressLisetCellOperationType) {
    WPAddressLisetCellOperationTypeDefault, // 设为默认
    WPAddressLisetCellOperationTypeEdit,    //  编辑
    WPAddressLisetCellOperationTypeDelete   //  删除
};

typedef NS_ENUM(NSUInteger, PUSH_FROM_TYPE) {
    PUSH_FROM_ORDER,//来自确认订单
    PUSH_FROM_CHANNEL,//来自频道首页
};

@class WPAddressListCell;
@protocol WPAddressListCellDelegate <NSObject>

- (void)addressListCell:(WPAddressListCell *)cell operationWithType:(WPAddressLisetCellOperationType)type;

@end
@interface WPAddressListCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *currentIndexPath;
@property (nonatomic, weak) id <WPAddressListCellDelegate> operationDelegate;
@property (nonatomic,assign)WPAddressCellType cellType;//cell类型

@property (nonatomic, assign) PUSH_FROM_TYPE pushType;//从哪里跳入的

+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (void)updaCellWithAddressEntity:(HDAddressObject *)addressEntity;

@end
