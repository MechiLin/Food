//
//  WTOShopCarCacheData.m
//  WolianwPro
//
//  Created by lishuaiWLW on 17/1/4.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTOShopCarCacheData.h"
#define WTOSHOP_CAR_CACHE @"shopCarCacheData"
@interface WTOShopCarCacheData ()


@end

@implementation WTOShopCarCacheData

static WTOShopCarCacheData * WTOShopCarCacheDataManager = nil;
//创建单例
+ (instancetype)sharedShopCarCache
{ 
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
            
        WTOShopCarCacheDataManager = [[super allocWithZone:NULL] init] ;;
        
        WTOShopCarCacheDataManager.shopCarCache = [NSMutableDictionary dictionary];
        
    });
        
    return WTOShopCarCacheDataManager;
}

+(id) allocWithZone:(struct _NSZone *)zone {
    
    return [WTOShopCarCacheData sharedShopCarCache] ;
    
}

-(id) copyWithZone:(struct _NSZone *)zone {
    
    return [WTOShopCarCacheData sharedShopCarCache] ;
    
}

#pragma mark - getter and setter PromotionData
//缓存店铺信息
-(void)saveStorePromotionData:(NSArray *)promotionArray
{
    
    [WTOShopCarCacheDataManager.shopCarCache setValue:[promotionArray copy] forKey:[NSString stringWithFormat:@"promotionData"]];
}

//根据店铺id获取店铺促销信息
-(NSArray *)getStorePromotionData
{
    NSArray *promotionArray = [WTOShopCarCacheDataManager.shopCarCache valueForKey:[NSString stringWithFormat:@"promotionData"]];
    return promotionArray;
}

#pragma mark - getter and setter GoodsData
//缓存订单数据  以storeID为key
- (void)saveOrderData:(NSArray *)orderData forStoreID:(NSString *)storeID
{
    if (!orderData.count) {
        return;
    }
    
    [WTOShopCarCacheDataManager.shopCarCache setValue:[orderData mutableCopy] forKey:storeID];
}


//从缓存中移除
-(void)removeOrderDataWithStoreid:(NSString *)storeid
{
    if ([WTOShopCarCacheDataManager.shopCarCache.allKeys containsObject:storeid]) {
        [WTOShopCarCacheDataManager.shopCarCache removeObjectForKey:storeid];
    }
}

//清空商品和促销数据
- (void)removeShopCarCache
{
    NSMutableArray *keys = [[WTOShopCarCacheDataManager.shopCarCache allKeys] mutableCopy];
    if ([keys containsObject:@"promotionData"]) {
        [keys removeObject:@"promotionData"];
    }
    
    if (keys.count) {
        
        [WTOShopCarCacheDataManager.shopCarCache removeObjectsForKeys:[keys copy]];
    }
}

@end
