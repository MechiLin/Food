//
//  MQLHomeModelManager.h
//  Food
//
//  Created by 李宗帅 on 2018/8/8.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "LTXBaseModelManager.h"
#import "HDFCategoryObject.h"
#import "HDFFoodObject.h"
#import "HDAddressObject.h"
#import "HDOrderObject.h"
@protocol MQLHomeModelManagerDelegate <NSObject>

- (void)updateListData;

@end

@interface MQLHomeModelManager : LTXBaseModelManager

@property (nonatomic, strong) NSArray *categoryArr;

@property (nonatomic, strong) NSMutableArray *carArr;

@property (nonatomic, weak) id<MQLHomeModelManagerDelegate>delegate;

@property (nonatomic, strong) HDAddressObject *addressObj;


//获取餐盒费和配送费
-(NSArray *)getFareDataSourceWithEntity;//自己自由添加咯

@end
