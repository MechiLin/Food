//
//  MQLHomeModelManager.m
//  Food
//
//  Created by 李宗帅 on 2018/8/8.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "MQLHomeModelManager.h"


@interface MQLHomeModelManager ()

@property (nonatomic, strong) RLMNotificationToken *foodToken;

@property (nonatomic, copy) RLMResults *allFoodObj;

@end

@implementation MQLHomeModelManager

- (instancetype)init
{
    if (self = [super init])
    {
        _allFoodObj = [HDFCategoryObject listOrderByAscendingWithPriority];
        _foodToken = self.foodToken;
    }
    return self;
}

- (RLMNotificationToken *)foodToken
{
    
    if (!_foodToken) {
        WS(weakSelf);
        _foodToken = [self.allFoodObj addNotificationBlock:^(RLMResults * _Nullable results, RLMCollectionChange * _Nullable change, NSError * _Nullable error) {
            
            if ([weakSelf.delegate respondsToSelector:@selector(updateListData)])
            {
                RLMResults *results = [HDFCategoryObject listOrderByAscendingWithPriority];
                NSMutableArray *arr = [NSMutableArray array];
                
                for (HDFCategoryObject *obj in results) {
                    
                    MQLCategoryObject *model = [[MQLCategoryObject alloc]init];
                    model.objectId = obj.objectId;
                    model.name = obj.name;
                    model.priority = obj.priority;
                    model.updatedAt = obj.updatedAt;
                    model.foodArr = obj.foodArr;
                    if (obj.foodArr.count > 0) {
                        [arr addObject:model];
                    }
                }
                weakSelf.categoryArr = arr;
                [weakSelf.delegate updateListData];
            }
        }];
    }
    return _foodToken;
}

//获取餐盒费和配送费
-(NSArray *)getFareDataSourceWithEntity
{
    
    NSMutableArray *fareArray = [[NSMutableArray alloc] init];
    
//    if ([entity.ordertotalamount floatValue] > 0) {
//
//        WTOpackFareEntity *fareEntity = [[WTOpackFareEntity alloc] init];
//        fareEntity.fareName = @"小计";
//        fareEntity.fareMoney = [NSString stringWithFormat:@"%.2f",[entity.promotionTotalPrice doubleValue] - [disCountAmount doubleValue]];
//        [fareArray addObject:fareEntity];
//    }
//
//    if ([entity.orderpackingfare floatValue] > 0) {
//        WTOpackFareEntity *fareEntity = [[WTOpackFareEntity alloc] init];
//        fareEntity.fareName = @"餐盒费";
//        fareEntity.fareMoney = entity.orderpackingfare.stringValue;
//        [fareArray addObject:fareEntity];
//    }
//
    WTOpackFareEntity *fareEntity = [[WTOpackFareEntity alloc] init];
    fareEntity.fareName = @"配送费";
    fareEntity.fareMoney = @"$0.0";
    [fareArray addObject:fareEntity];
    
    return [fareArray copy];
}

- (NSMutableArray *)carArr
{
    if (!_carArr) {
        _carArr = [NSMutableArray array];
    }
    
    return _carArr;
}


- (HDAddressObject *)addressObj
{
    if (!_addressObj) {
        _addressObj = [HDAddressObject allObjects].lastObject;
    }
    return _addressObj;
}

@end
