//
//  WTOShopCarCacheData.h
//  WolianwPro
//
//  Created by lishuaiWLW on 17/1/4.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WTOShopCarCacheData : NSObject

//购物车缓存数据 单例设计  杀死App即清空  storeID为key  value为购物车数组信息
@property (nonatomic, strong) NSMutableDictionary *shopCarCache;

//创建单例
+ (instancetype)sharedShopCarCache;


//缓存订单数据  以storeID为key
- (void)saveOrderData:(NSArray *)orderData forStoreID:(NSString *)storeID;

//从缓存中移除某个店铺的所有购物车商品
-(void)removeOrderDataWithStoreid:(NSString *)storeid;

//清空数据
- (void)removeShopCarCache;

//缓存店铺信息
-(void)saveStorePromotionData:(NSArray *)promotionArray;

//根据店铺id获取店铺促销信息
-(NSArray *)getStorePromotionData;

@end
