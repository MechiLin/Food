//
//  MQLHomeViewController.h
//  Food
//
//  Created by 李宗帅 on 2018/8/8.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "LTXBaseViewController.h"
#import "WTOGoodsListTabBarView.h"
@interface MQLHomeViewController : LTXBaseViewController

//购物车
@property (nonatomic, strong, readonly) WTOGoodsListTabBarView *shopCartView;

@property (nonatomic,assign) NSUInteger totalOrders;//购物车一共点的数量

@property (nonatomic, copy, readonly) NSString *storeID;//店铺ID

@property (nonatomic, copy, readonly) NSString *userID; //该店铺对应的用户ID

@end
