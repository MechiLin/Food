//
//  MQLHomeViewController.m
//  Food
//
//  Created by 李宗帅 on 2018/8/8.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "MQLHomeViewController.h"
#import "WPCustomScrollView.h"
#import "MJRefresh.h"
#import "WTAHomePageHeaderView.h"
#import "WTOShopCarCacheData.h"
#import "WTOCustomNavBar.h"
#import "WTOGoodsListLeftTableViewCell.h"
#import "WLWGoodsListTabViewCell.h"
#import "WTOCoodsListTableViewHeaderView.h"
#import "MQLHomeModelManager.h"
#import "YCLLoginViewController.h"
#import "WTOSureOrderViewController.h"
#define kWTOShopGoodsHeadHeight 346/2
@interface MQLHomeViewController ()<UIScrollViewDelegate,WTOGoodsListTabBarViewDelegate,WTAHomePageHeaderViewDelegate,WTOCustomNavBarDelegate,UITableViewDelegate,UITableViewDataSource,CAAnimationDelegate,MQLHomeModelManagerDelegate>

@property(nonatomic,assign)BOOL canScroll;//能不能滚动?
@property (nonatomic, strong) UIImageView *bgImageView;//头部背景图片
@property(nonatomic,weak)UIScrollView *contentScrollView;
@property (nonatomic, strong)WTAHomePageHeaderView *shopHeardView;//店铺头  WTOShopGoodDetailsHeardView

@property (nonatomic, strong) UILabel *suspendedService;//暂停服务的label

@property (nonatomic,weak) WTOCustomNavBar *navBar;//自定义导航


@property (nonatomic, strong) UITableView *leftTableView;//左边的tableView
@property (nonatomic, strong) UITableView *rightTableView;//右边的tableView

@property (nonatomic, assign) BOOL isBusiness; //是否是营业状态;

@property (nonatomic, strong) NSMutableArray *foodData;//每组下边的模型数据

//绘制放入购物车动画  要记录的点;
@property (nonatomic, assign) CGFloat endPointX;

@property (nonatomic, assign) CGFloat endPointY;

@property (nonatomic,strong) UIBezierPath *path;//动画贝塞尔

@property (nonatomic,strong) CALayer *dotLayer;//绘图

@property (nonatomic,assign) CGRect parentRect;//加号的坐标

@property (nonatomic, strong) UITableView *placeholderV;//占位图

@property (nonatomic,copy) NSString *storeID;
@property (nonatomic,copy) NSString *userID;

@property (nonatomic,assign)BOOL clickLeftS;//是否点击的是左边的cell

@property (nonatomic, strong) MQLHomeModelManager *modelManager;

@end

@implementation MQLHomeViewController
{
    NSInteger _selectIndex;//选中的记录一下
    BOOL _isScrollDown;//滚动的方向
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSubViews];
    [self initCustomNavView];
    
    [self requestData];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    
    [self refreshShoppingCart];//刷新购物车
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.modelManager.carArr = self.shopCartView.getShopCarDataSource.mutableCopy;

}


- (void)refreshShoppingCart
{
    [self.shopCartView setShopCarDataSourceWithArray:self.modelManager.carArr];
    
    [self.shopCartView refreshShopCar];
}

- (void)initCustomNavView
{
    self.isHideLeftBtn = YES;
    WTOCustomNavBar *navBar = [[WTOCustomNavBar alloc] init];
    navBar.delegate = self;
    
    navBar.barType = kAppHomeType;
    
    [self.view addSubview:navBar];
    
    self.navBar = navBar;
    
    [navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.trailing.mas_equalTo(self.view);
        make.height.mas_equalTo(SafeAreaTopHeight);
    }];
}

- (void)initSubViews
{
    self.canScroll = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // 头信息
    [self createAndSetupHeadBackground];
    
    // 全局的Scroll View
    [self createAndSetupScrollview];
    
    
    // 商品区
    [self setupUI];
    
    // 购物车
    [self createAndSetupShoppingCart];
    
    
    [self createAndSetupHeader];
    
    return;
    
}

- (void)updateListData
{
    
    [self createAndSetupHeader];
    WS(weakSelf)
    if (self.modelManager.categoryArr.count) {
        //有值
        _leftTableView.hidden = _rightTableView.hidden = NO;
        _placeholderV.hidden = YES;
        [weakSelf.shopCartView refreshShopCar];
        [weakSelf reloadTable];
        
        //有数据的情况下 让他默认选中第一个 刷新之后也是
        if (weakSelf.modelManager.categoryArr.count){
            [weakSelf.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
        }
        [weakSelf.rightTableView reloadData];
    }
    else
    {
        _leftTableView.hidden = YES;
        _rightTableView.hidden = YES;
        
        _placeholderV.hidden = NO;
    }
}


- (void)requestData
{
    self.modelManager.delegate = self;
    
    [[HDBmobFramework sharedInstance] updateServiceData];//加载数据
}

- (void)createAndSetupHeadBackground
{
    NSArray *imageArray = @[[UIImage imageNamed:@"beijing1"],[UIImage imageNamed:@"BEIJING2"],[UIImage imageNamed:@"beijing3"],[UIImage imageNamed:@"beijing4"],[UIImage imageNamed:@"beijing5"]];
    UIImage *backImage = imageArray[arc4random() % 5];
    _bgImageView = [[UIImageView alloc]initWithImage:backImage];
    [self.view addSubview:_bgImageView];
    [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.left.equalTo(self.view);
        make.height.equalTo(@(346));
    }];
}

- (void)createAndSetupScrollview {
    WPCustomScrollView *contentScrollView = [[WPCustomScrollView alloc] initWithFrame:self.view.bounds];

    contentScrollView.delegate = self;
    contentScrollView.backgroundColor = [UIColor clearColor];
    contentScrollView.delaysContentTouches = NO;
    contentScrollView.scrollEnabled = YES;
    contentScrollView.alwaysBounceVertical = YES;
    contentScrollView.alwaysBounceHorizontal = NO;
    contentScrollView.showsVerticalScrollIndicator = NO;
    contentScrollView.showsHorizontalScrollIndicator = NO;
    contentScrollView.contentSize = CGSizeMake(ScreenW, ScreenH + kWTOShopGoodsHeadHeight);
    [self.view addSubview:contentScrollView];
    self.contentScrollView = contentScrollView;
    
    [self.contentScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    contentScrollView.contentInset = UIEdgeInsetsMake(0, 0, 50 + SafeAreaBottomHeight + self.tabBarController.tabBar.height, 0);
}

- (void)setupUI
{
    
    _selectIndex = 0;
    _isScrollDown = YES;
    
    [self.contentScrollView addSubview:self.placeholderV];
    [self.contentScrollView addSubview:self.leftTableView];
    [self.contentScrollView addSubview:self.rightTableView];
    
    
    CGRect rect = [self.view convertRect:self.shopCartView.shoppingCartBtn.frame fromView:self.shopCartView];
    
    _endPointX = rect.origin.x + 15;
    _endPointY = rect.origin.y + 35 + self.rightTableView.height;
    
}

//添加购物车
- (void)createAndSetupShoppingCart {
    
    _shopCartView = [[WTOGoodsListTabBarView alloc] initWithFrame:CGRectMake(0, ScreenH - self.tabBarController.tabBar.height - SafeAreaBottomHeight - 49, CGRectGetWidth(self.view.bounds), 50) inView:self.view];
    _shopCartView.delegate = self;
    _shopCartView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_shopCartView];
    
}

- (void)createAndSetupHeader {
    
    WTAHomePageHeaderView *headerView = [[WTAHomePageHeaderView alloc] init];
    headerView.delegate = self;
    [self.contentScrollView addSubview:headerView];
    self.shopHeardView = headerView;
    WS(ws);
    
    [headerView updateHeaderViewWithStoreInfoEntity:[HDSellerObject sellerMsg]];
    self.shopHeardView.clickDiscount = ^{
//        [ws.goodsListVC changeSegmentControlSeleted:1];

    };
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self.contentScrollView);
        make.width.mas_equalTo(ScreenW);
        make.height.mas_equalTo(kWTOShopGoodsHeadHeight);
    }];
}


-(void)scrollEnabledNotification:(NSNotification *)notification
{
    self.navBar.title = @"";
    self.canScroll = YES;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView == self.contentScrollView)
    {
        [self.navBar refrehNavBackGroundWithOffSet:scrollView.contentOffset.y];
    }

    static CGFloat lastOffsetY = 0;
    
    UITableView *tableView = (UITableView *) scrollView;
    if (_rightTableView == tableView) {
        _isScrollDown = lastOffsetY < scrollView.contentOffset.y;
        lastOffsetY = scrollView.contentOffset.y;
    }
    
    if (scrollView == self.contentScrollView)
    {
        
        
        if (scrollView.contentOffset.y >= kWTOShopGoodsHeadHeight - SafeAreaTopHeight) {
            
            //设置父的UIScrollView不能滑动
            if (self.canScroll == YES) {
                
                self.canScroll = NO;
                scrollView.contentOffset = CGPointMake(0,kWTOShopGoodsHeadHeight - SafeAreaTopHeight);
            }
            
            if (scrollView.decelerating) {
                
                scrollView.contentOffset = CGPointMake(0,kWTOShopGoodsHeadHeight - SafeAreaTopHeight);
                self.canScroll = NO;
                self.navBar.title = [NSString stringWithFormat:@"%@",[HDSellerObject sellerMsg].name];
            }
            
        }else
        {
            self.canScroll = YES;
        }
        
        if (!self.canScroll) {
            self.navBar.title = [NSString stringWithFormat:@"%@",[HDSellerObject sellerMsg].name];
            scrollView.contentOffset = CGPointMake(0,kWTOShopGoodsHeadHeight - SafeAreaTopHeight);

        }
    }
    
    if (scrollView == _leftTableView || scrollView == _rightTableView) {
        
        if (scrollView.contentOffset.y < 0) {
            self.canScroll = YES;
        }
    }
    
}


- (void)setCanScroll:(BOOL)canScroll
{
    _canScroll = canScroll;
    self.contentScrollView.scrollEnabled = canScroll;
    self.leftTableView.scrollEnabled = self.rightTableView.scrollEnabled = !canScroll;
}

//购物车图片
-(void)setCartImage
{
    [self.shopCartView refreshCartImage];
    
}

//设置当前购物车总金额
-(void)setTotalMoney
{
    [[WTOShopCarCacheData sharedShopCarCache] saveOrderData:[self.shopCartView getShopCarDataSource] forStoreID:[NSString stringWithFormat:@"%@", self.storeID]];
    
    
}

- (UITableView *)placeholderV
{
    if (!_placeholderV) {
        
        _placeholderV = [[UITableView alloc]initWithFrame:CGRectMake(0, kWTOShopGoodsHeadHeight, ScreenW, ScreenH - SafeAreaTopHeight - self.tabBarController.tabBar.height - 50)];
        _placeholderV.delegate = self;
        _placeholderV.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        _placeholderV.backgroundColor = [UIColor whiteColor];
        _placeholderV.contentSize = CGSizeMake(0, ScreenH + 2);
//        _placeholderV.backgroundColor = LTXDefaultsColor;
        UILabel *promptL = [[UILabel alloc]init];
        promptL.text = @"暂无商品数据";
        promptL.font = [UIFont systemFontOfSize:20];
        promptL.textColor = [UIColor lightGrayColor];
        [promptL sizeToFit];
        promptL.center = CGPointMake(ScreenW/2, 200);
        [_placeholderV addSubview:promptL];

        
    }
    return _placeholderV;
}

- (UITableView *)leftTableView
{
    if (!_leftTableView)
    {
        _leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kWTOShopGoodsHeadHeight, 90, ScreenH - SafeAreaTopHeight - self.tabBarController.tabBar.height - 50)];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.rowHeight = 60;
        _leftTableView.tableFooterView = [UIView new];
        _leftTableView.showsVerticalScrollIndicator = NO;
        _leftTableView.contentInset = UIEdgeInsetsMake(0, 0, SafeAreaTopHeight + 50, 0);
        _leftTableView.backgroundColor = ZQColor(248, 248, 248);
        [_leftTableView registerClass:[WTOGoodsListLeftTableViewCell class] forCellReuseIdentifier:kCellIdentifier_Left];
    }
    return _leftTableView;
}


- (UITableView *)rightTableView
{
    if (!_rightTableView)
    {
        _rightTableView = [[UITableView alloc] initWithFrame:CGRectMake(90, kWTOShopGoodsHeadHeight, ScreenW - 90, ScreenH - SafeAreaTopHeight - self.tabBarController.tabBar.height - 50)];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        //        _rightTableView.rowHeight = 200;
        _rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _rightTableView.showsVerticalScrollIndicator = NO;
        _rightTableView.tableFooterView = [[UIView alloc] init];
        _rightTableView.contentInset = UIEdgeInsetsMake(0, 0, SafeAreaTopHeight + 50, 0);
        [_rightTableView registerClass:[WLWGoodsListTabViewCell class] forCellReuseIdentifier:kCellIdentifier_Right];
        
    }
    return _rightTableView;
}




#pragma mark - TableView DataSource Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_leftTableView == tableView)
    {
        return 1;
    }
    else
    {
        return self.modelManager.categoryArr.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_leftTableView == tableView)
    {
        return self.modelManager.categoryArr.count;
        
    }
    else
    {
        MQLCategoryObject *model = self.modelManager.categoryArr[section];
        
        return [model.foodArr count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_leftTableView == tableView)
    {
        WTOGoodsListLeftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier_Left forIndexPath:indexPath];
        MQLCategoryObject *model = self.modelManager.categoryArr[indexPath.row];
        cell.name.text = model.name;
        switch (indexPath.row) {
            case 0: // 热销
                cell.iconView.image = [UIImage imageNamed:@"order_icon_hot"];
                break;
            case 1: // 限时抢购
                cell.iconView.image = [UIImage imageNamed:@"order_icon_timelimit"];
                break;
            case 2: // 代金券抵扣
                cell.iconView.image = [UIImage imageNamed:@"order_icon_vouchers"];
                break;
                
            default:
                cell.iconView.image = nil;
                break;
        }
        
        return cell;
    }
    else
    {
        
        WLWGoodsListTabViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier_Right forIndexPath:indexPath];
        MQLCategoryObject *categoryModel = self.modelManager.categoryArr[indexPath.section];
        MQLFoodModel *model = categoryModel.foodArr[indexPath.row];
        cell.model = model;
        
        //如果是非营业时间  加减和选规格按钮不可被点击
        cell.isBusiness = self.isBusiness;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        __weak __typeof(&*cell)weakCell = cell;
        __weak __typeof(self)weakSelf = self;
        
        cell.plusBlock = ^(NSInteger nCount,BOOL animated)
        {
            
            weakSelf.parentRect = [weakCell convertRect:weakCell.plus.frame toView:self.view];
            
            if (animated)
            {
                weakSelf.totalOrders ++;
                [weakSelf JoinCartAnimationWithRect:weakSelf.parentRect];
                //加
                model.buyCount ++;
                [self storeOrders:model isAdded:YES];
                
            }else
            {
                
                weakSelf.totalOrders --;
                model.buyCount --;
                [self storeOrders:model isAdded:NO];
            }
            [weakSelf.shopCartView setBadgeWithNum:[NSString stringWithFormat:@"%zd",weakSelf.totalOrders]];
        };
        
        return cell;
        
    }
}

-(void)storeOrders:(MQLFoodModel *)model isAdded:(BOOL)added
{
    if (added) {
        //加
        //往购物车添加模型 刷新购物车列表
        [self.shopCartView addFoodModelToShopCar:model];
    }else
    {
        //减
        //往购物车减掉模型 刷新购物车列表
        [self.shopCartView removeFoodModelAtShopCar:model];
    }
}


#pragma mark --- delegate

- (void)clearnShopCar
{
    self.totalOrders = 0;
    [self.rightTableView reloadData];
}

- (void)clickPay:(UIButton *)payBtn
{
    //点击了支付按钮
    if (![HDAccount account]) {
        
        LTXNavgationViewController *loginNav = [[LTXNavgationViewController alloc]initWithRootViewController:[[YCLLoginViewController alloc]init]];
        
        [self presentViewController:loginNav animated:YES completion:nil];
        return;
    }

    if (HDAccount.account.isAdmin) {
        [MBProgressHUD showError:@"商家不能下单"];
        return;
    }
    
    self.modelManager.carArr = self.shopCartView.getShopCarDataSource.mutableCopy;
    //进订单性情页面
    WTOSureOrderViewController *vc = [[WTOSureOrderViewController alloc]initWithState:1];
    vc.goodsArray = [self.shopCartView getShopCarDataSource];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)addGoodsCount:(WTOGoodsListTabBarView *)tabbarView withModel:(MQLFoodModel *)model withIndexPath:(NSIndexPath *)indexPath
{
    //购物车添加物品数量
    [self reloadTable];
}

- (void)reduceGoodsCount:(WTOGoodsListTabBarView *)tabbarView withModel:(MQLFoodModel *)model withIndexPath:(NSIndexPath *)indexPath
{
    [self reloadTable];
}


#pragma mark ----WTOSpecificationsViewDlegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (_rightTableView == tableView)
    {
        return 30;
    }
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (_rightTableView == tableView)
    {
        
        WTOCoodsListTableViewHeaderView *view = [[WTOCoodsListTableViewHeaderView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 30)];
        MQLCategoryObject *model = self.modelManager.categoryArr[section];
        view.name.text = model.name;
        return view;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _leftTableView) {
        
        return 55;
    }else
    {
        //        WTOGoodsCategoryModel *categoryModel = self.categoryData[indexPath.section - 1];
        //        WTOGoodsDetailEntity *model = categoryModel.goodList[indexPath.row];
        //
        //计算行高的  在这不用计算了 一切从简
        CGFloat rowH = 110;
        
        return rowH;
    }
}

// TableView分区标题即将展示
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(nonnull UIView *)view forSection:(NSInteger)section
{
    // 当前的tableView是RightTableView，RightTableView滚动的方向向上，RightTableView是用户拖拽而产生滚动的（（主要判断RightTableView用户拖拽而滚动的，还是点击LeftTableView而滚动的）
    if ((_rightTableView == tableView) && !_isScrollDown && _rightTableView.dragging)
    {
        [self selectRowAtIndexPath:section];
    }
    
}


// TableView分区标题展示结束
- (void)tableView:(UITableView *)tableView didEndDisplayingHeaderView:(UIView *)view forSection:(NSInteger)section
{
    
    // 当前的tableView是RightTableView，RightTableView滚动的方向向下，RightTableView是用户拖拽而产生滚动的（（主要判断RightTableView用户拖拽而滚动的，还是点击LeftTableView而滚动的）
    if ((_rightTableView == tableView) && _isScrollDown && _rightTableView.dragging)
    {
        if (self.modelManager.categoryArr.count > 2) {
            
            [self selectRowAtIndexPath:section + 1];
        }else
        {
            [self selectRowAtIndexPath:section];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    
    if (_leftTableView == tableView)
    {
        self.clickLeftS = YES;
        _selectIndex = indexPath.row;
        [_rightTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:_selectIndex] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    } else {
        
        if (self.isBusiness) {
            
            return;
        }
        //进商品详情的
        //        WTOGoodsCategoryModel *categoryModel = self.categoryData[indexPath.section - 1];
        //        WTOGoodsDetailEntity *model = categoryModel.goodList[indexPath.row];
        //        WTOGoodsDetailViewController *vc = [[WTOGoodsDetailViewController alloc] init];
        //        vc.goodsEntity = model;
        //        vc.storeid = self.storeID;
        //        vc.gid = [NSString stringWithFormat:@"%@", model.goodsid];
        //        UINavigationController *nav = self.shopGoodsVC.navigationController;
        //
        //        [nav pushViewController:vc animated:YES];
        //
    }
}

// 当拖动右边TableView的时候，处理左边TableView
- (void)selectRowAtIndexPath:(NSInteger)index
{
    
    if (!self.modelManager.categoryArr.count||index < 0) return;
    if ([_leftTableView numberOfRowsInSection:0] <= index) return;
    
    [_leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    self.clickLeftS = NO;
}

#pragma mark -加入购物车动画
-(void) JoinCartAnimationWithRect:(CGRect)rect
{
    
    CGFloat startX = rect.origin.x;
    CGFloat startY = rect.origin.y;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(startX, startY)];
    //三点曲线
    [path addCurveToPoint:CGPointMake(_endPointX, _endPointY)
            controlPoint1:CGPointMake(startX, startY)
            controlPoint2:CGPointMake(startX - 180, startY - 200)];
    
    _dotLayer = [CALayer layer];
    _dotLayer.backgroundColor = [UIColor redColor].CGColor;
    _dotLayer.frame = CGRectMake(0, 0, 15, 15);
    _dotLayer.cornerRadius = (15 + 15) /4;
    [self.view.layer addSublayer:_dotLayer];
    [self groupAnimationWith:path layer:_dotLayer];
    
}

#pragma mark - 组合动画
-(void)groupAnimationWith:(UIBezierPath *)path layer:(CALayer *)layer
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.path = path.CGPath;
    animation.rotationMode = kCAAnimationRotateAuto;
    
    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"alpha"];
    alphaAnimation.duration = 0.5f;
    alphaAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    alphaAnimation.toValue = [NSNumber numberWithFloat:0.1];
    alphaAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    CAAnimationGroup *groups = [CAAnimationGroup animation];
    groups.animations = @[animation,alphaAnimation];
    groups.duration = 0.8f;
    groups.removedOnCompletion = NO;
    groups.fillMode = kCAFillModeForwards;
    groups.delegate = self;
    [groups setValue:@"groupsAnimation" forKey:@"animationName"];
    [layer addAnimation:groups forKey:nil];
    [self performSelector:@selector(removeFromLayer:) withObject:layer afterDelay:0.8f];
}

- (void)removeFromLayer:(CALayer *)layerAnimation
{
    [layerAnimation removeFromSuperlayer];
}

//刷新列表
- (void)reloadTable
{
    [_leftTableView reloadData];
    [_rightTableView reloadData];
}

- (MQLHomeModelManager *)modelManager
{
    if (!_modelManager) {
        _modelManager = [[MQLHomeModelManager alloc]init];
    }
    return _modelManager;
}

@end
