//
//  WTOSureOrderViewController.h
//  WolianwPro
//
//  Created by 李宗帅 on 17/3/2.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "LTXBaseViewController.h"
#import "HDOrderObject.h"
@interface WTOSureOrderViewController : LTXBaseViewController


@property(nonatomic,strong)NSArray *goodsArray;//商品数组

@property (nonatomic, strong) HDOrderObject *orderObj;

//从订单列表进来的  是0   普通的是1
- (instancetype)initWithState:(NSInteger)state;


@end
