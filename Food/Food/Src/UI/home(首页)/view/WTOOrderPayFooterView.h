//
//  WTOOrderPayFooterView.h
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/11.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTOOrderPayFooterView : UITableViewHeaderFooterView

+(NSString *)cellIndentify;

-(void)refreshWithTotalAmount:(NSNumber *)totalAmount;

@end
