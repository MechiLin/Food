//
//  WTOSureOrderBottomView.h
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/3.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WTOSureOrderBottomViewDelegate <NSObject>

-(void)takeOrder;

@end

@interface WTOSureOrderBottomView : UIView

@property(assign,nonatomic)id<WTOSureOrderBottomViewDelegate> delegate;

//刷新bottomView
-(void)updateWithEntityWithTotalAmount:(NSNumber *)totalAmount withdisCountAmount:(NSNumber *)disContAmount;

@end
