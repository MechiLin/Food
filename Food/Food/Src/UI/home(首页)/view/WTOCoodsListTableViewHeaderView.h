//
//  WTOCoodsListTableViewHeaderView.h
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/5.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTOCoodsListTableViewHeaderView : UIView

@property (nonatomic, strong) UILabel *name;

@end
