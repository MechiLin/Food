//
//  WLWShopContactCallView.m
//  OuLianWang
//
//  Created by 皇室美家 on 16/8/4.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WLWShopContactCallView.h"

@implementation WLWShopContactCallView
{
    NSArray       *_phoneAry;           //电话数组
    UITableView   *_tableView;          //列表
    NSString      *_selectedPhoneNum;   //选择的号码
}

- (id)initWithFrame:(CGRect)frame withPhones:(NSArray *)phoneAry
{
    self = [super initWithFrame:frame];
    if(self)
    {
        _phoneAry = phoneAry;
        [self createView];
    }
    return self;
}

#pragma mark - 创建视图

-(void) createView
{
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    
    UIView *phoneView = [[UIView alloc] initWithFrame:CGRectMake((self.width - 300)/2, (self.height - 250)/2, 300, 200)];
    phoneView.backgroundColor = [UIColor whiteColor];
    [phoneView.layer setMasksToBounds:YES];
    [phoneView.layer setCornerRadius:5];
    [self addSubview:phoneView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 100, 30)];
    label.text = @"拨打电话";
    label.textColor = [UIColor redColor];
    [phoneView addSubview:label];
    
    UIImageView *lineView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(label.frame), phoneView.width, 1)];
    lineView1.backgroundColor = [UIColor redColor];
    [phoneView addSubview:lineView1];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lineView1.frame), phoneView.width, 120)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [phoneView addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc] init];
    
    UIImageView *lineView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_tableView.frame), phoneView.width, 1)];
    lineView2.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    [phoneView addSubview:lineView2];
    
    UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lineView2.frame), phoneView.width, 40)];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [phoneView addSubview:cancelBtn];

}

#pragma mark - 按钮点击

- (void)cancelBtnClick
{
    [self removeFromSuperview];
}

#pragma mark - TableView Delegate & Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _phoneAry.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [UITableViewCell new];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 200, 20)];
    label.text = _phoneAry[indexPath.row];
    label.textColor = [UIColor grayColor];
    label.tag = indexPath.row;
    [cell addSubview:label];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.hidden = YES;
    UIAlertView *altView=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"是否呼叫%@", _phoneAry[indexPath.row]] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [altView  show];
    _selectedPhoneNum = _phoneAry[indexPath.row];
}

#pragma mark - UIAlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //  确定打电话
    if (buttonIndex==1)
    {
        //去掉电话中的 -
        NSString * str = [_selectedPhoneNum stringByReplacingOccurrencesOfString:@"-" withString:@""];
        openCall(str);
        
    }
    
    [self removeFromSuperview];
}

@end
