//
//  WTOLeaveMessegeCell.m
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/3.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTOLeaveMessegeCell.h"
#import <Masonry/Masonry.h>

@interface WTOLeaveMessegeCell ()<UITextViewDelegate>

@property(nonatomic,weak)UILabel *invoceLabel;//发票抬头
@property(nonatomic,weak)UILabel *businessInvoceLabel;//商家是否支持发票
@property(nonatomic,weak)UIImageView *line;//线条
@property(nonatomic,weak)UILabel *countLabel;//字数限制label

@end

@implementation WTOLeaveMessegeCell

+(NSString *)cellIndentify
{
    return NSStringFromClass([self class]);
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self initSubViews];
    }
    
    return self;
}

-(void)initSubViews
{
    
    if (_invoceLabel == nil) {
        
        UILabel *invoceLabel = [[UILabel alloc] init];
        invoceLabel.text = @"发票抬头";
        invoceLabel.textColor = ZQColor(51, 51, 51);
        invoceLabel.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:invoceLabel];
        self.invoceLabel = invoceLabel;
        
        [self.invoceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(@14);
            
        }];
    }
    
    if (self.businessInvoceLabel == nil) {
        
        UILabel *businessInvoceLabel = [[UILabel alloc] init];
        businessInvoceLabel.text = @"商家不支持开发票";
        businessInvoceLabel.font = [UIFont systemFontOfSize:14.0f];
        businessInvoceLabel.textColor = ZQColor(153, 153, 153);
        [self.contentView addSubview:businessInvoceLabel];
        self.businessInvoceLabel = businessInvoceLabel;
        
        [self.businessInvoceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.trailing.mas_equalTo(@-15);
            make.centerY.mas_equalTo(self.invoceLabel);
            make.width.mas_lessThanOrEqualTo(@120);
        }];
    }
    
    if (self.line == nil) {
        
        UIImageView *line = [[UIImageView alloc] init];
        line.backgroundColor = LTXDefaultsLineColor;
        [self.contentView addSubview:line];
        self.line = line;
        
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(self.invoceLabel.mas_bottom).mas_offset(@14);
            make.size.mas_equalTo(CGSizeMake(ScreenW - 10, 0.5));
        }];
    }
    
    if (self.invoceTextView == nil) {
        
        MyTextView *invoceTextView = [[MyTextView alloc] init];
        invoceTextView.placehoder = @"给卖家留言...";
        invoceTextView.placehoderColor = ZQColor(153, 153, 153);
        invoceTextView.delegate = self;
        [self.contentView addSubview:invoceTextView];
        self.invoceTextView = invoceTextView;
        
        [self.invoceTextView mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(self.line.mas_bottom).mas_offset(@10);
            make.trailing.mas_equalTo(@-10);
            make.height.mas_equalTo(@56);
        }];
    }
    
    if (self.countLabel == nil) {
        
        UILabel *countLabel = [[UILabel alloc] init];
        countLabel.textColor = ZQColor(153, 153, 153);
        countLabel.font = [UIFont systemFontOfSize:12.0f];
        countLabel.text = @"0-250";
        [self.contentView addSubview:countLabel];
        self.countLabel = countLabel;
        
        [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.trailing.mas_equalTo(@-15);
            make.top.mas_equalTo(self.invoceTextView.mas_bottom).mas_offset(@15);
        }];
    }
}

-(NSString *)getLeaveText
{
    return self.invoceTextView.text;
}

#pragma mark - UITextView Delegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (range.location >= 250) {
        
        NSString *repleceText = [textView.text stringByReplacingCharactersInRange:range withString:@""];
        textView.text = repleceText;
        
        return NO;
    }
    
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length > 250) {
        NSString *text = [textView.text substringToIndex:250];
        textView.text = text;
    }
    
    _countLabel.text = [NSString stringWithFormat:@"%ld/250",(long)textView.text.length];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
