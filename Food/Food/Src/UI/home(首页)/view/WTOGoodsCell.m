//
//  WTOGoodsCell.m
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/2.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTOGoodsCell.h"
#import <Masonry/Masonry.h>
#import "SDImageCache.h"

@interface WTOGoodsCell ()

@property(nonatomic,weak)UIImageView *promotionLogo;//商品图标
@property(nonatomic,weak)UILabel *goodsName;//商品名称
@property(nonatomic,weak)UILabel *buyCount;//购买数量
@property(nonatomic,weak)UILabel *priceLabel;//价格
@property(nonatomic,weak)UILabel *originalPriceLabel;//原价
@property(nonatomic,weak)UILabel *specLabel;//规格

@end

@implementation WTOGoodsCell

+(NSString *)cellIndentify
{
    return NSStringFromClass([self class]);
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self initSubViews];
    }
    
    return self;
}

#pragma mark - initSubViews
-(void)initSubViews
{
    if (self.goodsName == nil) {
        
        UILabel *goodsName = [[UILabel alloc] init];
        goodsName.font = [UIFont systemFontOfSize:14.0f];
        goodsName.textColor = ZQColor(51, 51, 51);
        [self.contentView addSubview:goodsName];
        self.goodsName = goodsName;
        
        [self.goodsName mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(@8);
            make.width.mas_lessThanOrEqualTo(@(150 * k750Scale));
        }];
    }
    
    if (self.buyCount == nil) {
        
        UILabel *bugCount = [[UILabel alloc] init];
        bugCount.font = [UIFont systemFontOfSize:12.0f];
        bugCount.textColor = ZQColor(153, 153, 153);
        [self.contentView addSubview:bugCount];
        self.buyCount = bugCount;
        
        [self.buyCount mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@(160 * k750Scale));
            make.centerY.mas_equalTo(self.goodsName);
        }];
    }
    
    if (self.priceLabel == nil) {
        
        UILabel *priceLabel = [[UILabel alloc] init];
        priceLabel.font = [UIFont systemFontOfSize:14.0f];
        priceLabel.textColor = ZQColor(51, 51, 51);
        [self.contentView addSubview:priceLabel];
        self.priceLabel = priceLabel;
        
        [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.trailing.mas_equalTo(@-15);
            make.centerY.mas_equalTo(self.goodsName);
        }];
    }
}

//更新UI成为促销样式
-(void)updatePromotionStyle
{
    [self.promotionLogo mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.leading.mas_equalTo(@10);
        make.top.mas_equalTo(@10);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.goodsName mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.leading.mas_equalTo(self.promotionLogo.mas_trailing).mas_offset(@5);
        make.top.mas_equalTo(@8);
        make.width.mas_equalTo(@(150 * k750Scale));
    }];
    
    [self.originalPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.trailing.mas_equalTo(self.priceLabel.mas_leading).mas_offset(@-5);
        make.top.mas_equalTo(self.goodsName);
    }];
}

//更新成有规格样式
-(void)updateSpecStyle
{
    [self.specLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.leading.mas_equalTo(@10);
        make.top.mas_equalTo(self.goodsName.mas_bottom).mas_offset(@5);
    }];
}

//还原成无规格样式
-(void)returnNoSpecStyle
{
    [self.specLabel removeFromSuperview];
}

//还原成默认UI样式
-(void)returnDefautStyle
{
    if (_promotionLogo) {
        [self.promotionLogo removeFromSuperview];
    }
    
    if (_originalPriceLabel) {
        [self.originalPriceLabel removeFromSuperview];
    }
    
    [self.goodsName mas_updateConstraints:^(MASConstraintMaker *make) {
       
        make.leading.mas_equalTo(@10);
    }];
}

-(void)updateWithEntityWithGoodsEntity:(MQLFoodModel *)entity
{
    
    self.goodsName.text = entity.name;
    self.buyCount.text = [NSString stringWithFormat:@"x%zd",entity.buyCount];
    self.priceLabel.text = [NSString stringWithFormat:@"¥ %.2f",entity.price];
    
//    //判断是否有规格
//    if (entity.speciName) {
//
//        self.specLabel.text = entity.speciName;
//        [self updateSpecStyle];
//
//    }else{
//
//
//    }
    [self returnNoSpecStyle];
    
//    //有优惠,参加了限时抢购
//    if (([entity.orginal integerValue] == 0 && entity.orginal) || entity.discount) {
//
//        [self updatePromotionStyle];
//
//        NSAttributedString *attribute = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥ %@",entity.totalprice] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f],NSForegroundColorAttributeName:ZQColor(153, 153, 153),NSStrikethroughStyleAttributeName:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle),NSStrikethroughColorAttributeName:ZQColor(153, 153, 153)}];
//        [self.originalPriceLabel setAttributedText:attribute];
//
//    }else{
//
//
//    }
    [self returnDefautStyle];
    
}

#pragma mark - getter and setter
-(UIImageView *)promotionLogo
{
    if (_promotionLogo == nil) {
        
        UIImageView *promotionLogo = [[UIImageView alloc] init];
        promotionLogo.image = [UIImage imageNamed:@"icon_pre"];
        [self.contentView addSubview:promotionLogo];
        _promotionLogo = promotionLogo;
    }
    
    return _promotionLogo;
}

-(UILabel *)originalPriceLabel
{
    if (_originalPriceLabel == nil) {
        
        UILabel *oritinalPriceLabel = [[UILabel alloc] init];
        oritinalPriceLabel.font = [UIFont systemFontOfSize:13.0f];
        oritinalPriceLabel.textColor = ZQColor(153, 153, 153);
        [self.contentView addSubview:oritinalPriceLabel];
        _originalPriceLabel = oritinalPriceLabel;
    }
    
    return _originalPriceLabel;
}

-(UILabel *)specLabel
{
    if (_specLabel == nil) {
        
        UILabel *specLabel = [[UILabel alloc] init];
        specLabel.font = [UIFont systemFontOfSize:12.0f];
        specLabel.textColor = ZQColor(153, 153, 153);
        [self.contentView addSubview:specLabel];
        _specLabel = specLabel;
    }
    
    return _specLabel;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
