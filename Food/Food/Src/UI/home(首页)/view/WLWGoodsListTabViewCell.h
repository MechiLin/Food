//
//  WLWGoodsListTabViewCell.h
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/1.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MQLFoodModel;
#define kCellIdentifier_Right @"WLWGoodsListTabViewCell"

typedef enum : NSUInteger {
    DISCOUNT_TYPE_OFFSET,//代金券
    DISCOUNT_TYPE_DISCO,//折扣
} DISCOUNT_TYPE;

@interface WLWGoodsListTabViewCell : UITableViewCell

@property (assign, nonatomic) NSInteger foodId; //商品的标记  接口好了之后 可以传来模型  把上边的都丢到.m去

@property (assign, nonatomic) NSUInteger amount; //一共点了几份的记录方便存储到数据库

@property (nonatomic, strong) MQLFoodModel *model;

@property (nonatomic, strong) UIButton *minus;//减

@property (nonatomic, strong) UIButton *plus;//加

@property (nonatomic, assign) BOOL isBusiness; //是否是营业状态;

//减少订单数量 不需要动画效果  点击按钮的操作
@property (copy, nonatomic) void (^plusBlock)(NSInteger count,BOOL animated);

- (void)clickJoinShopCarWithPlus:(BOOL)plus;

@end

@interface WTOGoodsListDiscountView : UIView

@property (nonatomic, copy) NSString *discountStr;//促销描述

@property (nonatomic, assign) DISCOUNT_TYPE type;

- (instancetype)initWithType:(DISCOUNT_TYPE)type;

@end
