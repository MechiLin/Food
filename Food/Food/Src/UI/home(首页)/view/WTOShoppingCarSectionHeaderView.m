//
//  WTOShoppingCarSectionHeaderView.m
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/6.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WTOShoppingCarSectionHeaderView.h"

@interface WTOShoppingCarSectionHeaderView ()
@property (nonatomic, strong) UILabel *headerTitle;
@end
@implementation WTOShoppingCarSectionHeaderView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    
    self.backgroundColor = ZQColor(244, 244, 244);
    self.headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
    self.headerTitle.text = [NSString stringWithFormat:@"共选了%zd件",self.foodCount];
    self.headerTitle.font = [UIFont systemFontOfSize:12];
    self.headerTitle.textColor = ZQColor(51, 51, 51);
    [self addSubview:self.headerTitle];
    
    UILabel *leftLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 3, 30)];
    [self addSubview:leftLine];
    leftLine.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    UIButton *clear = [UIButton buttonWithType:UIButtonTypeCustom];
    clear.frame= CGRectMake(ScreenW - 100, 0, 100, 30);
    [clear setTitle:@" 清空购物车" forState:UIControlStateNormal];
    [clear setTitleColor:ZQColor(51, 51, 51) forState:UIControlStateNormal];
    [clear setImage:[UIImage imageNamed:@"bc_icon_del"] forState:UIControlStateNormal];
//    clear.titleLabel.textAlignment = NSTextAlignmentCenter;
    clear.titleLabel.font = [UIFont systemFontOfSize:12];
    [clear addTarget:self action:@selector(clearShoppingCart:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:clear];
    
}

- (void)clearShoppingCart:(UIButton *)btn
{
    if ([self.delegate respondsToSelector:@selector(clickClearShoppingBtnWithShoppingCarSectionHeaderView:clearBtn:)]) {
        [self.delegate clickClearShoppingBtnWithShoppingCarSectionHeaderView:self clearBtn:btn];
    }
}

- (void)setFoodCount:(NSInteger)foodCount
{
    _foodCount = foodCount;
    
    self.headerTitle.text = [NSString stringWithFormat:@"共点了%zd件",self.foodCount];
}
@end
