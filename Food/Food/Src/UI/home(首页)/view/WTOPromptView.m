//
//  WTOPromptView.m
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/3.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTOPromptView.h"
#import <Masonry/Masonry.h>

@interface WTOPromptView ()

@property(nonatomic,weak)UILabel *promptLabel;//提示语句

@end

@implementation WTOPromptView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = ZQColor(254, 249, 238);
        
        [self initSubViews];
    }
    return self;
}

-(void)initSubViews
{
    if (self.promptLabel == nil) {
        
        UILabel *promptLabel = [[UILabel alloc] init];
        promptLabel.textAlignment = NSTextAlignmentCenter;
        promptLabel.font = [UIFont systemFontOfSize:14.0f];
        promptLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:promptLabel];
        self.promptLabel = promptLabel;
        
        [self.promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.centerY.mas_equalTo(self);
            make.leading.mas_equalTo(@10);
            make.trailing.mas_equalTo(@-10);
        }];
    }
}

-(void)showWithProMessege:(NSString *)proMessege
{
    self.promptLabel.text = proMessege;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
