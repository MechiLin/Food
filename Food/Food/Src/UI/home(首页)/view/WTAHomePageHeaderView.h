//
//  WTAHomePageHeaderView.h
//  WolianwPro
//
//  Created by 邓雨来 on 2017/2/24.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HDSellerObject.h"
@class WTAHomePageHeaderView;
typedef NS_ENUM(NSUInteger, WTAStoreCollectionState) {
    WTAStoreCollectionStateUncollected,  // 未收藏
    WTAStoreCollectionStateCollected     // 已收藏
};
@protocol WTAHomePageHeaderViewDelegate <NSObject>

- (void)headerView:(WTAHomePageHeaderView *)headerView didClickCollectButtonWithCurrentCollectState:(WTAStoreCollectionState)collectState collectButton:(UIButton *)collectButton;

- (void)headerView:(WTAHomePageHeaderView *)headerView didClickLocationBtn:(UIButton *)btn;//点击定位按钮  跳转

@end
@interface WTAHomePageHeaderView : UIView

@property (nonatomic, weak) id<WTAHomePageHeaderViewDelegate>delegate;

@property (nonatomic, copy) void(^clickDiscount)(void);

- (void)updateHeaderViewWithStoreInfoEntity:(HDSellerObject *)storeInfoEntity;
- (void)updateDistance:(CGFloat)distance;

@end
