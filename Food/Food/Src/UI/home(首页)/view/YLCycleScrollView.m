//
//  YLCycleScrollView.m
//  YLCycleScrollView
//
//  Created by 邓雨来 on 2017/3/23.
//  Copyright © 2017年 邓雨来. All rights reserved.
//

#import "YLCycleScrollView.h"
#import <Masonry/Masonry.h>

#define YLColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

#pragma mark - ======= 数据实体 类 =======
@implementation YLCycleScorllViewCellEntity

@end

#pragma mark - ======= cell 类 =======
static CGFloat const HorizontalMargin = 12;
@interface YLCycleScorllViewCell : UICollectionViewCell
/** 图标 */
@property (nonatomic, weak) UIImageView *iconView;

/** 内容lable */
@property (nonatomic, weak) UILabel *contentLabel;

- (void)updateCellWithCellEntity:(WTAHomeActivityEntity *)entity;

@end
@implementation YLCycleScorllViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        UIImageView *iconView = [[UIImageView alloc] init];
        iconView.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:iconView];
        self.iconView = iconView;
        [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(60);
            make.top.bottom.mas_equalTo(self.contentView);
            make.width.mas_equalTo(0);
            make.left.mas_equalTo(self.contentView).offset(HorizontalMargin);
        }];
        
        UILabel *contentLabel = [[UILabel alloc] init];
        contentLabel.font = [UIFont systemFontOfSize:12];
        contentLabel.textColor = [UIColor whiteColor];//YLColor(51, 51, 51);
        [self.contentView addSubview:contentLabel];
        self.contentLabel = contentLabel;
        
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.mas_equalTo(self.contentView);
            make.left.mas_equalTo(iconView.mas_right).offset(HorizontalMargin);
        }];
    }
    return self;
}


//- (void)updateCellWithCellEntity:(WTAHomeActivityEntity *)entity {
//    UIImage *image =  [UIImage imageNamed:entity.promotionImg];
//    CGFloat imageW = image.size.width;
//    self.iconView.image = image;
//    [self.iconView mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(imageW);
//    }];
//    self.contentLabel.text = entity.showPromtionMsg;
//}

@end



#pragma mark - ======= YLCycleScrollView 类 =======

static NSUInteger  const YLMaxSections = 3;

@interface YLCycleScrollView ()<UICollectionViewDataSource, UICollectionViewDelegate>
/** 数据源 */
@property (nonatomic, strong) NSArray<WTAHomeActivityEntity *> *dataArr;
/** 点击回调block */
@property (nonatomic, copy) void(^clickBlcok)(NSInteger, WTAHomeActivityEntity *);
@property (nonatomic, strong) UICollectionViewFlowLayout *layout;
@property (nonatomic, weak) UICollectionView *collectionView;
@property (nonatomic, strong) NSTimer *timer;

@end
@implementation YLCycleScrollView
#pragma mark - 初始化
+ (instancetype)cycleScrollViewWithDataArr:(NSArray<WTAHomeActivityEntity *> *)dataArr clickBlock:(void(^)(NSInteger index, WTAHomeActivityEntity *promotionEntity))clickBlcok{
    return [[self alloc] initWithDataArr:dataArr clickBlock:clickBlcok];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupSubviews];
    
    [self initData];
}

- (void)initData {
    self.timeInterval = 3.0;
    
    [self addTimer];
}

- (instancetype)initWithDataArr:(NSArray<WTAHomeActivityEntity *> *)dataArr clickBlock:(void(^)(NSInteger index, WTAHomeActivityEntity *promotionEntity))clickBlcok {
    if (self = [super init]) {
        self.dataArr = dataArr;
        self.clickBlcok = clickBlcok;
        
        [self.collectionView reloadData];
        // 默认显示最中间的那组
        [self setdefaultSection];
    }
    return self;
}

/// 默认选中的组
- (void)setdefaultSection {
    if (self.dataArr.count == 0) return;
    
    // 默认显示最中间的那组
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:YLMaxSections / 2] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self setupSubviews];
        
        [self initData];
    }
    return self;
}

// 添加子控件
- (void)setupSubviews {
    
    //layout
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    self.layout = layout;
    
    // collectionView
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.scrollsToTop = NO;
    collectionView.scrollEnabled = NO;
    collectionView.pagingEnabled = YES;
    collectionView.showsVerticalScrollIndicator = NO;
    collectionView.showsHorizontalScrollIndicator = NO;
    [collectionView registerClass:YLCycleScorllViewCell.class forCellWithReuseIdentifier:cycleScrollViewCellID];
    [self addSubview:collectionView];
    self.collectionView = collectionView;
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.collectionView.frame;
    self.layout.itemSize = frame.size;
}

#pragma mark - - - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return YLMaxSections;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YLCycleScorllViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cycleScrollViewCellID forIndexPath:indexPath];
    [cell updateCellWithCellEntity:self.dataArr[indexPath.item]];
    return cell;
}
#pragma mark - - - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.clickBlcok) {
        self.clickBlcok(indexPath.item, self.dataArr[indexPath.item]);
    }
}

#pragma mark - - - 创建timer
- (void)addTimer {
    [self removeTimer];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timeInterval target:self selector:@selector(scrollContentView) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}
#pragma mark - - - 移除timer
- (void)removeTimer {
    [_timer invalidate];
    _timer = nil;
}
#pragma mark - - - timer方法
- (void)scrollContentView {
    if (self.dataArr.count == 0) return;
    
    NSIndexPath *currentIndexPath = [[self.collectionView indexPathsForVisibleItems] lastObject];
    NSIndexPath *resetCurrentIndexPath = [NSIndexPath indexPathForItem:currentIndexPath.item inSection:YLMaxSections / 2];
    [self.collectionView scrollToItemAtIndexPath:resetCurrentIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
    
    NSInteger nextItem = resetCurrentIndexPath.item + 1;
    NSInteger nextSection = resetCurrentIndexPath.section;
    if (nextItem == self.dataArr.count) {
        nextItem = 0;
        nextSection++;
    }
    
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:nextItem inSection:nextSection];
    
    [self.collectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
}

#pragma mark - - - setter

- (void)setTimeInterval:(CGFloat)timeInterval {
    _timeInterval = timeInterval;
    [self addTimer];
}


@end
