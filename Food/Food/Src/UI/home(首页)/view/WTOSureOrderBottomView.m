//
//  WTOSureOrderBottomView.m
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/3.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTOSureOrderBottomView.h"
#import <Masonry/Masonry.h>

@interface WTOSureOrderBottomView ()

@property(nonatomic,weak)UILabel *discountMoney;//已优惠金额
@property(nonatomic,weak)UILabel *totalMoney;//总金额
@property(nonatomic,weak)UIButton *takeOrderButton;//提交订单按钮
@property(nonatomic,weak)UIImageView *topLine;//底部线条

@end

@implementation WTOSureOrderBottomView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self initSubViews];
        
    }
    return self;
}

-(void)initSubViews
{
    if (self.discountMoney == nil) {
        
        UILabel *discountMoney = [[UILabel alloc] init];
        discountMoney.font = [UIFont systemFontOfSize:14.0f];
        discountMoney.textColor = [UIColor darkGrayColor];
        [self addSubview:discountMoney];
        self.discountMoney = discountMoney;
        
        [self.discountMoney mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@14);
            make.centerY.mas_equalTo(self);
            make.width.mas_lessThanOrEqualTo(@120);
        }];
    }
    
    if (self.takeOrderButton == nil) {
        
        UIButton *takeOrderButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [takeOrderButton setTitle:HDAccount.account.isAdmin?@"确认订单":@"提交订单" forState:UIControlStateNormal];
        [takeOrderButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [takeOrderButton setBackgroundColor:LTXDefaultsColor];
        [takeOrderButton addTarget:self action:@selector(takeOrder:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:takeOrderButton];
        self.takeOrderButton = takeOrderButton;
        
        [self.takeOrderButton mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.top.bottom.trailing.mas_equalTo(self);
            make.width.mas_equalTo(@100);
        }];
    }
    
    if (self.totalMoney == nil) {
        
        UILabel *totalMoney = [[UILabel alloc] init];
        totalMoney.textAlignment = NSTextAlignmentRight;
        [self addSubview:totalMoney];
        self.totalMoney = totalMoney;
        
        [self.totalMoney mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.trailing.mas_equalTo(self.takeOrderButton.mas_leading).mas_offset(@-15);
            make.centerY.mas_equalTo(self);
        }];
    }
    
    if (self.topLine == nil) {
        
        UIImageView *topLine = [[UIImageView alloc] init];
        topLine.backgroundColor = LTXDefaultsLineColor;
        [self addSubview:topLine];
        self.topLine = topLine;
        
        [self.topLine mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.top.leading.trailing.mas_equalTo(self);
            make.height.mas_equalTo(@0.5);
        }];
    }
}

-(void)updateWithEntityWithTotalAmount:(NSNumber *)totalAmount withdisCountAmount:(NSNumber *)disContAmount
{
    
    
    NSString *priceString = [NSString stringWithFormat:@"合计: ¥%.2f",[totalAmount floatValue]];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    [attributeString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],NSForegroundColorAttributeName:[UIColor blackColor]} range:NSMakeRange(0, 3)];
    [attributeString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],NSForegroundColorAttributeName:[UIColor blackColor]} range:NSMakeRange(3, priceString.length - 3)];
    [self.totalMoney setAttributedText:attributeString];
}

//提交订单
-(void)takeOrder:(UIButton *)orderButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(takeOrder)]) {
        [self.delegate takeOrder];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
