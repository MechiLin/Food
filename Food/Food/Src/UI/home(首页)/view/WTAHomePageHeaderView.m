//
//  WTAHomePageHeaderView.m
//  WolianwPro
//
//  Created by 邓雨来 on 2017/2/24.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTAHomePageHeaderView.h"
#import <Masonry/Masonry.h>
#import "ODLbScrollView.h"
#import "YLCycleScrollView.h"
#import <UIImageView+WebCache.h>

@interface WTAHeaderButton : UIButton

@end
@implementation WTAHeaderButton

- (void)setHighlighted:(BOOL)highlighted{}

@end


static const CGFloat storeIconIViewWH = 57; // logo的宽高
static const CGFloat headerGap = 12;  // 子控件间距
@interface WTAHomePageHeaderView ()
@property (nonatomic, weak) UIImageView *storeBgIView;
@property (nonatomic, weak) UIImageView *storeIconIView;
@property (nonatomic, weak) UIButton *collectButton;
@property (nonatomic, weak) UILabel *fansCountLabel;
@property (nonatomic, weak) UILabel *storeNameLabel;
@property (nonatomic, weak) UILabel *addressLabel;
@property (nonatomic, weak) UIImageView *darkImage;
@property (nonatomic, weak) UIImageView *lightImage;
@property (nonatomic, weak) UILabel *distanceLabel;
@property (nonatomic, weak) UIView *localNewsView;
@property (nonatomic, weak) UILabel *sendLabel;
@property (nonatomic, weak) UIImageView *promotionIconView;
@property (nonatomic, weak) YLCycleScrollView *odlScrollView;//公告容器
@property (nonatomic, weak) UIView *promotionsView;
@property (nonatomic, weak) UIImageView *locationIView;
@end
@implementation WTAHomePageHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // 添加子控件
        [self setupSubViews];
        
    }
    return self;
}

- (void)setupSubViews {
    // 顶部的背景图片
    
    UIImageView *storeBgIView = [[UIImageView alloc] init];
    storeBgIView.userInteractionEnabled = YES;
    [self addSubview:storeBgIView];
    [storeBgIView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.mas_equalTo(self);
    }];
    
    // 添加背景图片的内部子控件
    // logo
    UIImageView *storeIconIView = [[UIImageView alloc] init];
    storeIconIView.contentMode = UIViewContentModeScaleAspectFill;
    storeIconIView.clipsToBounds = YES;
    [storeBgIView addSubview:storeIconIView];
    self.storeIconIView = storeIconIView;
    [storeIconIView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(storeBgIView).offset(headerGap);
        make.width.height.mas_equalTo(storeIconIViewWH);
    }];
    
    // 已收藏图标
    WTAHeaderButton *collectButton = [[WTAHeaderButton alloc] init];
    [collectButton setImage:[UIImage imageNamed:@"home_cel"] forState:UIControlStateNormal];
    [collectButton setImage:[UIImage imageNamed:@"home_cel_fill"] forState:UIControlStateSelected];
    [storeBgIView addSubview:collectButton];
    self.collectButton = collectButton;
    [collectButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(storeBgIView);
        make.top.mas_equalTo(storeIconIView).offset(10);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.width.mas_equalTo(100);
    }];
    
    // 粉丝数
    UILabel *fansCountLabel = [[UILabel alloc] init];
    fansCountLabel.numberOfLines = 0;
    fansCountLabel.font = [UIFont systemFontOfSize:12];
    fansCountLabel.textAlignment = NSTextAlignmentCenter;
    fansCountLabel.textColor = [UIColor whiteColor];
    [storeBgIView addSubview:fansCountLabel];
    self.fansCountLabel = fansCountLabel;
    [fansCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(collectButton.mas_bottom).with.offset(2);
        make.centerX.height.mas_equalTo(collectButton);
        make.width.mas_equalTo(100);
    }];
    
    // 分割线
    UIView *seporatorLine = [[UIView alloc] init];
    seporatorLine.backgroundColor = ZQColorRGBA(255, 255, 255, 0.3);
    [storeBgIView addSubview:seporatorLine];
    [seporatorLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(collectButton.mas_left);
        make.centerY.mas_equalTo(storeIconIView);
        make.height.mas_equalTo(storeIconIView).multipliedBy(0.8);
        make.width.mas_equalTo(1);
    }];
    
    // 店铺名
    UILabel *storeNameLabel = [[UILabel alloc] init];
    storeNameLabel.textColor = [UIColor whiteColor];
    storeNameLabel.font = [UIFont systemFontOfSize:16];
    [storeBgIView addSubview:storeNameLabel];
    self.storeNameLabel = storeNameLabel;
    [storeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(storeIconIView.mas_right).offset(headerGap);
        make.centerY.mas_equalTo(collectButton);
        make.height.mas_equalTo(storeIconIView).multipliedBy(0.6);
        make.right.mas_lessThanOrEqualTo(fansCountLabel.mas_left).offset(-headerGap);
    }];
    
    // 配送信息
    UILabel *sendLabel = [[UILabel alloc] init];
    sendLabel.font = [UIFont systemFontOfSize:11];
    sendLabel.textColor = [UIColor whiteColor];
    [storeBgIView addSubview:sendLabel];
    self.sendLabel = sendLabel;
    [sendLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(storeNameLabel);
        make.top.mas_equalTo(storeNameLabel.mas_bottom).offset(5);
        make.right.mas_lessThanOrEqualTo(seporatorLine.mas_left);
    }];
    
    UIView *promotionsView = [[UIView alloc] init];
    promotionsView.backgroundColor = ZQColorRGBA(0, 0, 0, 0.2);
    [storeBgIView addSubview:promotionsView];
    self.promotionsView = promotionsView;
    [promotionsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(storeBgIView);
        make.top.mas_equalTo(storeIconIView.mas_bottom).offset(headerGap);
        make.height.mas_equalTo(30);
        make.bottom.mas_equalTo(storeBgIView);
    }];
    
    UIImageView *locationIView = [[UIImageView alloc] init];
    locationIView.image = [UIImage imageNamed:@"home_location"];
    locationIView.contentMode = UIViewContentModeCenter;
    [promotionsView addSubview:locationIView];
    locationIView.userInteractionEnabled = YES;
    self.locationIView = locationIView;
    [locationIView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.mas_equalTo(promotionsView);
//        make.left.mas_equalTo(promotionsView.mas_right).offset(-100);
        make.width.mas_equalTo(30);
    }];
    
    // 距离Label
    UILabel *distanceLabel = [[UILabel alloc] init];
    distanceLabel.text = @"...";
    distanceLabel.textColor = [UIColor whiteColor];
    distanceLabel.font = [UIFont systemFontOfSize:12];
    distanceLabel.textAlignment = NSTextAlignmentCenter;
    distanceLabel.userInteractionEnabled = YES;
    [promotionsView addSubview:distanceLabel];
    self.distanceLabel = distanceLabel;
    [distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(promotionsView);
        make.width.equalTo(@50);
        make.right.equalTo(promotionsView.mas_right).with.offset(-12);
    }];
    
    // 地址Label
    UILabel *addressLabel = [[UILabel alloc] init];
    [promotionsView addSubview:addressLabel];
    addressLabel.textColor = [UIColor whiteColor];
    addressLabel.font = [UIFont systemFontOfSize:12];
    self.addressLabel = addressLabel;
    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(locationIView.mas_right);
        make.top.bottom.mas_equalTo(promotionsView);
        make.right.mas_lessThanOrEqualTo(distanceLabel.mas_left);
    }];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickLocationBtn)];
    tapGesture.numberOfTouchesRequired = 1; //手指数
    tapGesture.numberOfTapsRequired = 1; //tap次数
    [distanceLabel addGestureRecognizer:tapGesture];
    
    //
    tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickLocationBtn)];
    tapGesture.numberOfTouchesRequired = 1; //手指数
    tapGesture.numberOfTapsRequired = 1; //tap次数
    [locationIView addGestureRecognizer:tapGesture];
}

- (void)clickLocationBtn;
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerView:didClickLocationBtn:)]) {
        
        [self.delegate headerView:self didClickLocationBtn:nil];
    }
}

- (void)updateHeaderViewWithStoreInfoEntity:(HDSellerObject *)storeInfoEntity {
    
    if (!storeInfoEntity) {
        return; 
    }
    
    [self.storeIconIView sd_setImageWithURL:storeInfoEntity.logoUrl.mj_url placeholderImage:LoadImage(@"shop_default_logo")];
    /** 收藏情况 [如果userid是店主，返回店铺收藏量]--[如果userid不是店主，值等于-2表示未登录；-1未收藏；其他值代表收藏id] */
   // @property (nonatomic, strong) NSNumber *col_condition;
    
    
    
    self.storeNameLabel.text = storeInfoEntity.name;

    self.collectButton.selected = YES;

    self.fansCountLabel.text = storeInfoEntity.telPhone;
    
    self.sendLabel.text = [NSString stringWithFormat:@"营业时间:%@-%@",storeInfoEntity.startBusinessTime,storeInfoEntity.endBusinessTime];
    self.addressLabel.text = [NSString stringWithFormat:@"地址:%@",storeInfoEntity.address];
    CGFloat score = 1;

    [self.lightImage mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.darkImage).multipliedBy(score);
    }];
    
    // 促销活动
    if (self.odlScrollView) {
        [self.odlScrollView removeFromSuperview];
        self.odlScrollView = nil;
    }
    
//    [storeInfoEntity.promotionTypeList enumerateObjectsUsingBlock:^(WTAHomeActivityEntity  *_Nonnull notice, NSUInteger idx, BOOL * _Nonnull stop) {
//        if (notice.promotionType.integerValue == WTAPromotionTypeCounteract) {
//            // 满抵
//            notice.promotionImg = @"icon_di";
//        } else if (notice.promotionType.integerValue == WTAPromotionTypePresented) {
//            // 满送
//            notice.promotionImg = @"icon_zeng";
//
//        } else if (notice.promotionType.integerValue == WTAPromotionTypeFlashSale) {
//            // 限时抢购
//            notice.promotionImg = @"icon_zhe";
//        } else if (notice.promotionType.integerValue == -1){
//            // -1 代金券
//            notice.promotionImg = @"icon_dai";
//        }
//    }];
//    YLCycleScrollView *lbSc = [YLCycleScrollView cycleScrollViewWithDataArr:storeInfoEntity.promotionTypeList clickBlock:^(NSInteger index, WTAHomeActivityEntity *promotionEntity) {
//        TTDPRINT(@"点击了 %zd  promotionId = %@  prmotionName = %@", index, promotionEntity.promotionId, promotionEntity.showPromtionMsg);
//        //点击了活动
//        if (self.clickDiscount) {
//            self.clickDiscount();
//        }
//
//    }];
//    [self.promotionsView addSubview:lbSc];
//    self.odlScrollView = lbSc;
//    [lbSc mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.top.bottom.mas_equalTo(self.promotionsView);
//        make.right.mas_equalTo(self.locationIView.mas_left).offset(-headerGap);
//    }];
}

- (void)updateDistance:(CGFloat)distance {

    self.distanceLabel.text = distance > 1000 ? [NSString stringWithFormat:@"%.2fKm", distance/1000] : [NSString stringWithFormat:@"%.2fm", distance];
}

#pragma mark - 事件处理
- (void)collectButtonClick:(UIButton *)collectBtn {
    TTDPRINT(@"点击收藏按钮");
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerView:didClickCollectButtonWithCurrentCollectState:collectButton:)]) {
        [self.delegate headerView:self didClickCollectButtonWithCurrentCollectState:collectBtn.selected  collectButton:collectBtn];
    }
}

@end

/**
 
 // 评分
 //    if (self.darkImage ==  nil) {
 //        UIImageView *darkImage = [[UIImageView alloc] init];
 //        darkImage.image = [UIImage imageNamed:@"iconfont-xingxing2"];
 //        darkImage.contentMode = UIViewContentModeLeft;
 //        [storeBgIView addSubview:darkImage];
 //        self.darkImage = darkImage;
 //        [darkImage mas_makeConstraints:^(MASConstraintMaker *make) {
 //            make.left.mas_equalTo(storeNameLabel);
 //            make.top.mas_equalTo(storeNameLabel.mas_bottom);
 //            make.size.mas_equalTo(CGSizeMake(157/2, 33/2));
 //        }];
 //    }
 //
 //    if (self.lightImage == nil) {
 //        UIImageView *lightImage = [[UIImageView alloc] init];
 //        lightImage.clipsToBounds = YES;
 //        lightImage.image = [UIImage imageNamed:@"iconfont-xingxing"];
 //        lightImage.contentMode = UIViewContentModeLeft;
 //        [self.darkImage addSubview:lightImage];
 //        self.lightImage = lightImage;
 //        [lightImage mas_makeConstraints:^(MASConstraintMaker *make) {
 //            make.leading.width.top.height.mas_equalTo(self.darkImage);
 //        }];
 //    }
 
 // 店铺地址
 //    UIView *locationView = [[UIView alloc] init];
 //    [self addSubview:locationView];
 //    [locationView mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.top.mas_equalTo(storeBgIView.mas_bottom);
 //        make.left.right.mas_equalTo(self);
 //        make.height.mas_equalTo(40);
 //    }];
 
 //    UIImageView *locationIView = [[UIImageView alloc] init];
 //    locationIView.image = [UIImage imageNamed:@"shop_icon_location"];
 //    locationIView.contentMode = UIViewContentModeCenter;
 //    [locationView addSubview:locationIView];
 //    [locationIView mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.left.top.bottom.mas_equalTo(locationView);
 //        make.width.mas_equalTo(60);
 //    }];
 //
 //    // 距离Label
 //    UILabel *distanceLabel = [[UILabel alloc] init];
 //    distanceLabel.text = @"1.2km";
 //    distanceLabel.textAlignment = NSTextAlignmentCenter;
 //    [locationView addSubview:distanceLabel];
 //    self.distanceLabel = distanceLabel;
 //    [distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.top.bottom.right.mas_equalTo(locationView);
 //        make.width.mas_equalTo(60);
 //    }];
 //
 //    // 竖线
 //    UIView *lineViewV = [[UIView alloc] init];
 //    lineViewV.backgroundColor = [UIColor grayColor];
 //    [locationView addSubview:lineViewV];
 //    [lineViewV mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.right.mas_equalTo(distanceLabel.mas_left);
 //        make.width.mas_equalTo(1);
 //        make.height.mas_equalTo(locationView).multipliedBy(0.5);
 //        make.centerY.mas_equalTo(locationView);
 //    }];
 //    // 地址Label
 //    UILabel *addressLabel = [[UILabel alloc] init];
 //    [locationView addSubview:addressLabel];
 //    self.addressLabel = addressLabel;
 //    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.left.mas_equalTo(locationIView.mas_right);
 //        make.top.bottom.mas_equalTo(locationView);
 //        make.right.mas_lessThanOrEqualTo(lineViewV.mas_left);
 //    }];
 
 //    UIView *separatorView = [[UIView alloc] init];
 //    separatorView.backgroundColor = [UIColor lightGrayColor];
 //    [locationView addSubview:separatorView];
 //    [separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.right.bottom.mas_equalTo(locationView);
 //        make.height.mas_equalTo(1);
 //        make.left.mas_equalTo(headerGap);
 //    }];
 
 // 本地之窗
 //    UIView *localNewsView = [[UIView alloc] init];
 //    [self addSubview:localNewsView];
 //    self.localNewsView = localNewsView;
 //    [localNewsView mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.top.mas_equalTo(locationView.mas_bottom);
 //        make.left.right.mas_equalTo(self);
 //        make.height.mas_equalTo(40);
 //    }];
 //
 //    UIImageView *localNewsIView = [[UIImageView alloc] init];
 //    localNewsIView.image = [UIImage imageNamed:@"shop_icon_local"];
 //    localNewsIView.contentMode = UIViewContentModeCenter;
 //    [localNewsView addSubview:localNewsIView];
 //    [localNewsIView mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.left.top.bottom.mas_equalTo(localNewsView);
 //        make.width.mas_equalTo(60);
 //    }];
 //
 //    UIImageView *arrowIIview = [[UIImageView alloc] init];
 //    arrowIIview.contentMode = UIViewContentModeCenter;
 //    arrowIIview.image = [UIImage imageNamed:@"list_arrow"];
 //    [localNewsView addSubview:arrowIIview];
 //    [arrowIIview mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.right.top.bottom.mas_equalTo(localNewsView);
 //        make.width.mas_equalTo(60);
 //    }];
 //
 //    UILabel *titleLabel = [[UILabel alloc] init];
 //    titleLabel.text = @"本地之窗（本地最新资讯）";
 //    [localNewsView addSubview:titleLabel];
 //    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.left.mas_equalTo(locationIView.mas_right);
 //        make.top.bottom.mas_equalTo(localNewsView);
 //        make.right.mas_lessThanOrEqualTo(arrowIIview.mas_left);
 //    }];
 //    [self mas_makeConstraints:^(MASConstraintMaker *make) {
 //        make.bottom.mas_equalTo(localNewsView);
 //    }];
 */

/*
 UIView *separatorView = [[UIView alloc] init];
 separatorView.backgroundColor = [UIColor lightGrayColor];
 [locationView addSubview:separatorView];
 [separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
 make.right.bottom.mas_equalTo(locationView);
 make.height.mas_equalTo(1);
 make.left.mas_equalTo(headerGap);
 }];
 
 // 本地之窗
 UIView *localNewsView = [[UIView alloc] init];
 [self addSubview:localNewsView];
 self.localNewsView = localNewsView;
 [localNewsView mas_makeConstraints:^(MASConstraintMaker *make) {
 make.top.mas_equalTo(locationView.mas_bottom);
 make.left.right.mas_equalTo(self);
 make.height.mas_equalTo(40);
 }];
 
 UIImageView *localNewsIView = [[UIImageView alloc] init];
 localNewsIView.image = [UIImage imageNamed:@"shop_icon_local"];
 localNewsIView.contentMode = UIViewContentModeCenter;
 [localNewsView addSubview:localNewsIView];
 [localNewsIView mas_makeConstraints:^(MASConstraintMaker *make) {
 make.left.top.bottom.mas_equalTo(localNewsView);
 make.width.mas_equalTo(60);
 }];
 
 UIImageView *arrowIIview = [[UIImageView alloc] init];
 arrowIIview.contentMode = UIViewContentModeCenter;
 arrowIIview.image = [UIImage imageNamed:@"wto_list_arrow"];
 [localNewsView addSubview:arrowIIview];
 [arrowIIview mas_makeConstraints:^(MASConstraintMaker *make) {
 make.right.top.bottom.mas_equalTo(localNewsView);
 make.width.mas_equalTo(60);
 }];
 
 UILabel *titleLabel = [[UILabel alloc] init];
 titleLabel.text = @"本地之窗（本地最新资讯）";
 [localNewsView addSubview:titleLabel];
 [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
 make.left.mas_equalTo(locationIView.mas_right);
 make.top.bottom.mas_equalTo(localNewsView);
 make.right.mas_lessThanOrEqualTo(arrowIIview.mas_left);
 }];
 [self mas_makeConstraints:^(MASConstraintMaker *make) {
 make.bottom.mas_equalTo(localNewsView);
 }];
 */
