//
//  WTOCustomNavBar.h
//  WolianwPro
//
//  Created by 赵丁丁 on 16/12/6.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WTOCustomNavBar;

typedef NS_ENUM(NSInteger,CustomNavBarClickType) {
    
    kShareButtonClickType,
    kMoreButtonClickType,
};

typedef NS_ENUM(NSInteger,CustomBarType) {
    
    kAppHomeType = 0,
    kShopHomeType,
};

@protocol WTOCustomNavBarDelegate <NSObject>

-(void)cusTomNavBarClick:(WTOCustomNavBar *)navBar withType:(CustomNavBarClickType) type;

@end

@interface WTOCustomNavBar : UIView

@property(nonatomic,assign)id<WTOCustomNavBarDelegate> delegate;
@property(nonatomic,assign)CustomBarType barType;

@property (nonatomic, copy) NSString *title;

#pragma mark publickMethods

//根据偏移量改变导航条alpha值
-(void)refrehNavBackGroundWithOffSet:(CGFloat)offSety;

@end
