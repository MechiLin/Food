//
//  WTOSureOrderSectionHeader.h
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/2.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WTOSureOrderSectionHeader;
@protocol WTOSureOrderSectionDelegate <NSObject>

-(void)clickSectionHeaderInMessege:(WTOSureOrderSectionHeader *)header;
-(void)clickSectionHeaderInCall:(WTOSureOrderSectionHeader *)header;

@end

@interface WTOSureOrderSectionHeader : UITableViewHeaderFooterView

@property(nonatomic,assign)id<WTOSureOrderSectionDelegate> delegate;

+(NSString *)cellIdentify;

-(void)refreshWithEntity;

@end
