//
//  ODLbScrollView.m
//  XxFinancial
//
//  Created by Owen on 15/12/24.
//  Copyright © 2015年 Owen. All rights reserved.
//

#import "ODLbScrollView.h"

@interface ODLbScrollView ()<UIScrollViewDelegate>
{
    NSInteger curIndex;
}

@property (nonatomic, strong) NSMutableArray *sourceMArr;
@property (nonatomic, strong) UIScrollView *mainSroll;

@property (nonatomic, strong) NSTimer *myTimer;

@property (nonatomic, copy) Tapback myBack;
@end

@implementation ODLbScrollView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame lbArrs:(NSArray *)arrays back:(Tapback)back
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.myBack = back;
        
        self.sourceMArr = [NSMutableArray arrayWithArray:arrays];
        [self initSourceViews:frame];
    }
    
    return self;
}

- (void)initSourceViews:(CGRect)frame
{
//    CGFloat height = frame.size.height;
    
    self.mainSroll = [[UIScrollView alloc] initWithFrame:self.bounds];
    self.mainSroll.delegate = self;
    [self.mainSroll setContentSize:CGSizeMake(0, 25 * _sourceMArr.count)];
    self.mainSroll.showsVerticalScrollIndicator = NO;
    self.mainSroll.scrollEnabled = NO;
    [self addSubview:_mainSroll];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [_mainSroll addGestureRecognizer:tap];
    
    for (int i = 0; i < _sourceMArr.count; i++) {
        
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(10, 25*i, CGRectGetWidth(self.frame), 25)];
        lb.text = _sourceMArr[i][@"title"];
//        lb.text = _sourceMArr[i];
        lb.font = [UIFont systemFontOfSize:14];
        lb.textColor = ZQColor(153, 153, 153);
        [_mainSroll addSubview:lb];
    }
    
    if (_sourceMArr.count > 1) {
        
        UILabel *lb_first = [[UILabel alloc] initWithFrame:CGRectMake(10, 25*(_sourceMArr.count), CGRectGetWidth(self.frame) - 20, 25)];
        lb_first.text = _sourceMArr[0][@"title"];
        //    lb_first.text = _sourceMArr[0];
        lb_first.textColor = ZQColor(153, 153, 153);
        lb_first.font = FontSet(14);
        [_mainSroll addSubview:lb_first];
        
        UILabel *lb_sec = [[UILabel alloc] initWithFrame:CGRectMake(10, 25*(_sourceMArr.count+1), CGRectGetWidth(self.frame) - 20, 25)];
        lb_sec.text = _sourceMArr[1][@"title"];
        //    lb_sec.text = _sourceMArr[1];
        lb_sec.textColor = ZQColor(153, 153, 153);
        lb_sec.font = FontSet(14);
        [_mainSroll addSubview:lb_sec];
    }else{
        
        UILabel *lb_sec = [[UILabel alloc] initWithFrame:CGRectMake(10, 25*_sourceMArr.count, CGRectGetWidth(self.frame) - 20, 25)];
        lb_sec.text = @"暂无活动";
        //    lb_sec.text = _sourceMArr[1];
        lb_sec.textColor = ZQColor(153, 153, 153);
        lb_sec.font = FontSet(14);
        [_mainSroll addSubview:lb_sec];
    }
    
    
    [_mainSroll setContentOffset:CGPointMake(0, 0) animated:NO];
    
    if (_sourceMArr.count > 1) {
        
        self.myTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(autoNextPage:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];
    }else{
        
    }
}

- (void)autoNextPage:(id)sender
{
    CGFloat height = self.mainSroll.frame.size.height;
//    NSLog(@" height ---> %f", height);

    CGFloat curY = _mainSroll.contentOffset.y;
//    NSLog(@"curY ---> %f", curY);
    
    [self.mainSroll setContentOffset:CGPointMake(0, curY + height) animated:YES];
}

#pragma mark - UIScrollViewDelegate

//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    [_myTimer invalidate];
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    _myTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(autoNextPage:) userInfo:nil repeats:YES];
//}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    curIndex = scrollView.contentOffset.y/25;
    
    if (curIndex == _sourceMArr.count) {
        [_mainSroll setContentOffset:CGPointMake(0, 0) animated:NO];
    }

}

#pragma mark - ----- tapAction ----

- (void)tapAction:(UITapGestureRecognizer *)tap
{
    if (_myBack) {
        
        self.myBack();
    }
}
@end
