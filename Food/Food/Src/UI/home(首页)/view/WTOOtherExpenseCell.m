//
//  WTOOtherExpenseCell.m
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/2.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTOOtherExpenseCell.h"
#import <Masonry/Masonry.h>

@interface WTOOtherExpenseCell ()

@property(nonatomic,weak)UILabel *expenseLabel;//经费类型label
@property(nonatomic,weak)UILabel *expenseCount;//经费额label

@end

@implementation WTOOtherExpenseCell

+(NSString *)cellIndentify
{
    return NSStringFromClass([self class]);
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self initSubViews];
    }
    
    return self;
}

-(void)initSubViews
{
    if (self.expenseLabel == nil) {
        
        UILabel *expenseLabel = [[UILabel alloc] init];
        expenseLabel.font = [UIFont systemFontOfSize:14.0f];
        expenseLabel.textColor = ZQColor(51, 51, 51);
        [self.contentView addSubview:expenseLabel];
        self.expenseLabel = expenseLabel;
        
        [self.expenseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.centerY.mas_equalTo(self.contentView);
        }];
    }
    
    if (self.expenseCount == nil) {
        
        UILabel *expenseCount = [[UILabel alloc] init];
        expenseCount.font = [UIFont systemFontOfSize:14.0f];
        expenseCount.textColor = ZQColor(51, 51, 51);
        [self.contentView addSubview:expenseCount];
        self.expenseCount = expenseCount;
        
        [self.expenseCount mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.trailing.mas_equalTo(@-15);
            make.centerY.mas_equalTo(self.contentView);
        }];
    }
}

-(void)setTitleColor:(UIColor *)titleColor
{
    self.expenseLabel.textColor = titleColor;
}

-(void)updateWithEntity:(WTOpackFareEntity *)entity
{
    self.expenseLabel.text = entity.fareName;
    self.expenseCount.text = [NSString stringWithFormat:@"¥ %.2f",[entity.fareMoney floatValue]];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
