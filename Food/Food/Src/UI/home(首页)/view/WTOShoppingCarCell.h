//
//  WTOShoppingCarCell.h
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/1.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MQLFoodModel.h"
#define kCellIdentifier_shopCar @"WTOShoppingCarCell"
@interface WTOShoppingCarCell : UITableViewCell

@property (nonatomic,copy) void (^operationBlock)(NSUInteger number,BOOL isPlus, NSString *goodsID);//点击按钮时所执行的block

@property (nonatomic,copy) NSString *goodsID;//商品ID

@property (nonatomic,assign) NSInteger number;//商品数量

@property (nonatomic, strong) MQLFoodModel *model;


- (void)showNumber:(NSUInteger)count;

@end
