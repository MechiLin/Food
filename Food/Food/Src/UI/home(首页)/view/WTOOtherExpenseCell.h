//
//  WTOOtherExpenseCell.h
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/2.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MQLFoodModel.h"

@interface WTOOtherExpenseCell : UITableViewCell

+(NSString *)cellIndentify;

@property(nonatomic,assign)UIColor *titleColor;

-(void)updateWithEntity:(WTOpackFareEntity *)entity;

@end
