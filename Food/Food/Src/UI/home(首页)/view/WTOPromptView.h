//
//  WTOPromptView.h
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/3.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTOPromptView : UIView

-(void)showWithProMessege:(NSString *)proMessege;

@end
