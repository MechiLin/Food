//
//  WTOCustomNavBar.m
//  WolianwPro
//
//  Created by 赵丁丁 on 16/12/6.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WTOCustomNavBar.h"
#import <Masonry/Masonry.h>


@interface WTOCustomNavBar ()

@property(nonatomic,weak)UIImageView *backGroundImage;//背景图片
@property(nonatomic,weak)UIButton *moreButton;//更多按钮
@property (nonatomic, strong) UILabel *titleLable;

@end

@implementation WTOCustomNavBar

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        [self initSubViews];
        
    }
    return self;
}

-(void)initSubViews
{
    // 背景
    UIImageView *image = [[UIImageView alloc] init];
    image.backgroundColor = [UIColor colorWithHexString:@"#fafafa"];
    [self addSubview:image];
    image.alpha = 0;
    
    
    self.backGroundImage = image;
    
    [self.backGroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    
    //更多按钮
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton setImage:[UIImage imageNamed:@"nav_menu02"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreButton];
    self.moreButton = moreButton;
    
    [moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.mas_equalTo(self.backGroundImage.mas_centerY).offset(StatusBarHeight/2);
        make.trailing.mas_equalTo(-(10 * k750Scale));
        make.size.mas_equalTo(CGSizeMake(30 * k750Scale, 30 * k750Scale));
    }];
    
    [self addSubview:self.titleLable];
   
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    
    self.titleLable.text = title;
}

#pragma mark - PublickMethods
//设置导航栏类型
-(void)setBarType:(CustomBarType)barType
{
    _barType = barType;
}

//根据偏移量改变导航条alpha值
-(void)refrehNavBackGroundWithOffSet:(CGFloat)offSety
{
    if (offSety > 44) {
        offSety = 44;
    }
    else if (offSety < -30) {
        offSety = -30;
    }
    
    if (offSety > 20) {      //半透明或者不透明状态
        
        [self refreshNormalType:NO];
        
    }else{
        
        [self refreshNormalType:YES];
        
    }
    
    self.backGroundImage.alpha = offSety/44;
    
    if (offSety <= 0) {
        self.alpha = 1 + offSety/30;
    }
    
}

//刷新导航栏样式
-(void)refreshNormalType:(BOOL)normalType
{
    if (normalType) {
        

        [self.moreButton setImage:[UIImage imageNamed:@"nav_menu02"] forState:UIControlStateNormal];
        // statusBar
        [UIApplication.sharedApplication setStatusBarStyle:UIStatusBarStyleLightContent];
        
    }else{
        
        [self.moreButton setImage:[UIImage imageNamed:@"nav_icon_menu"] forState:UIControlStateNormal];
        //
        [UIApplication.sharedApplication setStatusBarStyle:UIStatusBarStyleDefault];
    }
}


#pragma mark - UIActions


-(void)shareAction:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cusTomNavBarClick:withType:)]) {
        [self.delegate cusTomNavBarClick:self withType:kShareButtonClickType];
    }
}

-(void)moreButtonClick:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cusTomNavBarClick:withType:)]) {
        [self.delegate cusTomNavBarClick:self withType:kMoreButtonClickType];
    }
}


- (UILabel *)titleLable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc]initWithFrame:CGRectMake(ScreenW/4, StatusBarHeight, ScreenW/2, 44)];
        _titleLable.font = [UIFont boldSystemFontOfSize:20];
        _titleLable.textColor = [UIColor blackColor];
        _titleLable.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLable;
}

@end
