//
//  WTOSureOrderHeadView.m
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/2.
//  Copyright © 2017年 我连网. All rights reserved.
//


#import "WTOSureOrderHeadView.h"
#import <Masonry/Masonry.h>
#import "WTOAddaddressButton.h"
#import "HDAddressObject.h"

@interface WTOSureOrderHeadView ()

@property(nonatomic,weak)UIImageView *topLineImage;//头部线条
@property(nonatomic,weak)UILabel *goodsReceiptMan;//收货人提示
@property(nonatomic,weak)UILabel *receiptManLabel;//收货人
@property(nonatomic,weak)UILabel *receiptAddress;//收货地址提示
@property(nonatomic,weak)UILabel *receiptAddressLabel;//收货地址
@property(nonatomic,weak)UIImageView *clickMark;//点击标识
@property(nonatomic,weak)UIImageView *line;//线条
@property(nonatomic,weak)UILabel *serviceTime;//送达时间
@property(nonatomic,weak)UILabel *estimateTime;//预计送达时间
@property(nonatomic,weak)UIImageView *seperateLine;//分割线
@property(nonatomic,weak)WTOAddaddressButton *addAddressButton;//添加收货地址按钮

@end

@implementation WTOSureOrderHeadView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        self.frame = CGRectMake(0, 0, ScreenW, 230/2 + 8);
        
        [self initSubViews];
        
    }
    return self;
}

-(void)initSubViews
{
    UITapGestureRecognizer *tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickHeader:)];
    tapGest.numberOfTapsRequired = 1;
    [self addGestureRecognizer:tapGest];
    
    if (self.topLineImage == nil) {
        
        UIImageView *topLine = [[UIImageView alloc] init];
        topLine.image = [UIImage imageNamed:@"address_color-line"];
        [self addSubview:topLine];
        self.topLineImage = topLine;
        
        [self.topLineImage mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.top.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(ScreenW, 3));
        }];
    }
    
    if (self.clickMark == nil) {
        
        UIImageView *clickMark = [[UIImageView alloc] init];
        clickMark.image = [UIImage imageNamed:@"more_arrow"];
        [self addSubview:clickMark];
        self.clickMark = clickMark;
        
        [self.clickMark mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.top.mas_equalTo(@31);
            make.trailing.mas_equalTo(@(-15));
            make.size.mas_equalTo(CGSizeMake(6, 12.5));
        }];
    }
    
    if (self.goodsReceiptMan == nil) {
        
        UILabel *receiptMan = [[UILabel alloc] init];
        receiptMan.textColor = ZQColor(51, 51, 51);
        receiptMan.font = [UIFont systemFontOfSize:14.0f];
        receiptMan.numberOfLines = 1;
        receiptMan.text = @"收货人:";
        [self addSubview:receiptMan];
        self.goodsReceiptMan = receiptMan;
        
        [self.goodsReceiptMan mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(@17);
        }];
    }
    
    if (self.receiptManLabel == nil) {
        
        UILabel *receiveManLabel = [[UILabel alloc] init];
        receiveManLabel.textColor = ZQColor(51, 51, 51);
        receiveManLabel.font = [UIFont systemFontOfSize:14.0f];
        receiveManLabel.numberOfLines = 1.0f;
        [self addSubview:receiveManLabel];
        self.receiptManLabel = receiveManLabel;
        
        [self.receiptManLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(self.goodsReceiptMan.mas_trailing).mas_offset(@5);
            make.top.mas_equalTo(self.goodsReceiptMan);
            make.trailing.mas_offset(@-30);
        }];
    }
    
    if (self.receiptAddress == nil) {
        
        UILabel *receiptAddress = [[UILabel alloc] init];
        receiptAddress.textColor = ZQColor(51, 51, 51);
        receiptAddress.font = [UIFont systemFontOfSize:14.0f];
        receiptAddress.text = @"收货地址:";
        [self addSubview:receiptAddress];
        self.receiptAddress = receiptAddress;
        
        [self.receiptAddress mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(self.goodsReceiptMan.mas_bottom).mas_offset(@10);
            make.width.mas_equalTo(@75);
        }];
    }
    
    if (self.receiptAddressLabel == nil) {
        
        UILabel *receiptLabel = [[UILabel alloc] init];
        receiptLabel.font = [UIFont systemFontOfSize:14.0f];
        receiptLabel.textColor = ZQColor(51, 51, 51);
        receiptLabel.numberOfLines = 0;
        [self addSubview:receiptLabel];
        self.receiptAddressLabel = receiptLabel;
        
        [self.receiptAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(self.receiptAddress.mas_trailing).mas_offset(@5);
            make.top.mas_equalTo(self.receiptAddress);
            make.trailing.mas_offset(@-30);
        }];
    }
    
    if (self.line == nil) {
        
        UIImageView *line = [[UIImageView alloc] init];
        line.backgroundColor = LTXDefaultsLineColor;
        [self addSubview:line];
        self.line = line;
        
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(self);
            make.top.mas_equalTo(self.mas_bottom).mas_offset(@-45);
            make.size.mas_equalTo(CGSizeMake(ScreenW, 0.5));
        }];
    }
    
    if (self.serviceTime == nil) {
        
        UILabel *serviceTime = [[UILabel alloc] init];
        serviceTime.font = [UIFont systemFontOfSize:14.0f];
        serviceTime.textColor = ZQColor(51, 51, 51);
        serviceTime.text = @"送达时间";
        [self addSubview:serviceTime];
        self.serviceTime = serviceTime;
        
        [self.serviceTime mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(self.line.mas_bottom).mas_offset(@14);
        }];
    }
    
    if (self.estimateTime == nil) {
        
        UILabel *estimateTime = [[UILabel alloc] init];
        estimateTime.font = [UIFont systemFontOfSize:14.0f];
        estimateTime.textColor = ZQColor(53, 139, 238);
        estimateTime.numberOfLines = 1;
        [self addSubview:estimateTime];
        self.estimateTime = estimateTime;
        
        [self.estimateTime mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.trailing.mas_equalTo(@-15);
            make.centerY.mas_equalTo(self.serviceTime);
        }];
    }
    
    if (self.seperateLine == nil) {
        
        UIImageView *seperateLine = [[UIImageView alloc] init];
        seperateLine.backgroundColor = [UIColor colorWithHexString:@"#f2f6f6"];
        [self addSubview:seperateLine];
        self.seperateLine = seperateLine;
        
        [self.seperateLine mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.trailing.mas_equalTo(self);
            make.bottom.mas_equalTo(self);
            make.height.mas_equalTo(@8);
            
        }];
    }
    
}

-(void)clickHeader:(UITapGestureRecognizer *)tapGesture
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickOnorderHeadView:)]) {
        [self.delegate clickOnorderHeadView:self];
    }
}

//刷新数据
-(void)refreshWithAddressEntity:(HDAddressObject *)obj
{
    if ([obj isInvalidated]) {
        
        [self.goodsReceiptMan removeFromSuperview];
        [self.receiptManLabel removeFromSuperview];
        [self.receiptAddressLabel removeFromSuperview];
        [self.receiptAddress removeFromSuperview];
        [self.estimateTime removeFromSuperview];
        
        [self.addAddressButton mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(@25);
            make.size.mas_equalTo(CGSizeMake(120, 25));
        }];
        
        return;
    }
    
    //移除
    [self.addAddressButton removeFromSuperview];
    
    self.receiptManLabel.text = [NSString stringWithFormat:@"%@   %@",obj.realName,obj.mobile];
    self.receiptAddressLabel.text = obj.detailAddress;
    
    NSString *sendWayName = @"到店自提";
    
//    if ([orderEntity.storeInfo.sendtypeid integerValue] == 1004) {
//        sendWayName = @"我连专送";
//    }else if ([orderEntity.storeInfo.sendtypeid integerValue] == 1001){
//        sendWayName = @"同城配送";
//    }else if([orderEntity.storeInfo.sendtypeid integerValue] == 1002){
//        sendWayName = @"商家配送";
//    }else{
//        sendWayName = @"到店自提";
//    }
    
    NSString *sendWay = sendWayName;
    
//    NSDate *date = [NSDate date];
//
//    //当前时间戳
//    NSTimeInterval nowTimeInterval = [date timeIntervalSince1970];
//
//    //送达时间的时间戳
//    NSInteger sendCostTimeInterval = [orderEntity.storeInfo.avgSendCostTime integerValue] * 60 + nowTimeInterval;
//    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:sendCostTimeInterval];
//
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"hh:MM"];
    
    //送达时间字符串
//    NSString *timeString = [formatter stringFromDate:endDate];
    
    self.estimateTime.text = [NSString stringWithFormat:@"%@ %@之前",sendWay,[HDSellerObject sellerMsg].endBusinessTime];
    
}

#pragma mark - getter and setter
-(WTOAddaddressButton *)addAddressButton
{
    if (_addAddressButton == nil) {
        
        WTOAddaddressButton *addAddressButton = [WTOAddaddressButton buttonWithType:UIButtonTypeCustom];
        [addAddressButton setImage:[UIImage imageNamed:@"order_add_quantity"] forState:UIControlStateNormal];
        [addAddressButton.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
        [addAddressButton setTitle:@"添加收货地址" forState:UIControlStateNormal];
        addAddressButton.userInteractionEnabled = NO;
        [addAddressButton setTitleColor:LTXDefaultsColor forState:UIControlStateNormal];
        [self addSubview:addAddressButton];
        _addAddressButton = addAddressButton;
    }
    
    return _addAddressButton;
}

-(UILabel *)goodsReceiptMan
{
    if (_goodsReceiptMan == nil) {
        
        UILabel *receiptMan = [[UILabel alloc] init];
        receiptMan.textColor = ZQColor(51, 51, 51);
        receiptMan.font = [UIFont systemFontOfSize:14.0f];
        receiptMan.numberOfLines = 1;
        receiptMan.text = @"收货人:";
        [self addSubview:receiptMan];
        _goodsReceiptMan = receiptMan;
        
        [self.goodsReceiptMan mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(@17);
        }];
    }
    
    return _goodsReceiptMan;
}

-(UILabel *)receiptManLabel
{
    if (_receiptManLabel == nil) {
        
        UILabel *receiveManLabel = [[UILabel alloc] init];
        receiveManLabel.textColor = ZQColor(51, 51, 51);
        receiveManLabel.font = [UIFont systemFontOfSize:14.0f];
        receiveManLabel.numberOfLines = 1.0f;
        [self addSubview:receiveManLabel];
        _receiptManLabel = receiveManLabel;
        
        [_receiptManLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.leading.mas_equalTo(self.goodsReceiptMan.mas_trailing).mas_offset(@5);
            make.top.mas_equalTo(self.goodsReceiptMan);
            make.trailing.mas_offset(@-30);
        }];
    }
    
    return _receiptManLabel;
}

-(UILabel *)receiptAddressLabel
{
    if (_receiptAddressLabel == nil) {
        
        UILabel *receiptLabel = [[UILabel alloc] init];
        receiptLabel.font = [UIFont systemFontOfSize:14.0f];
        receiptLabel.textColor = ZQColor(51, 51, 51);
        receiptLabel.numberOfLines = 0.0f;
        [self addSubview:receiptLabel];
        _receiptAddressLabel = receiptLabel;
        
        [_receiptAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.leading.mas_equalTo(self.receiptAddress.mas_trailing).mas_offset(@5);
            make.top.mas_equalTo(self.receiptAddress);
            make.trailing.mas_offset(@-30);
        }];
    }
    
    return _receiptAddressLabel;
}

-(UILabel *)receiptAddress
{
    if (_receiptAddress == nil) {
        
        UILabel *receiptAddress = [[UILabel alloc] init];
        receiptAddress.textColor = ZQColor(51, 51, 51);
        receiptAddress.font = [UIFont systemFontOfSize:14.0f];
        receiptAddress.text = @"收货地址:";
        [self addSubview:receiptAddress];
        _receiptAddress = receiptAddress;
        
        [_receiptAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.leading.mas_equalTo(@10);
            make.top.mas_equalTo(self.goodsReceiptMan.mas_bottom).mas_offset(@10);
            make.width.mas_equalTo(@75);
            
        }];
    }
    
    return _receiptAddress;
}

-(UILabel *)estimateTime
{
    if (_estimateTime == nil) {
        
        UILabel *estimateTime = [[UILabel alloc] init];
        estimateTime.font = [UIFont systemFontOfSize:14.0f];
        estimateTime.textColor = ZQColor(53, 139, 238);
        estimateTime.numberOfLines = 1;
        [self addSubview:estimateTime];
        _estimateTime = estimateTime;
        
        [_estimateTime mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.trailing.mas_equalTo(@-15);
            make.centerY.mas_equalTo(self.serviceTime);
        }];
    }
    
    return _estimateTime;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
