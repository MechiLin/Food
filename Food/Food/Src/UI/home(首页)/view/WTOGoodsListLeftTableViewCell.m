//
//  WTOGoodsListLeftTableViewCell.m
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/5.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WTOGoodsListLeftTableViewCell.h"
#import <Masonry/Masonry.h>
@interface WTOGoodsListLeftTableViewCell ()

@property (nonatomic, strong) UIView *yellowView;//最前的标识
@end

@implementation WTOGoodsListLeftTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(35, 10, 60, 40)];
        self.name.numberOfLines = 0;
        self.name.font = [UIFont systemFontOfSize:13];
        self.name.textColor = ZQColor(51, 51, 51);
        self.name.highlightedTextColor = ZQColorRGBA(266, 78, 61, 1);
        [self.contentView addSubview:self.name];
        
        self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 17.5, 20, 20)];
        self.iconView.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:self.iconView];
        
        self.yellowView = [[UIView alloc] initWithFrame:CGRectMake(0, 15, 3, 25)];
        self.yellowView.backgroundColor = ZQColorRGBA(255, 78, 61, 1);
        [self.contentView addSubview:self.yellowView];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 54, 90, 1)];
        lineView.backgroundColor = ZQColor(228, 228, 228);
        [self.contentView addSubview:lineView];
        
    }
    return self;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    self.contentView.backgroundColor = selected ? [UIColor whiteColor] : ZQColor(248, 248, 248);
    self.highlighted = selected;
    self.name.highlighted = selected;
    self.yellowView.hidden = !selected;
}

- (void)layoutSubviews
{
    if (!self.iconView.image)
    {
        self.name.frame = CGRectMake(10, 10, 80, 40);
    }else
    {
        self.name.frame = CGRectMake(35, 10, 60, 40);
    }
    
}

@end
