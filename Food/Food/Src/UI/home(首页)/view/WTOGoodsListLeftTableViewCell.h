//
//  WTOGoodsListLeftTableViewCell.h
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/5.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kCellIdentifier_Left @"WTOGoodsListLeftTableViewCell"
@interface WTOGoodsListLeftTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *name;

@property (nonatomic, strong) UIImageView *iconView;

@end
