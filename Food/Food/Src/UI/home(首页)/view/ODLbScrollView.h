//
//  ODLbScrollView.h
//  XxFinancial
//
//  Created by Owen on 15/12/24.
//  Copyright © 2015年 Owen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^Tapback)(void);

@interface ODLbScrollView : UIView

- (id)initWithFrame:(CGRect)frame lbArrs:(NSArray *)arrays back:(Tapback)back;
@end
