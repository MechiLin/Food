//
//  WTOCoodsListTableViewHeaderView.m
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/5.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WTOCoodsListTableViewHeaderView.h"

@implementation WTOCoodsListTableViewHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = ZQColorRGBA(248, 248, 248, 1);
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 30)];
        self.name.font = [UIFont systemFontOfSize:14];
        self.name.textColor = ZQColor(102, 102, 102);
        [self addSubview:self.name];
    }
    return self;
}

@end
