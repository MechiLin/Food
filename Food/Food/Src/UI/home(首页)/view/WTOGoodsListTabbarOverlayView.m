//
//  WTOGoodsListTabbarOverlayView.m
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/1.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WTOGoodsListTabbarOverlayView.h"
#import "WTOGoodsListTabBarView.h"

@implementation WTOGoodsListTabbarOverlayView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    }
    return self;
}

//蒙版 点击蒙版的空白处就动画收回已选择的商品列表
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    UIView *view = touch.view;
    
    if (view == self) {
        
        [self.ShoppingCartView dismissAnimated:YES];
    }
}

@end
