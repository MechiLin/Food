//
//  WTOSureOrderSectionHeader.m
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/2.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTOSureOrderSectionHeader.h"
#import <Masonry/Masonry.h>
#import "SDImageCache.h"
#import "HDSellerObject.h"

@interface WTOSureOrderSectionHeader ()

@property(nonatomic,weak)UIImageView *logoImage;//店铺logo
@property(nonatomic,weak)UILabel *storeName;//店铺名称
@property(nonatomic,weak)UIButton *messegeButton;//消息按钮
@property(nonatomic,weak)UIButton *callButton;//拨打电话按钮

@end

@implementation WTOSureOrderSectionHeader

+(NSString *)cellIdentify
{
    return NSStringFromClass([self class]);
}

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        [self initSubViews];
    }
    
    return self;
}

-(void)initSubViews
{
    
    if (self.logoImage == nil) {
        
        UIImageView *logo = [[UIImageView alloc] init];
        logo.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:logo];
        self.logoImage = logo;
        
        [self.logoImage mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.centerY.mas_equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(15, 15));
        }];
    }
    
    if (self.storeName == nil) {
        
        UILabel *storeName = [[UILabel alloc] init];
        storeName.font = [UIFont systemFontOfSize:14.0f];
        storeName.textColor = ZQColor(51, 51, 51);
        storeName.numberOfLines = 1;
        [self.contentView addSubview:storeName];
        self.storeName = storeName;
        
        [self.storeName mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(self.logoImage.mas_trailing).mas_offset(@5);
            make.centerY.mas_equalTo(self.logoImage);
            make.width.mas_lessThanOrEqualTo(@200);
        }];
    }
    
    if (self.messegeButton == nil) {
        
        UIButton *messegeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [messegeButton setImage:[UIImage imageNamed:@"wto_order_message"] forState:UIControlStateNormal];
        [messegeButton addTarget:self action:@selector(clickMessegeButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:messegeButton];
        self.messegeButton = messegeButton;
        
        [self.messegeButton mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.size.mas_equalTo(CGSizeMake(25, 25));
            make.centerY.mas_equalTo(self.contentView);
            make.trailing.mas_equalTo(@-55);
        }];
    }
    
    if (self.callButton == nil) {
        
        UIButton *callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [callButton setImage:[UIImage imageNamed:@"wto_order_tel"] forState:UIControlStateNormal];
        [callButton addTarget:self action:@selector(clickCallButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:callButton];
        self.callButton = callButton;
        
        [self.callButton mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.trailing.mas_equalTo(@-15);
            make.size.mas_equalTo(CGSizeMake(25, 25));
            make.centerY.mas_equalTo(self.contentView);
        }];
    }
    
}

#pragma mark - UIActions
-(void)clickMessegeButton:(UIButton *)messegeButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickSectionHeaderInMessege:)]) {
        [self.delegate clickSectionHeaderInMessege:self];
    }
}

-(void)clickCallButton:(UIButton *)callButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickSectionHeaderInCall:)]) {
        [self.delegate clickSectionHeaderInCall:self];
    }
}

-(void)refreshWithEntity
{
    
    HDSellerObject *obj = [HDSellerObject sellerMsg];
    
    [self.logoImage sd_setImageWithURL:[NSURL URLWithString:obj.logoUrl] placeholderImage:nil];
    
    self.storeName.text = obj.name;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
