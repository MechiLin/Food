//
//  WTOGoodsCell.h
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/2.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MQLFoodModel.h"
@interface WTOGoodsCell : UITableViewCell

+(NSString *)cellIndentify;

-(void)updateWithEntityWithGoodsEntity:(MQLFoodModel *)entity;

@end
