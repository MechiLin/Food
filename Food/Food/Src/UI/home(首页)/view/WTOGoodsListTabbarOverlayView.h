//
//  WTOGoodsListTabbarOverlayView.h
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/1.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WTOGoodsListTabBarView;

@interface WTOGoodsListTabbarOverlayView : UIView

@property (nonatomic,weak) WTOGoodsListTabBarView *ShoppingCartView;

@end
