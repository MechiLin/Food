//
//  WTOAddaddressButton.m
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/17.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTOAddaddressButton.h"

@implementation WTOAddaddressButton

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(2, 2, 20, 20);
    self.titleLabel.frame = CGRectMake(self.imageView.right + 5, 2, 95, 20);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
