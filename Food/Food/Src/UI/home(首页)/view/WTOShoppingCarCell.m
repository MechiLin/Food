//
//  WTOShoppingCarCell.m
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/1.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WTOShoppingCarCell.h"
#import "Masonry.h"
#import "WTOShopCarCacheData.h"

@interface WTOShoppingCarCell ()

@property (strong, nonatomic) UILabel *nameLabel;//商品名字

@property (strong, nonatomic) UILabel *priceLabel;//单价

@property (strong, nonatomic) UILabel *numberLabel;//数量

@property (strong, nonatomic) UIButton *minus;//减

@property (strong, nonatomic) UIButton *plus;//加

@property (nonatomic, strong) UILabel *specName;//规格名字

@end

@implementation WTOShoppingCarCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self setupUI];
        [self setupLayout];
    }
    return self;
}

- (void)minus:(id)sender {
    
    self.number -= 1;
    [self showNumber:self.number];
    self.operationBlock(self.number,NO, self.goodsID);
}

- (void)plus:(id)sender {
    
        self.number += 1;
        [self showNumber:self.number];
        self.operationBlock(self.number,YES, self.goodsID);
}

- (void)showNumber:(NSUInteger)count
{
    self.numberLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.number];
    
    self.model.buyCount = self.number;
    
    if (self.number > 0)
    {
        [self.minus setHidden:NO];
        [self.numberLabel setHidden:NO];
    }
    else
    {
        [self.minus setHidden:YES];
        [self.numberLabel setHidden:YES];
    }
}



-(void)layoutSubviews
{
    [super layoutSubviews];
    [self showNumber:self.number];
}

- (void)setupUI
{
    //配置到时候出了标注再改
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    self.nameLabel.textColor = ZQColor(51, 51, 51);
    self.nameLabel.font = [UIFont boldSystemFontOfSize:16];
    [self.nameLabel sizeToFit];
    [self addSubview:self.nameLabel];
    
    self.specName = [[UILabel alloc]init];
    self.specName.textAlignment = NSTextAlignmentCenter;
    self.specName.textColor = ZQColor(153, 153, 153);
    self.specName.font = [UIFont boldSystemFontOfSize:13];
    [self.specName sizeToFit];
    [self addSubview:self.specName];
    
    
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.textAlignment = NSTextAlignmentCenter;
    self.priceLabel.textColor = ZQColor(51, 51, 51);
    self.priceLabel.font = [UIFont systemFontOfSize:16];
    [self.priceLabel sizeToFit];
    [self addSubview:self.priceLabel];
    
    self.minus = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.minus setImage:[UIImage imageNamed:@"order_minus_quantity"] forState:UIControlStateNormal];
    [self.minus addTarget:self action:@selector(minus:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.minus];
    
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.textColor = ZQColor(51, 51, 51);
    self.numberLabel.font = [UIFont systemFontOfSize:14];
    [self.numberLabel sizeToFit];
    [self addSubview:self.numberLabel];
    
    self.plus = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.plus setImage:[UIImage imageNamed:@"order_add_quantity"] forState:UIControlStateNormal];
    [self.plus addTarget:self action:@selector(plus:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.plus];
}

- (void)setupLayout
{
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(12);
        make.width.equalTo(@(100));
    }];
    
    [self.specName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(3);
        make.left.width.equalTo(self.nameLabel);
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    [self.plus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-20);
        make.width.height.equalTo(@(22));
    }];
    
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(40));
        make.centerY.equalTo(self.plus);
        make.right.equalTo(self.plus.mas_left);
    }];
    
    [self.minus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.centerY.equalTo(self.plus);
        make.right.equalTo(self.numberLabel.mas_left);
    }];
}

- (void)setModel:(MQLFoodModel *)model
{
    _model = model;

    self.goodsID = model.objectId;
    self.nameLabel.text = model.name;
    
    self.specName.text = @"";
    [self.nameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
    }];

    self.priceLabel.text = [NSString stringWithFormat:@"$%.2f",model.price];

    NSInteger count = model.buyCount;
    self.number = count;
    self.numberLabel.text = [NSString stringWithFormat:@"%zd",count];
    self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
