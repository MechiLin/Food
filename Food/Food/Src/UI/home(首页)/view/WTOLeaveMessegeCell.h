//
//  WTOLeaveMessegeCell.h
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/3.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTextView.h"

@interface WTOLeaveMessegeCell : UITableViewCell

@property(nonatomic,weak)MyTextView *invoceTextView;//留言板


+(NSString *)cellIndentify;

//获取留言
-(NSString *)getLeaveText;

@end
