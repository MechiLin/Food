//
//  WTOSureOrderHeadView.h
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/2.
//  Copyright © 2017年 我连网. All rights reserved.
//

#define kScrollBackGroundColor [UIColor colorWithRed:238/255.0 green:238/255.0  blue:238/255.0  alpha:0];
#define kThemeTextRedColor ZQColor(242, 87, 85)
#define kThemeLightGrayColor ZQColor(102, 102, 102)
#define kWTOShopGoodsHeadHeight 346/2


#import <UIKit/UIKit.h>
#import "HDSellerObject.h"
#import "HDAddressObject.h"
@class WTOSureOrderHeadView;
@protocol WTOSureOrderHeadViewDelegate <NSObject>

-(void)clickOnorderHeadView:(WTOSureOrderHeadView *)headView;

@end

@interface WTOSureOrderHeadView : UIView

@property(nonatomic,assign)id<WTOSureOrderHeadViewDelegate> delegate;

-(void)refreshWithAddressEntity:(HDAddressObject *)obj;//根据确认订单模型刷新

@end
