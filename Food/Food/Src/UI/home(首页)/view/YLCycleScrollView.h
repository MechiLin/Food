//
//  YLCycleScrollView.h
//  YLCycleScrollView
//
//  Created by 邓雨来 on 2017/3/23.
//  Copyright © 2017年 邓雨来. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const cycleScrollViewCellID = @"YLCycleScorllViewCell";
@class WTAHomeActivityEntity;
@interface YLCycleScrollView : UIView
/** 文字颜色 */
@property (nonatomic, strong) UIColor *titleColor;
/** 文字字体 */
@property (nonatomic, strong) UIFont *titleFont;
/** 设置滚动时间间隔(默认 3s) */
@property (nonatomic, assign) CGFloat timeInterval;

+ (instancetype)cycleScrollViewWithDataArr:(NSArray <WTAHomeActivityEntity *>*)dataArr clickBlock:(void(^)(NSInteger index, WTAHomeActivityEntity *promotionEntity))clickBlcok;
@end






@interface YLCycleScorllViewCellEntity : NSObject

/** 最左边的固定图标 */
@property (nonatomic, copy) NSString *icon;
/** 内容文字 */
@property (nonatomic, copy) NSString *contentTitle;

@end
