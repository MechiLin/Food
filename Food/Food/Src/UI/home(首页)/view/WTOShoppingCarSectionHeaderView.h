//
//  WTOShoppingCarSectionHeaderView.h
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/6.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WTOShoppingCarSectionHeaderView;
@protocol WTOShoppingCarSectionHeaderViewDelegate <NSObject>

@optional
- (void)clickClearShoppingBtnWithShoppingCarSectionHeaderView:(WTOShoppingCarSectionHeaderView *)view clearBtn:(UIButton *)clearBtn;

@end
@interface WTOShoppingCarSectionHeaderView : UIView

@property (nonatomic, weak) id<WTOShoppingCarSectionHeaderViewDelegate> delegate;

@property (nonatomic, assign) NSInteger foodCount;//共选多少

@end
