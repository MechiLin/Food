//
//  WTOShoppingCarBadgeView.m
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/1.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WTOShoppingCarBadgeView.h"

@interface WTOShoppingCarBadgeView ()

@property (nonatomic,strong) UILabel *textLabel;

@end

@implementation WTOShoppingCarBadgeView

/*
 最简单的数字提示view,没有涉及到字体，边框，内容的自适应等
 */
-(instancetype)initWithFrame:(CGRect)frame withString:(NSString *)string
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = ZQColor(255, 60, 41);
        self.layer.cornerRadius = frame.size.height /2;
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.textLabel.font = [UIFont systemFontOfSize:10.0f];
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.text = string;
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.textLabel];
        self.badgeValue = string;
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame withString:(NSString *)string withTextColor:(UIColor *)textColor
{
    return [self initWithFrame:frame withString:string];
}

-(void)setBadgeValue:(NSString *)badgeValue
{
    _badgeValue = badgeValue;
    if (badgeValue.integerValue > 99) {
        self.textLabel.text = @"99+";
    }else
    {
        self.textLabel.text = badgeValue;
    }
    if (!badgeValue || [badgeValue isEqualToString:@""] || ![badgeValue integerValue]) {
        self.hidden = YES;
    }
    else{
        self.hidden = NO;

    }
    
    
}

- (void)setTextColor:(UIColor *)textColor
{
    self.textLabel.textColor = textColor;
}

@end
