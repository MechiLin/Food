//
//  WLWShopContactCallView.h
//  OuLianWang
//
//  Created by 皇室美家 on 16/8/4.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLWShopContactCallView : UIView<UITableViewDelegate,UITableViewDataSource>

- (id)initWithFrame:(CGRect)frame withPhones:(NSArray *)phoneAry;

@end
