//
//  WTOShoppingCarBadgeView.h
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/1.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTOShoppingCarBadgeView : UIView

//购物车按钮的小红点
-(instancetype)initWithFrame:(CGRect)frame withString:(NSString *)string;

-(instancetype)initWithFrame:(CGRect)frame withString:(NSString *)string withTextColor:(UIColor *)textColor;

//数量
@property (nonatomic,strong) NSString *badgeValue;

//颜色设置 默认白色
@property (nonatomic,strong) UIColor *textColor;
@end
