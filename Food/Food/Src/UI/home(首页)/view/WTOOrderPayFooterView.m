//
//  WTOOrderPayFooterView.m
//  WolianwPro
//
//  Created by 赵丁丁 on 17/3/11.
//  Copyright © 2017年 我连网. All rights reserved.
//

#import "WTOOrderPayFooterView.h"
#import <Masonry/Masonry.h>

@interface WTOOrderPayFooterView ()

@property(nonatomic,weak)UIImageView *topLine;//头部线条
@property(nonatomic,weak)UILabel *orderMoneyPlaceHolder;//本单实付金额
@property(nonatomic,weak)UILabel *moneyLabel;//金额

@end

@implementation WTOOrderPayFooterView

+(NSString *)cellIndentify
{
    return NSStringFromClass([self class]);
}

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        [self initSubViews];
    }
    
    return self;
}

-(void)initSubViews
{
    
    if (self.topLine == nil) {
        
        UIImageView *topLine = [[UIImageView alloc] init];
        topLine.backgroundColor = LTXDefaultsLineColor;
        [self.contentView addSubview:topLine];
        self.topLine = topLine;
        
        [self.topLine mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.top.leading.trailing.mas_equalTo(self.contentView);
            make.height.mas_equalTo(@1);
        }];
    }
    
    if (self.orderMoneyPlaceHolder == nil) {
        
        UILabel *orderMoneyPlaceLabel = [[UILabel alloc] init];
        orderMoneyPlaceLabel.font = [UIFont systemFontOfSize:14.0f];
        orderMoneyPlaceLabel.text = @"本单实付";
        orderMoneyPlaceLabel.textColor = ZQColor(51, 51, 51);
        [self.contentView addSubview:orderMoneyPlaceLabel];
        self.orderMoneyPlaceHolder = orderMoneyPlaceLabel;
        
        [self.orderMoneyPlaceHolder mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.leading.mas_equalTo(@10);
            make.centerY.mas_equalTo(self.contentView);
        }];
    }
    
    if (self.moneyLabel == nil) {
        
        UILabel *moneyLabel = [[UILabel alloc] init];
        moneyLabel.font = [UIFont systemFontOfSize:16.0f];
        moneyLabel.textColor = LTXDefaultsColor;
        moneyLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:moneyLabel];
        self.moneyLabel = moneyLabel;
        
        [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.trailing.mas_equalTo(@-12);
            make.centerY.mas_equalTo(self.contentView);
            
        }];
    }
}

-(void)refreshWithTotalAmount:(NSNumber *)totalAmount
{
    self.moneyLabel.text = [NSString stringWithFormat:@"¥ %.2f",[totalAmount floatValue]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
