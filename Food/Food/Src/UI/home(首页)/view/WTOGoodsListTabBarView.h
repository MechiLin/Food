//
//  WTOGoodsListTabBarView.h
//  WolianwPro
//
//  Created by lishuaiWLW on 16/11/30.
//  Copyright © 2016年 我连网. All rights reserved.
//  购物车tabbar

#import <UIKit/UIKit.h>
#import "WTOShoppingCarBadgeView.h"
#import "MQLFoodModel.h"

typedef enum : NSUInteger {
    SEND_FARE_WOLIAN = 1004,//我连专送
    SEND_FARE_TONGCHENG = 1001,//同城配送
    SEND_FARE_SHOPSEND = 1002,//商家配送
} SEND_FARE_TYPE;

@class WTOGoodsListTabBarView,FoodModel;
@protocol WTOGoodsListTabBarViewDelegate <NSObject>

@optional
//计算价格
-(void)caculateMoney:(float)nTotal;

@required
//点击了计算按钮
- (void)clickPay:(UIButton *)payBtn;

-(void)addGoodsCount:(WTOGoodsListTabBarView *)tabbarView withModel:(MQLFoodModel *)model withIndexPath:(NSIndexPath *)indexPath;//添加商品

-(void)reduceGoodsCount:(WTOGoodsListTabBarView *)tabbarView withModel:(MQLFoodModel *)model withIndexPath:(NSIndexPath *)indexPath;//减少商品

-(void)clearnShopCar;//清空购物车

@end

//购物车tabbar;
@interface WTOGoodsListTabBarView : UIView
@property (nonatomic,weak) UIView *parentView;   //购物车所在的父view

@property (nonatomic,assign) BOOL up;//是否向上弹出tableView的标记

@property (nonatomic,strong) UITableView *orderList; //点击购物车展开的tableView

@property (nonatomic,strong) UIButton *shoppingCartBtn;//购物车按钮

@property (nonatomic,strong) WTOShoppingCarBadgeView *badge;//红点提示view

@property (nonatomic,assign) float minFreeMoney;//最少消费

@property (nonatomic,weak) id<WTOGoodsListTabBarViewDelegate>delegate;

@property (nonatomic,copy) NSString *payBtnString;//结算按钮的文本

@property (nonatomic, assign) float shoppingFare;//配送费

@property (nonatomic, assign) SEND_FARE_TYPE sendType;//配送类型

#pragma mark - add by zhaodd
//创建的方法
-(instancetype) initWithFrame:(CGRect)frame inView:(UIView *)parentView;

//收起动画
-(void)dismissAnimated:(BOOL) animated;

//设置购物车数据源
-(void)setShopCarDataSourceWithArray:(NSArray *)array;

//清空购物车
-(void)clearnShopCar;

//获取购物车数据源
-(NSArray *)getShopCarDataSource;



#pragma mark - refreshMethods
//数据源有变动的情况下刷新购物车
-(void)refreshShopCar;

//刷新购物车图标
- (void)refreshCartImage;

//设置购物车图标个数
-(void)setBadgeWithNum:(NSString *)numString;

//刷新头部sectionheader
-(void)refreshSectionHeaderBuyCount;

//刷新表格
-(void)refreshList;

//计算餐盒费,总价格，并设置
- (void)calculateOrderFareAndSetTotolMoney;

//获取列表对象
-(UITableView *)getShopCarTableView;

//刷新frame
-(void)updateFrame;

////根据id获取购物车内该规格的商品
//-(WTOGoodsDetailEntity *)getCacheModelWithStoreid:(NSString *)storeid withGoodsid:(NSString *)goodsid withSpecid:(NSString *)specid;
//
////根据id判断该规格的商品是否存在于购物车
//-(BOOL)checkHasCacheGoodsEntityWithStoreid:(NSString *)storeid withGoodsid:(NSString *)goodsid withSpecid:(NSString *)specid;
//
////根据id在缓存中获取总数量
//-(long)getCacheBuyCountWithStoreid:(NSString *)storeid;

//在内存获取总数量
-(long)getBuyCountFromShopCarDataSource;

-(void)removeFoodModelAtShopCar:(MQLFoodModel *)model;

-(void)addFoodModelToShopCar:(MQLFoodModel *)model;

@end
