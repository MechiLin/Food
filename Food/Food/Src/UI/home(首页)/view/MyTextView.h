//
//  MyTextView.h
//  OuLianWang
//
//  Created by 皇室美家 on 15/7/17.
//  Copyright (c) 2015年 皇室美家. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTextView : UITextView
@property (nonatomic, copy) NSString *placehoder;
@property (nonatomic, strong) UIColor *placehoderColor;
@property (nonatomic, copy) void(^textChangedBlock)(NSString *currentText);
/** 是否使用font计算占位文字label的高度，为兼容以前的方法，新增该属性 */
@property (nonatomic, assign) BOOL calculatePHLabelHeightByFont;
@end
