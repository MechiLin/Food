//
//  WLWGoodsListTabViewCell.m
//  WolianwPro
//
//  Created by lishuaiWLW on 16/12/1.
//  Copyright © 2016年 我连网. All rights reserved.
//

#import "WLWGoodsListTabViewCell.h"
#import "Masonry.h"
#import "MQLFoodModel.h"
#import "UIImageView+WebCache.h"

@interface WLWGoodsListTabViewCell ()

@property (nonatomic, strong) UIImageView *foodImageView;//商品图片

@property (nonatomic, strong) UILabel *titleLabel;//标题

@property (nonatomic, strong) UIImageView *signatureImageView;//招牌
//
@property (nonatomic, strong) UILabel *productLabel;//售罄标识

@property (nonatomic, strong) UILabel *describeLabel;//商品描述

@property (nonatomic, strong) UILabel *unitPriceLabel;//单价

@property (nonatomic, strong) UILabel *orderCount;//总数量


@end
@implementation WLWGoodsListTabViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    //商品图片 切角  显示中间部分
    self.foodImageView = [[UIImageView alloc]init];
    self.foodImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.foodImageView.layer.cornerRadius = 2;
    self.foodImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.foodImageView];
    //标题
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    self.titleLabel.textColor = ZQColor(51, 51, 51);
    self.titleLabel.numberOfLines = 2;
    [self.titleLabel sizeToFit];
    [self.contentView addSubview:self.titleLabel];
    
    //招牌  后边要根据后台数据判断是否显示
    self.signatureImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.signatureImageView];
    //已售罄的标识
    self.productLabel = [[UILabel alloc]init];
    self.productLabel.font = [UIFont systemFontOfSize:12];
    self.productLabel.textAlignment = NSTextAlignmentCenter;
    self.productLabel.backgroundColor = ZQColorRGBA(0, 0, 0, 0.5);
    self.productLabel.textColor = [UIColor whiteColor];//ZQColor(153, 153, 153);
    self.productLabel.text = @"已售罄";
    self.productLabel.hidden = YES;
    [self.productLabel sizeToFit];
    [self.contentView addSubview:self.productLabel];
    
    //菜品描述
    self.describeLabel = [[UILabel alloc]init];
    self.describeLabel.numberOfLines = 2;
    self.describeLabel.font = [UIFont systemFontOfSize:10];
    self.describeLabel.textColor = ZQColor(153, 153, 153);
    [self.contentView addSubview:self.describeLabel];

    self.plus = [UIButton buttonWithType:UIButtonTypeCustom];

    [self.plus setImage:[UIImage imageNamed:@"order_add_quantity"] forState:UIControlStateNormal];
    [self.plus addTarget:self action:@selector(plus:) forControlEvents:UIControlEventTouchUpInside];
    self.plus.hidden = NO;
    [self.contentView addSubview:self.plus];
    

    self.minus = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.minus setImage:[UIImage imageNamed:@"order_minus_quantity"] forState:UIControlStateNormal];
    [self.minus addTarget:self action:@selector(minus:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.minus];
    self.minus.hidden = YES;
    
    self.orderCount = [[UILabel alloc]init];
    self.orderCount.textColor = [UIColor blackColor];
    self.orderCount.textAlignment = NSTextAlignmentCenter;
    self.orderCount.font = [UIFont boldSystemFontOfSize:16];
    [self.orderCount sizeToFit];
    [self.contentView addSubview:self.orderCount];
    self.orderCount.hidden = YES;
    
    self.unitPriceLabel = [[UILabel alloc]init];
    
    self.unitPriceLabel.textAlignment = NSTextAlignmentLeft;
    
    self.unitPriceLabel.font = [UIFont boldSystemFontOfSize:14];
    
    self.unitPriceLabel.textColor = ZQColor(246, 2, 5);
    
    [self.unitPriceLabel sizeToFit];
    [self.contentView addSubview:self.unitPriceLabel];
  
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = ZQColor(228, 228, 228);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
    
    [self setupLayout];
}

- (void)setupLayout
{
    [self.foodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(57);
        make.top.mas_equalTo(self.contentView).offset(14);
        make.left.mas_equalTo(self.contentView).offset(11);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.foodImageView.mas_right).offset(10);
        make.top.equalTo(self.foodImageView);
        make.right.equalTo(self).offset(-10);
    }];
    
    [self.signatureImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self);
        make.height.width.equalTo(@(28));
    }];

    [self.productLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.foodImageView);
    }];
    
    [self.describeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(- 5);
        make.left.equalTo(self.titleLabel.mas_left);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
    }];
    
    [self updataUI];
}

- (void)updataUI
{
    
//    [self.specBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(50);
//        make.height.right.bottom.mas_equalTo(self.plus);
//    }];
//
//    [self.countTagLable mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.width.height.mas_equalTo(14);
//        make.centerX.mas_equalTo(self.specBtn.mas_right);
//        make.centerY.mas_equalTo(self.specBtn.mas_top);
//    }];
    
    [self.orderCount mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.plus.mas_left);
        make.centerY.equalTo(self.plus);
        make.width.mas_equalTo(30);
    }];
    
    [self.minus mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.orderCount.mas_left);
        make.width.height.centerY.mas_equalTo(self.plus);
    }];
    
    [self.unitPriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.centerY.equalTo(self.minus.mas_centerY);
    }];

}

- (void)plus:(id)sender {
    
    self.amount += 1;
    self.plusBlock(self.amount,YES);

    [self showOrderNumbers:self.amount];
    
}
- (void)minus:(id)sender {
    
    if (self.amount <= 0) {
        
        return;
    }
    self.amount -= 1;
    
    self.plusBlock(self.amount,NO);
    
    [self showOrderNumbers:self.amount];
}




-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self showOrderNumbers:self.amount];
    
}

- (void)clickJoinShopCarWithPlus:(BOOL)plus
{
    if (plus) {
        self.amount += 1;
    }else
    {
        
        self.amount -= 1;
    }
    [self showOrderNumbers:self.amount];
    
}

-(void)showOrderNumbers:(NSInteger)count
{
    self.model.buyCount = count;
    if (self.amount > 99) {
        self.orderCount.text = [NSString stringWithFormat:@"99+"];
    }else
    {
        self.orderCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.amount];
    }
    if (self.amount > 0)
    {
        [self.minus setHidden:NO];
        [self.orderCount setHidden:NO];
    }
    else
    {
        [self.minus setHidden:YES];
        [self.orderCount setHidden:YES];
    }
}

- (void)setModel:(MQLFoodModel *)model
{
    _model = model;
    
//    看库存 如果为0 隐藏按钮 显示已售罄
    if (![self checkInventory]) {
        self.plus.hidden = YES;
        self.minus.hidden = YES;
        self.productLabel.hidden = NO;
    }else
    {
        self.productLabel.hidden = YES;

        self.plus.hidden = NO;
    }

    [self.foodImageView sd_setImageWithURL:[NSURL URLWithString:model.imgUrl]];
    self.titleLabel.text = model.name;
//    self.monthFaledLable.text = [NSString stringWithFormat:@"月售%zd份 赞 %zd", model.monthSlaesAmount.integerValue, model.likeAmount.integerValue];

//    __block WTOSpeciListEntity *minPriceComb = nil;
//    __block WTOSpeciListEntity *maxCouponsComb = nil;
//    if (model.speciList.count > 1) {
//        // 遍历选出最低折扣的规格
//        [model.speciList enumerateObjectsUsingBlock:^(WTOSpeciListEntity * _Nonnull comb, NSUInteger idx, BOOL * _Nonnull stop) {
//            if (!minPriceComb && comb.discountPrice.floatValue > 0) {
//                minPriceComb = comb;
//            } else {
//                if (minPriceComb && minPriceComb.discountPrice.floatValue > comb.discountPrice.floatValue && comb.isDiscountPriceUsable && comb.discountPrice.floatValue > 0) {
//                    minPriceComb = comb;
//                }
//            }
//            if (!maxCouponsComb && comb.coupons.floatValue>0) {
//                maxCouponsComb = comb;
//            }else
//            {
//                if (maxCouponsComb && maxCouponsComb.coupons.floatValue > comb.coupons.floatValue && comb.coupons.floatValue > 0) {
//                    maxCouponsComb = comb;
//                }
//            }
//        }];
//    }else {
//
//        maxCouponsComb = minPriceComb = [model.speciList lastObject];
//    }

//    self.discountLabel.hidden = !minPriceComb.isDiscountPriceUsable.integerValue;
//    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
//    NSString *discountPrice = minPriceComb.isDiscountPriceUsable ?minPriceComb.unitPrice : minPriceComb.discountPrice;
//    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%.2f", discountPrice.floatValue] attributes:attribtDic];
//    self.discountLabel.attributedText = attribtStr;
//    self.discountLabel.textColor = ZQColor(153, 153, 153);
////    }
//
//    if (model.goodstagid.integerValue == 1) {
//        self.signatureImageView.image = [UIImage imageNamed:@"lab_new"];
//    } else if (model.goodstagid.integerValue == 2) {
//        self.signatureImageView.image = [UIImage imageNamed:@"lab_spicy"];
//    }else if (model.goodstagid.integerValue == 3) {
//        self.signatureImageView.image = [UIImage imageNamed:@"lab_zp"];
//    }else
//    {
//        self.signatureImageView.image = [UIImage imageNamed:@""];
//    }
//
//    self.describeLabel.text = model.goodsDesc;
//    [self.describeLabel sizeToFit];
//    [self.monthFaledLable mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.mas_right);
//        make.left.equalTo(self.titleLabel);
//        make.top.equalTo(self.describeLabel.mas_bottom).offset(8);
//    }];
//
    [self.plus mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(22);
        make.height.mas_equalTo(22);
        make.right.mas_equalTo(self.contentView).offset(- 10);
        make.top.mas_equalTo(self.foodImageView.mas_bottom).offset(10);
    }];

    [self updataUI];
//
    self.unitPriceLabel.text = [NSString stringWithFormat:@"$%.2f",model.price];
//
    [self.unitPriceLabel sizeToFit];
//    [self.discountLabel sizeToFit];
////    self.discountLabel.hidden = minPriceComb.isDiscountPriceUsable == NO;
//    if (IS_IPHONE_5) {
//
//        [self.discountLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.unitPriceLabel);
//            make.top.equalTo(self.unitPriceLabel.mas_bottom).offset(2);
//        }];
//    }else
//    {
//
//        [self.discountLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.unitPriceLabel.mas_right).offset(5);
//            make.centerY.equalTo(self.unitPriceLabel);
//            make.trailing.equalTo(self.minus.mas_leading);
//        }];
//    }
//
//    //这里判断展示什么促销标签
//    if (model.goodsTimeLimitSaleInfo == nil) {
//
//        self.disCountDescLabel.hidden = YES;
//        [self.disCountDescLabel mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(0);
//        }];
//    }else
//    {
//        self.disCountDescLabel.hidden = NO;
//        [self.disCountDescLabel mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(13);
//        }];
//    }
//
//    self.disCountDescLabel.discountStr = model.goodsTimeLimitSaleInfo.limitnum.integerValue ? [NSString stringWithFormat:@" %.1f折 每单限%zd份 ",model.goodsTimeLimitSaleInfo.discount.floatValue/10,model.goodsTimeLimitSaleInfo.limitnum.integerValue] : [NSString stringWithFormat:@" %.1f折 ",model.goodsTimeLimitSaleInfo.discount.floatValue/10];
//    self.disCountDescLabel.type = DISCOUNT_TYPE_DISCO;
//
//    if (maxCouponsComb.coupons.floatValue > 0) {
//
//        self.SalesDescLabel.hidden = NO;
//        [self.SalesDescLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.unitPriceLabel);
//            if (_disCountDescLabel.hidden) {
//                make.top.mas_equalTo(self.disCountDescLabel);
//            }else
//            {
//                make.top.mas_equalTo(self.disCountDescLabel.mas_bottom).offset(5);
//            }
//        }];
//
//        self.SalesDescLabel.discountStr = model.speciList.count > 1?[NSString stringWithFormat:@" 代金券可折扣%@元起 ",maxCouponsComb.coupons]:[NSString stringWithFormat:@" 代金券可折扣%@元 ",maxCouponsComb.coupons];
//        self.SalesDescLabel.type = DISCOUNT_TYPE_OFFSET;
//    }else
//    {
//        self.SalesDescLabel.hidden = YES;
//    }
//
    self.amount = model.buyCount;

    [self showOrderNumbers:self.amount];
}

- (void)setIsBusiness:(BOOL)isBusiness
{
    if (isBusiness) {
        self.plus.enabled = NO;
        self.minus.enabled = NO;
    }
}

//判断没有库存  就显示已售罄的方法
- (NSInteger)checkInventory
{
    NSInteger count = 1;
//    for (WTOSpeciListEntity *comModel in self.model.speciList) {
//
//        count += comModel.inventory.integerValue;
//    }
    
    return count;
}

@end

@interface WTOGoodsListDiscountView ()

@property (nonatomic, strong) UILabel *discountLabel;

@property (nonatomic, strong) UIImageView *discountImg;
@end

@implementation WTOGoodsListDiscountView

- (instancetype)initWithType:(DISCOUNT_TYPE)type
{
    if (self = [super init]) {
        
        if (!_discountImg) {
            
            _discountImg = [[UIImageView alloc]init];
            
            switch (type) {
                case DISCOUNT_TYPE_OFFSET:
                    _discountImg.image = [UIImage imageNamed:@"lab_coupon"];
                    break;
                    
                case DISCOUNT_TYPE_DISCO:
                    _discountImg.image = [UIImage imageNamed:@"lab_discount"];
                    break;
                    
                default:
                    break;
            }
            [self addSubview:_discountImg];
            
            [_discountImg mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.width.mas_equalTo(14);
                make.left.centerY.mas_equalTo(self);
            }];
        }
        
        if (!_discountLabel) {
            _discountLabel = [[UILabel alloc]init];
            _discountLabel.textColor = ZQColor(153, 153, 153);
            _discountLabel.font = [UIFont systemFontOfSize:13];
            
            [self addSubview:_discountLabel];
            
            [_discountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.discountImg.mas_right).offset(3);
                make.top.bottom.right.equalTo(self);
            }];
        }
    }
    
    return self;
}


- (void)setDiscountStr:(NSString *)discountStr
{
    _discountLabel.text = discountStr;
}


- (void)setType:(DISCOUNT_TYPE)type
{
    switch (type) {
        case DISCOUNT_TYPE_OFFSET:
            _discountImg.image = [UIImage imageNamed:@"lab_coupon"];
            break;
            
        case DISCOUNT_TYPE_DISCO:
            _discountImg.image = [UIImage imageNamed:@"lab_discount"];
            break;
            
        default:
            break;
    }
}
@end
