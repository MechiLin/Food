//
//  HDUserModel.h
//  Food
//
//  Created by admin on 2018/8/9.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDUserModel : NSObject
@property (nonatomic,copy) NSString *objectId;
@property (nonatomic,copy) NSString *nickName;
@end
