//
//  HDFCategory.m
//  Food
//
//  Created by 包月兴 on 2018/7/25.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "HDFCategoryObject.h"
#import "RLMObject+Extension.h"
#import "HDBmobFramework.h"
#import <BmobSDK/Bmob.h>
#import "HDFFoodObject.h"

@implementation HDFCategoryObject


+(NSString *)primaryKey
{
    return @"objectId";
}


+(NSArray<NSString *> *)indexedProperties
{
    return @[@"priority",@"updatedAt"];
}


+(NSArray<NSString *> *)ignoredProperties
{
    return @[@"foodArr"];
}


+(void)insertDataWithBmobObjectLists:(NSArray<BmobObject *> *)lists complete:(void (^)(NSError * _Nullable))complete
{
    __block NSMutableArray<HDFCategoryObject *> *arr = [NSMutableArray array];
    [lists enumerateObjectsUsingBlock:^(BmobObject * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HDFCategoryObject *object = [[HDFCategoryObject alloc] initWithBmobObject:obj];
        [arr addObject:object];
    }];
    if (arr.count > 0) {
        [self asynDataBlock:^{
            RLMRealm *realm = [RLMRealm defaultRealm];
            NSError *error;
            [realm transactionWithBlock:^{
                 [realm addOrUpdateObjects:arr];
            } error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (complete) {
                    complete(error);
                }
            });
        }];
        return;
    }
    if (complete) {
        complete(nil);
    }
    
}

+(RLMResults *)listOrderByAscendingWithPriority
{
    return [[HDFCategoryObject allObjects] sortedResultsUsingKeyPath:@"priority" ascending:YES];
}

+(NSDate *)getNewUpdatedAt
{
    RLMResults *results =[[HDFCategoryObject allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:YES];
    if (results.count > 0) {
        HDFCategoryObject *object = results.lastObject;
        return object.updatedAt;
    }else
    {
        return nil;
    }
}


-(instancetype)initWithBmobObject:(BmobObject *)object
{
    if (self = [super init]) {
        _objectId = [object objectForKey:@"objectId"];
        _name = [object objectForKey:@"title"];
        _updatedAt = [BmobDataInstance.formatter dateFromString: [object objectForKey:@"updatedAt"]];
        _priority =  [[object objectForKey:@"priority"] integerValue];
       
    }
    return self;
}


- (NSArray *)foodArr
{
    if (!_foodArr) {
        
        NSMutableArray *foods = [NSMutableArray array];
        RLMResults *results = [HDFFoodObject listOrderByAscendingWithPriorityWithCategoryId:self.objectId];
        for (HDFFoodObject *obj in results)
        {
            MQLFoodModel *model = [[MQLFoodModel alloc]init];
            model.objectId = obj.objectId;
            model.name = obj.name;
            model.priority = obj.priority;
            model.updatedAt = obj.updatedAt;
            model.imgUrl = obj.imgUrl;
            model.price = obj.price;
            model.categoryId = obj.categoryId;
            model.buyCount = 0;
            [foods addObject:model];
        }
        
        _foodArr = foods.copy;
    }
    return _foodArr;
}




@end
