//
//  MQLFoodModel.h
//  Food
//
//  Created by 李宗帅 on 2018/8/9.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MQLFoodModel : NSObject

@property (nonatomic,copy) NSString *objectId;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,assign) NSInteger priority;
@property (nonatomic,copy) NSDate *updatedAt;
@property (nonatomic,copy) NSString *imgUrl;
@property (nonatomic,assign) double price;
@property (nonatomic,copy) NSString *categoryId;
@property (nonatomic,assign) NSInteger buyCount;//当前点了多少?

@end


@interface MQLCategoryObject : NSObject

@property (nonatomic,copy) NSString *objectId;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,assign) NSInteger priority;
@property (nonatomic,copy) NSDate *updatedAt;
@property (nonatomic,strong) NSArray *foodArr;

@end

@interface WTOpackFareEntity : NSObject

@property(nonatomic,copy)NSString *fareName;//费用名称
@property(nonatomic,copy)NSString *fareMoney;//费用

@end

