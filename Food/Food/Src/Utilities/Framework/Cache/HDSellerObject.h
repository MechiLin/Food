//
//  HDSellerObject.h
//  Food
//
//  Created by admin on 2018/8/9.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "RLMObject.h"
NS_ASSUME_NONNULL_BEGIN
@class BmobObject;
@interface HDSellerObject : RLMObject
@property (nonatomic,copy) NSString *objectId;
@property (nonatomic,copy) NSString *name;  //商户名称
@property (nonatomic,copy) NSString *logoUrl;  //商户icon
@property (nonatomic,copy) NSString *address; //商户地址
@property (nonatomic,copy) NSString *startBusinessTime; //营业开始时间
@property (nonatomic,copy) NSString *endBusinessTime; //营业结束时间
@property (nonatomic,copy) NSString *telPhone; //联系电话
@property (nonatomic,copy) NSDate *updatedAt; //更新时间
+(void)insertDataWithBmobObjectLists:(NSArray<BmobObject *>*)lists
                            complete:(void (^)(NSError * _Nullable error))complete;

+(HDSellerObject *)sellerMsg;
+(NSDate *)getNewUpdatedAt;
@end
NS_ASSUME_NONNULL_END
RLM_ARRAY_TYPE(HDSellerObject)
