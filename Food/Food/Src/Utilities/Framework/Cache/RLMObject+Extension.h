//
//  RLMObject+Extension.h
//  HDAI
//
//  Created by 包月兴 on 2018/3/13.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import <Realm/Realm.h>

@interface RLMObject (Extension)
/**
 子线程异步修改和新增数据
 
 @param block 回调
 */
+(void)asynDataBlock:(dispatch_block_t)block;


/**
 主线程异步修改和新增数据
 
 @param block 回调
 */
+(void)mainAsynDataBlock:(dispatch_block_t)block;


@end
