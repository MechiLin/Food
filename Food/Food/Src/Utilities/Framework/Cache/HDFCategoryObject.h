//
//  HDFCategory.h
//  Food
//
//  Created by 包月兴 on 2018/7/25.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import <Realm/Realm.h>
#import "MQLFoodModel.h"
@class BmobObject;
NS_ASSUME_NONNULL_BEGIN
@interface HDFCategoryObject : RLMObject
@property (nonatomic,copy) NSString *objectId;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,assign) NSInteger priority;
@property (nonatomic,copy) NSDate *updatedAt;
@property (nonatomic,strong) NSArray *foodArr;


+(void)insertDataWithBmobObjectLists:(NSArray<BmobObject *>*)lists
                           complete:(void (^)(NSError * _Nullable error))complete;
+(RLMResults *)listOrderByAscendingWithPriority;

+(NSDate *)getNewUpdatedAt;


@end
RLM_ARRAY_TYPE(HDFCategoryObject)
NS_ASSUME_NONNULL_END
