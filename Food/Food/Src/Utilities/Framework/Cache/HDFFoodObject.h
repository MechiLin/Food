//
//  HDFFoodObject.h
//  Food
//
//  Created by 包月兴 on 2018/7/26.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import <Realm/Realm.h>
@class BmobObject;
NS_ASSUME_NONNULL_BEGIN
@interface HDFFoodObject : RLMObject
@property (nonatomic,copy) NSString *objectId;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,assign) NSInteger priority;
@property (nonatomic,copy) NSDate *updatedAt;
@property (nonatomic,copy) NSString *imgUrl;
@property (nonatomic,assign) double price;
@property (nonatomic,copy) NSString *categoryId;
@property (nonatomic,assign) NSInteger *buyCount;//当前点了多少?

+(void)insertDataWithBmobObjectLists:(NSArray<BmobObject *>*)lists
                            complete:(void (^)(NSError * _Nullable error))complete;

+(RLMResults *)listOrderByAscendingWithPriorityWithCategoryId:(NSString *)categoryId;

+(NSDate *)getNewUpdatedAt;
@end
NS_ASSUME_NONNULL_END
RLM_ARRAY_TYPE(HDFFoodObject)


