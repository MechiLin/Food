//
//  HDOrderObject.m
//  Food
//
//  Created by admin on 2018/8/11.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "HDOrderObject.h"
#import <BmobSDK/Bmob.h>
#import "RLMObject+Extension.h"
#import "HDSellerObject.h"
@implementation HDOrderDetailObject
+(instancetype)objectId:(NSString *)objectId name:(NSString *)name imgUrl:(NSString *)imgUrl price:(double)price buyCount:(NSInteger)buyCount
{
    return [[HDOrderDetailObject alloc] initWithobjectId:objectId name:name imgUrl:imgUrl price:price buyCount:buyCount];
}


-(instancetype)initWithobjectId:(NSString *)objectId name:(NSString *)name imgUrl:(NSString *)imgUrl price:(double)price buyCount:(NSInteger)buyCount
{
    if (self = [super init]) {
        _objectId = objectId;
        _name = name;
        _imgUrl = imgUrl;
        _price = price;
        _buyCount = buyCount;
    }
    return self;
}

@end
@implementation HDOrderObject
+(NSString *)primaryKey
{
    return @"objectId";
}
+(NSArray<NSString *> *)indexedProperties
{
    return @[@"updatedAt"];
}


+(void)insertDataWithBmobObjectLists:(NSArray<BmobObject *> *)lists complete:(void (^)(NSError * _Nullable))complete
{
    
    HDSellerObject *sellerObject = [HDSellerObject sellerMsg];
    __block NSMutableArray *arr = [NSMutableArray array];
    [lists enumerateObjectsUsingBlock:^(BmobObject * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HDOrderObject *addressObject = [[HDOrderObject alloc] initWithBmobObject:obj logoUrl:sellerObject.logoUrl];
        [arr addObject:addressObject];
    }];
    if (arr.count > 0) {
        [self asynDataBlock:^{
            RLMRealm *realm = [RLMRealm defaultRealm];
            NSError *error;
            [realm transactionWithBlock:^{
                [realm addOrUpdateObjects:arr];
            } error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (complete) {
                    complete(error);
                }
            });
        }];
        return;
    }
    if (complete) {
        complete(nil);
    }
}

+(void)confirmOrderWithObjectId:(NSString *)objectId complete:(void (^)(NSError * _Nullable))complete
{
    [self asynDataBlock:^{
        HDOrderObject *orderObject = [HDOrderObject objectForPrimaryKey:objectId];
        RLMRealm *realm = [RLMRealm defaultRealm];
        NSError *error;
        [realm transactionWithBlock:^{
            orderObject.orderStatus = HDOrderStatusVerify;
        } error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (complete) {
                complete(error);
            }
        });
    }];
}


+(NSDate *)getNewUpdatedAt
{
    RLMResults *results =[[HDOrderObject allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:YES];
    if (results.count > 0) {
        HDOrderObject *object = results.lastObject;
        return object.updatedAt;
    }else
    {
        return nil;
    }
}

-(instancetype)initWithBmobObject:(BmobObject *)object logoUrl:(NSString *)logoUrl
{
    if (self = [super init]) {
        _objectId = [object objectForKey:@"objectId"];
        _amount = [[object objectForKey:@"amount"] doubleValue];
        _remark = [object objectForKey:@"remark"];
        _logoUrl = logoUrl;
        
        BmobObject *address =  [object objectForKey:@"address"];
        _realName = [address objectForKey:@"realName"];
        _mobile = [address objectForKey:@"mobile"];
        _details = [address objectForKey:@"detail"];
        _orderStatus = [[object objectForKey:@"state"] integerValue];
        _updatedAt =[BmobDataInstance.formatter dateFromString: [object objectForKey:@"updatedAt"]];
        _createAt = [BmobDataInstance.formatter dateFromString: [object objectForKey:@"createdAt"]];
        NSArray *detail = [object objectForKey:@"detail"];
        for (NSDictionary *dic in detail) {
            NSInteger  buyCount = [[dic objectForKey:@"buyCount"] integerValue];
            NSString *imgUrl = [dic objectForKey:@"imgUrl"];
            NSString *name = [dic objectForKey:@"name"];
            NSString *objectId = [dic objectForKey:@"objectId"];
            double price = [[dic objectForKey:@"price"] integerValue];
            HDOrderDetailObject *detailObject =  [HDOrderDetailObject objectId:objectId name:name imgUrl:imgUrl price:price buyCount:buyCount];
            [self.details addObject:detailObject];
        }
    }
    return self;
}



@end
