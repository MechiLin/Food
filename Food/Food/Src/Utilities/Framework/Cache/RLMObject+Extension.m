//
//  RLMObject+Extension.m
//  HDAI
//
//  Created by 包月兴 on 2018/3/13.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "RLMObject+Extension.h"

@implementation RLMObject (Extension)

#pragma mark --- 子线程异步修改和新增数据
+(void)asynDataBlock:(dispatch_block_t)block
{
    dispatch_queue_t queue = [self createrealmQueue];
    dispatch_async(queue, ^{
        @autoreleasepool {
            if (block) {
                block();
            }
        }
    });
}



#pragma mark ---  主线程异步修改和新增数据
 

+(void)mainAsynDataBlock:(dispatch_block_t)block
{
    dispatch_async(dispatch_get_main_queue(), ^{
        @autoreleasepool {
            if (block) {
                block();
            }
        }
    });
}




#pragma mark ---  创建Rleam数据库操作线程

+(dispatch_queue_t)createrealmQueue
{
    return dispatch_queue_create("com.Realm",
                                 DISPATCH_QUEUE_CONCURRENT);
}

@end
