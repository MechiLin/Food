//
//  HDAddressObject.h
//  Food
//
//  Created by admin on 2018/8/10.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "RLMObject.h"
@class BmobObject;
NS_ASSUME_NONNULL_BEGIN
@interface HDAddressObject : RLMObject
@property (nonatomic,copy) NSString *objectId;
@property (nonatomic,copy) NSString *realName;  //真实姓名
@property (nonatomic,copy) NSString *mobile;  //电话号码
@property (nonatomic,copy) NSString *detailAddress; //详细地址
@property (nonatomic,copy) NSString *userObjectId;

+(void)insertDataWithBmobObjectLists:(NSArray<BmobObject *>*)lists
                            complete:(void (^)(NSError * _Nullable error))complete;


+(void)deleteAddressObjectWithObjectId:(NSString *)objectId;
+(RLMResults *)listResults;
@end
RLM_ARRAY_TYPE(HDAddressObject)
NS_ASSUME_NONNULL_END
