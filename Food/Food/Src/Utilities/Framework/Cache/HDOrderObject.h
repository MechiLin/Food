//
//  HDOrderObject.h
//  Food
//
//  Created by admin on 2018/8/11.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import "RLMObject.h"
@class BmobObject;
NS_ASSUME_NONNULL_BEGIN
@interface HDOrderDetailObject : RLMObject
@property (nonatomic,copy) NSString *objectId;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *imgUrl;
@property (nonatomic,assign) double price;
@property (nonatomic,assign) NSInteger buyCount;//当前点了多少?
+(instancetype)objectId:(NSString *)objectId
                   name:(NSString *)name
                 imgUrl:(NSString *)imgUrl
                  price:(double)price
               buyCount:(NSInteger)buyCount;

@end
RLM_ARRAY_TYPE(HDOrderDetailObject)

@interface HDOrderObject : RLMObject
@property (nonatomic,copy) NSString *objectId;
@property (nonatomic,copy) NSString *logoUrl;
@property (nonatomic,assign) double amount;  //总价
@property (nonatomic,copy) NSString *remark;  //备注
@property (nonatomic,copy) NSString *realName; //真实姓名
@property (nonatomic,copy) NSString *mobile; //电话号码
@property (nonatomic,copy) NSString *detailAddress;
@property (nonatomic,assign) HDOrderStatus orderStatus;
@property (nonatomic,copy) NSDate *updatedAt;
@property (nonatomic,copy) NSDate *createAt; //创建时间
@property RLMArray<HDOrderDetailObject *><HDOrderDetailObject> *details; //订单详情

+(void)insertDataWithBmobObjectLists:(NSArray<BmobObject *>*)lists
                            complete:(void (^)(NSError * _Nullable error))complete;
+(void)confirmOrderWithObjectId:(NSString *)objectId
                       complete:(void (^)(NSError * _Nullable error))complete;

+(NSDate *)getNewUpdatedAt;
@end
RLM_ARRAY_TYPE(HDOrderObject)
NS_ASSUME_NONNULL_END
