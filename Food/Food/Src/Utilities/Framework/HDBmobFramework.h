//
//  HDBmobFramework.h
//  Food
//
//  Created by 包月兴 on 2018/7/25.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YZMacro.h>
@class MQLFoodModel;
@class HDAddressObject;
typedef NS_ENUM(NSInteger,HDOrderStatus) {
    HDOrderStatusNoVerify = 0,
    HDOrderStatusVerify   = 1,
};



#define BmobDataInstance [HDBmobFramework sharedInstance]

NS_ASSUME_NONNULL_BEGIN
@interface HDBmobFramework : NSObject

@property (nonatomic,copy) NSDateFormatter *formatter;

SingletonH
-(void)updateServiceData;

/**
 登录

 @param name 用户名
 @param pwd 密码
 @param complete 结果
 */
-(void)loginWithName:(NSString *)name
                 pwd:(NSString *)pwd
            complete:(void (^)(NSError * _Nullable error))complete;


/**
 新增收货地址

 @param realName 真实密码
 @param mobile 电话号码
 @param detailAddress 详细地址
 @param complete 结果
 */
-(void)addAddressWithRealName:(NSString *)realName
                       mobile:(NSString *)mobile
                detailAddress:(NSString *)detailAddress
                     complete:(void (^)(NSError * _Nullable error))complete;


/**
 删除收货地址

 @param objectId 唯一标识
 @param complete 结果
 */
-(void)deleteAddressWithObjectId:(NSString *)objectId
                        complete:(void (^)(NSError * _Nullable error))complete;


/**
 下订单

 @param foodList 商品列表
 @param amount 数量
 @param addressObject 地址
 @param complete 结果
 */
-(void)placeOrderWithFoodList:(NSArray<MQLFoodModel *>*)foodList
                       amount:(double)amount
              addressObject:(HDAddressObject *)addressObject
                       remark:(NSString *)remark
                     complete:(void (^)(NSError * _Nullable error))complete;


/**
 商家确认订单

 @param objectId 订单唯一标识
 @param complete 结果
 */
-(void)confirmOrderWithObjectId:(NSString *)objectId
                       complete:(void (^)(NSError * _Nullable error))complete;


-(void)downOrderlist;

@end
NS_ASSUME_NONNULL_END
