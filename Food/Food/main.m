//
//  main.m
//  Food
//
//  Created by 包月兴 on 2018/7/31.
//  Copyright © 2018年 包月兴. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <BmobSDK/Bmob.h>
#import "HDBmobFramework.h"
int main(int argc, char * argv[]) {
    @autoreleasepool {
        [Bmob registerWithAppKey:@"8799bb2abc8c990630bb6a7bcc28c63b"];
        [BmobDataInstance updateServiceData];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
